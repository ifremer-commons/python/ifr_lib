# ifr_lib

This module provides utilities for developing python programs :

- General
  - ifr_exceptions : common exceptions
  - ifr_logging : add behaviors to logs (use colors, print dictionary, compress log files, ...)
  - ifr_template : add facilities to manage Jinja2 template
  - ifr_timer : Timer and Timers classes  
  - ifr_misc : other utilities (retrieve package version, ...)
- Manage files format :
  - ifr_yaml : add facilities to manage yaml files (merge, inject data)
  - ifr_json : add facilities to load/dump json
  - ifr_zip : add facilities to manage zip files (add file to existing zip, ...)
  - ifr_xml : add facilities to manage xml files
- Manage datatype :
  - ifr_collections : to manage  (merge, compare, prettify, ...)
  - ifr_datetimes : to manage date and datime (parse, format, days in a period, ...)
  - ifr_numbers : to manage numbers (is_number, cast, ...)
  - ifr_enum : to compare enum
- Manage system files and directories :
  - ifr_os.path : to manage directories (manage trees, ...), permissions, mount point (`is_io_locked`)
  - ifr_files : to manage files (touch, hash, is mtime older than, ...)
- Manage process :
  - ifr_subprocess : to manage run and external command 
  - ifr_thread : add facilities to manage thread (timeout, ...)
  - ifr_pbs : add a command `qmonitor` to supervise the end of a jobId List
 
## Development

### Install poetry

```bash
pip install poetry poetry-dynamic-versioning poetry2conda
poetry --version
poetry config repositories.nexus-public-release https://nexus-test.ifremer.fr/repository/hosted-pypi-public-release/
```

### retrieve and install project

```bash
git clone https://gitlab.ifremer.fr/ifremer-commons/python/ifr_lib.git
poetry install -v --no-root
```

### List dependencies

```bash
poetry show --tree
```

# build and publish wheel

```bash
poetry build --format wheel
poetry publish -r nexus-public-release -u nexus-ci -p w2bH2NjgFmQnzVk3
```
 
# build documentation

```
mkdocs build -f docs/mkdocs.yml
```
