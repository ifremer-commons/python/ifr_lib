#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tests import IfrTestCase
import os
from ifr_lib import ifr_template, ifr_exception
from jinja2 import FileSystemLoader


class TemplateTestCase(IfrTestCase):

    def test_interpolate_number(self):
        renderer = ifr_template.BaseTemplateRenderer()

        self.assertEqual(renderer.interpolate(10), 10)
        self.assertEqual(renderer.interpolate(10.5), 10.5)

    def test_interpolate_string(self):
        renderer = ifr_template.BaseTemplateRenderer()

        self.assertEqual(
            renderer.interpolate("Hello World !"),
            "Hello World !"
        )

        self.assertEqual(
            renderer.interpolate("Hello {{ msg }} !", {"msg": "you"}),
            "Hello you !"
        )

        self.assertEqual(
            renderer.interpolate("Hi {{ key.subkey }} !", {"key": {"subkey": "stranger"}}),
            "Hi stranger !"
        )

    def test_interpolate_block(self):
        renderer = ifr_template.BaseTemplateRenderer()

        self.assertEqual(
            renderer.interpolate("{% set a = 1 %}Number {{ a }}"),
            "Number 1"
        )

    def test_interpolate_string_dollar_renderer(self):
        renderer = ifr_template.BaseTemplateRenderer(j2_env_params=ifr_template.TOKEN_STYLE_DOLLAR)

        self.assertEqual(
            renderer.interpolate("Hello ${ msg } !", {"msg": "padawan"}),
            "Hello padawan !"
        )

        self.assertEqual(
            renderer.interpolate("Hello ${  key.subkey } !", {"key": {"subkey": "jedi"}}),
            "Hello jedi !"
        )

    def test_interpolate_functions(self):
        renderer = ifr_template.BaseTemplateRenderer(j2_env_params=ifr_template.TOKEN_STYLE_DOLLAR)

        self.assertEqual(
            renderer.interpolate("Hello ${ env('CAS_USER') } !"),
            "Hello {} !".format(os.environ.get("CAS_USER"))
        )

        self.assertEqual(
            renderer.interpolate("Hello ${ user or env('FAKE') } !", {"user": "toto"}),
            "Hello toto !"
        )

        self.assertEqual(
            renderer.interpolate("${ parse_date('20200101', '%Y%m%d') | datetime('%Y') }"),
            "2020"
        )

    def test_interpolate_undefined(self):
        raise_renderer = ifr_template.BaseTemplateRenderer(raise_if_undefined=True)

        with self.assertRaises(ifr_exception.ItemNotFoundException):
            raise_renderer.interpolate("Good Morning {{ key }} !")
            raise_renderer.interpolate("Hello {{ key.b }} !")
            raise_renderer.interpolate("Hello {{ key.b }} !", {"key": "tests"})

        empty_renderer = ifr_template.BaseTemplateRenderer(hide_undefined_tokens=True)
        self.assertEqual(
            empty_renderer.interpolate("Ola {{ key }} !"),
            "Ola  !"
        )

        default_renderer = ifr_template.BaseTemplateRenderer()
        self.assertEqual(
            default_renderer.interpolate("Good Evening {{ key }} !"),
            "Good Evening {{ key }} !"
        )

        dollar_renderer = ifr_template.BaseTemplateRenderer(j2_env_params=ifr_template.TOKEN_STYLE_DOLLAR)
        self.assertEqual(
            dollar_renderer.interpolate("Hello ${ key } !"),
            "Hello ${ key } !"
        )

        self.assertEqual(
            dollar_renderer.interpolate(
                {
                    "ssmi_concentration_pattern": '${cersat.public_data}/sea-ice/sea-ice-concentration/<pole>'
                                                  '/ssmi/CER_PSI_ANT_1D_012_PSI_SS/daily/netcdf/%Y/%Y%m%d.nc'
                }
            ),
            {
                "ssmi_concentration_pattern": '${cersat.public_data}/sea-ice/sea-ice-concentration/<pole>'
                                              '/ssmi/CER_PSI_ANT_1D_012_PSI_SS/daily/netcdf/%Y/%Y%m%d.nc'
            }
        )

    def test_find_undefined_variables(self):
        default_renderer = ifr_template.BaseTemplateRenderer()

        """
        self.assertListEqual(
            default_renderer.find_undeclared_variables("Hello {{ key }} !"),
            ['key']
        )
        """

        # TODO : try to return ['key.subkey'] ?
        # default_renderer.parse :
        # Template(body=[Output(nodes=[
        #   TemplateData(data='Hello '),
        #   Getattr(node=Name(name='key', ctx='load'), attr='subkey', ctx='load'),
        #   TemplateData(data=' !')
        # ])])
        #
        # default_renderer.interpolate :
        # Hello {{ no such element: str object['subkey'] }} !

        self.assertListEqual(
            default_renderer.find_undeclared_variables("Hello {{ key.subkey }} !", {"key": "tests"}),
            []
        )

        self.assertListEqual(
            default_renderer.find_undeclared_variables("Hello {{ key1 }} {{ key2 }} {{ key1 }}!"),
            ['key1', 'key2']
        )

    def test_interpolate_list(self):
        self.assertListEqual(
            ifr_template.BaseTemplateRenderer().interpolate(
                [
                    "Good Morning {{ key.subkey }} !",
                    "Welcome {{ user }} !"
                ],
                {
                    "user": "ebodere",
                    "key": {
                        "subkey": "World"
                    }
                }
            ),
            [
                "Good Morning World !",
                "Welcome ebodere !"
            ]
        )

    def test_interpolate_dict(self):
        self.assertDictEqual(
            ifr_template.BaseTemplateRenderer().interpolate(
                {
                    "msg1": "Good Afternoon {{ key.subkey }} !",
                    "msg2": {
                        "msg21": "Welcome {{ user }} !"
                    }
                },
                {
                    "user": "ebodere",
                    "key": {
                        "subkey": "World"
                    }
                }
            ),
            {
                "msg1": "Good Afternoon World !",
                "msg2": {
                    "msg21": "Welcome ebodere !"
                }
            }
        )

    def test_interpolate_template(self):
        renderer = ifr_template.BaseTemplateRenderer(
            loader=FileSystemLoader(self.get_resource_path("j2_template_test_files"))
        )

        self.assertEqual(
            renderer.interpolate(
                renderer.get_template("hello_world.tpl"),
                {
                    "header": "Entete",
                    "footer": "Pied",
                    "numbers": [1, 2, 3, 4]
                }
            ),
            "Entete\n- 1\n- 2\n- 3\n- 4\nPied"
        )

    def test_find_undeclared_variables(self):
        self.assertEqual(True, True)


if __name__ == '__main__':
    import unittest
    unittest.main()
