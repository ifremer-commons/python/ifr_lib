#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tempfile
import os
from testfixtures import compare

from tests import IfrTestCase
from ifr_lib import ifr_path, ifr_datetimes, ifr_exception

CFO_20200110T1520_NC_FILE_NAME = "CFO_20200110T1520.nc"
CFO_T1520_FILTER = 'CFO*T1520.nc'
CFO_FILE_DATE_PATTERN = "CFO_([0-9]{8}T[0-9]{4}).nc"
ARCHIVING_PATTERN = '%Y/%j'


class PathTestCase(IfrTestCase):

    def setUp(self) -> None:
        self.tmp_dir = ifr_path.Folder.from_parts(tempfile.gettempdir(), "ifr_lib", "ifr_path").clean_or_create()
        self.dir1 = self.tmp_dir.new_folder("2020").create()
        self.dir1_1 = self.dir1.new_folder("010").create()
        self.dir1_1.new_file(CFO_20200110T1520_NC_FILE_NAME).touch(new_mtime=ifr_datetimes.parse_date('8 days ago'))
        self.dir1_1.new_file("CFO_20200110T1620.nc").touch(new_mtime=ifr_datetimes.parse_date('4 days ago'))

        self.dir1_2 = self.dir1.new_folder("011").create()
        self.dir1_2.new_file("CFO_20200111T1520.nc").touch()
        self.dir1_2.new_file("CFO_20200111T1620.nc").touch()

        self.dir1_3 = self.dir1.new_folder("012").create()
        self.dir1_3.new_file("CFO_20200112T1520.nc").touch()
        self.dir1_3.new_file("CFO_20200112T1620.nc").touch()

    def tearDown(self) -> None:
        self.tmp_dir.rmtree()

    def test_path_finder(self):
        self.assertEqual(len(self.dir1.find()), 9)
        self.assertEqual(len(self.dir1.find(depth=1)), 3)

        self.assertEqual(len(ifr_path.PathFinder(self.dir1.path, ifr_path.DirectoryFilter())), 3)
        self.assertEqual(len(self.dir1.find(ifr_path.DirectoryFilter())), 3)

        self.assertEqual(len(ifr_path.PathFinder(self.dir1.path, ifr_path.FileFilter())), 6)
        self.assertEqual(len(self.dir1.find(ifr_path.FileFilter())), 6)

        self.assertEqual(
            len(self.dir1.find(
                ifr_path.AndFilter(
                    ifr_path.FileFilter(),
                    ifr_path.BasenameFnmatchFilter(CFO_T1520_FILTER)
                )
            )),
            3
        )

        self.assertEqual(
            len(self.dir1.find(
                ifr_path.AndFilter(
                    ifr_path.FileFilter(),
                    ifr_path.BasenameRegexFilter(r'CFO.*\.nc')
                )
            )),
            6
        )

        not_exist_dir = self.dir1.new_folder("fake")
        self.assertFalse(not_exist_dir.exists())
        with self.assertRaises(ifr_exception.FileNotFoundException):
            not_exist_dir.find()

        if os.name == 'nt':
            paths = self.dir1.find(ifr_path.AndFilter(ifr_path.RegexFilter(r'.*\\010\\CFO.*T1520\.nc')))
        else:
            paths = self.dir1.find(ifr_path.AndFilter(ifr_path.RegexFilter(r'.*/010/CFO.*T1520\.nc')))
        self.assertEqual(len(paths), 1)
        self.assertTrue(type(paths[0]), ifr_path.File)
        self.assertEqual(paths[0].name, CFO_20200110T1520_NC_FILE_NAME)
        self.assertEqual(paths[0].stem, "CFO_20200110T1520")
        self.assertEqual(paths[0].extension, ".nc")
        self.assertEqual(paths[0].path, os.path.join(self.tmp_dir.path, "2020", "010", CFO_20200110T1520_NC_FILE_NAME))

        if os.name == 'nt':
            paths = self.dir1.find(
                ifr_path.AndFilter(ifr_path.RegexFilter(r'.*\\010\\CFO.*T1520\.nc')),
                format_fct=None
            )
        else:
            paths = self.dir1.find(
                ifr_path.AndFilter(ifr_path.RegexFilter(r'.*/010/CFO.*T1520\.nc')),
                format_fct=None
            )
        self.assertEqual(len(paths), 1)
        self.assertTrue(type(paths[0]), str)
        self.assertEqual(paths[0], os.path.join(self.tmp_dir.path, "2020", "010", CFO_20200110T1520_NC_FILE_NAME))

        with self.assertRaises(ifr_exception.BadParameterException):
            ifr_path.PathFinder(os.path.join(self.dir1_1.path, CFO_20200110T1520_NC_FILE_NAME))

    def test_archive_path_finder(self):
        self.assertEqual(len(ifr_path.ArchivePathFinder(
            folder=os.path.join(self.tmp_dir.path, ARCHIVING_PATTERN),
            search_date=ifr_datetimes.str_to_datetime("20200110", "%Y%m%d"),
            to_date=ifr_datetimes.str_to_datetime("20200113", "%Y%m%d"),
            filter=ifr_path.BasenameFnmatchFilter(CFO_T1520_FILTER),
            silent=True
        )), 3)

        with self.assertRaises(ifr_exception.FileNotFoundException):
            ifr_path.ArchivePathFinder(
                folder=os.path.join(self.tmp_dir.path, ARCHIVING_PATTERN),
                search_date=ifr_datetimes.str_to_datetime("20200115", "%Y%m%d"),
                filter=ifr_path.BasenameFnmatchFilter(CFO_T1520_FILTER)
            ).scan()

    def test_time_filter(self):
        self.assertEqual(
            len(self.dir1.find(
                ifr_path.AndFilter(
                    ifr_path.FileFilter(),
                    ifr_path.TimeFilter(ifr_path.TimeFilter.FILE_MTIME, 2, ifr_path.TimeFilter.DELTA_UNIT_DAYS)
                )
            )),
            2
        )

        self.assertEqual(
            len(self.dir1.find(
                ifr_path.AndFilter(
                    ifr_path.FileFilter(),
                    ifr_path.TimeFilter(ifr_path.TimeFilter.FILE_MTIME, 5, ifr_path.TimeFilter.DELTA_UNIT_DAYS)
                )
            )),
            1
        )

        self.assertEqual(
            len(self.dir1.find(
                ifr_path.AndFilter(
                    ifr_path.FileFilter(),
                    ifr_path.TimeFilter(ifr_path.TimeFilter.FILE_MTIME, -3, ifr_path.TimeFilter.DELTA_UNIT_DAYS)
                )
            )),
            4
        )

    def test_date_filter(self):
        self.assertEqual(
            len(self.dir1.find(
                ifr_path.AndFilter(
                    ifr_path.FileFilter(),
                    ifr_path.BasenameFnmatchFilter("CFO_*.nc"),
                    ifr_path.BasenameDateFilter(
                        file_date_pattern=CFO_FILE_DATE_PATTERN,
                        file_date_format="%Y%m%dT%H%M",
                        operator="egt",
                        search_date_value="20200110T1520",
                        search_date_format="%Y%m%dT%H%M"
                    ),
                    ifr_path.BasenameDateFilter(
                        file_date_pattern=CFO_FILE_DATE_PATTERN,
                        file_date_format="%Y%m%dT%H%M",
                        operator="lgt",
                        search_date_value="20200110T1620",
                        search_date_format="%Y%m%dT%H%M"
                    )
                )
            )),
            2
        )

        self.assertEqual(
            len(self.dir1.find(
                ifr_path.AndFilter(
                    ifr_path.FileFilter(),
                    ifr_path.BasenameFnmatchFilter("CFO_*.nc"),
                    ifr_path.BasenameDateFilter(
                        file_date_pattern=CFO_FILE_DATE_PATTERN,
                        file_date_format="%Y%m%dT%H%M",
                        operator="egt",
                        search_date_value="20200110T1520",
                        search_date_format="%Y%m%dT%H%M"
                    ),
                    ifr_path.BasenameDateFilter(
                        file_date_pattern=CFO_FILE_DATE_PATTERN,
                        file_date_format="%Y%m%dT%H%M",
                        operator="lgt",
                        search_date_value="20200110T1525",
                        search_date_format="%Y%m%dT%H%M"
                    )
                )
            )),
            1
        )

    def test_comparator_serialization(self):
        # -----------------------------------------------
        # ifr_path.PathFinder
        # -----------------------------------------------
        pf = ifr_path.PathFinder(
            self.dir1.path,
            ifr_path.AndFilter(
                ifr_path.FileFilter(),
                ifr_path.BasenameFnmatchFilter(CFO_T1520_FILTER)
            )
        )
        # convert pf -> yaml
        pf_yaml = pf.to_yaml()
        # convert yaml -> new ifr_path.PathFinder instance
        pf_from_yaml = ifr_path.PathFinder.from_yaml(pf_yaml)
        # check yaml
        self.assertEqual(pf_yaml, pf_from_yaml.to_yaml())
        # check results
        self.assertListEqual(pf.paths, pf_from_yaml.paths)
        print(pf_yaml)

        # -----------------------------------------------
        # ifr_path.ArchivePathFinder
        # -----------------------------------------------
        apf = ifr_path.ArchivePathFinder(
            folder=os.path.join(self.tmp_dir.path, ARCHIVING_PATTERN),
            search_date=ifr_datetimes.str_to_datetime("20200110", "%Y%m%d"),
            filter=ifr_path.AndFilter(
                ifr_path.BasenameFnmatchFilter(CFO_T1520_FILTER),
                ifr_path.BasenameDateFilter(
                    file_date_pattern=CFO_FILE_DATE_PATTERN,
                    file_date_format="%Y%m%dT%H%M",
                    operator="lgt",
                    search_date_value="20200110T1525",
                    search_date_format="%Y%m%dT%H%M"
                )
            )
        )
        # convert pf -> yaml
        apf_yaml = apf.to_yaml()
        # convert yaml -> new ifr_path.PathFinder instance
        apf_from_yaml = ifr_path.ArchivePathFinder.from_yaml(apf_yaml)
        # check objects
        compare(apf, apf_from_yaml)
        # check results
        self.assertListEqual(apf.paths, apf_from_yaml.paths)


if __name__ == '__main__':
    import unittest
    unittest.main()
