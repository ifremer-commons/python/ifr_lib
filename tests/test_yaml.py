#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from collections import Counter, OrderedDict
from tempfile import NamedTemporaryFile

from tests import IfrTestCase
from ifr_lib import ifr_yaml, ifr_exception


class YamlConfigTestCase(IfrTestCase):

    def setUp(self):
        # Method executed before each tests method
        self.yaml_config = ifr_yaml.YamlConfig(
            file=self.get_resource_path('yaml_tests_files', 'global_configuration.yaml')
        )

        if not self.yaml_config:
            self.fail("No yaml config loaded")

        self.__default_config_as_str = """\
            name:
              family: Smith
              given: Alice
            """

        self.__bad_config_as_str = """\
            name:
              family: Smith
              given: Alice
              toto:
             titi:
            """

        self.__default_config_with_variables_as_str = """\
            name:
              family: ${myfamily.name}
              given: Alice
            """

        self.__default_config_as_dict = {"name": {"family": "Smith", "given": "Alice"}}

    def test_empty_yaml(self):
        config = ifr_yaml.YamlConfig()
        with self.assertRaises(ifr_exception.ConfigurationException):
            config['tests']

    def test_malformed_yaml(self):
        with self.assertRaises(Exception):
            ifr_yaml.YamlConfig(file=self.get_resource_path('yaml_tests_files', 'malformed.yaml'))

    def test_encoding(self):
        config = ifr_yaml.YamlConfig(file=self.get_resource_path('yaml_tests_files', 'encoding_utf8.yaml'))
        self.assertEqual(
            config['database.password'],
            'Serpillières'
        )

        config = ifr_yaml.YamlConfig(
            file=self.get_resource_path('yaml_tests_files', 'encoding_utf8.yaml'),
            encoding='latin-1'
        )
        self.assertNotEqual(
            config['database.password'],
            'Serpillières'
        )

    def test_yaml_config_content(self):
        self.assertIsNotNone(self.yaml_config.content)

    def test_yaml_config_box_keys(self):
        self.assertIsNotNone(self.yaml_config.content.keys())

    def test_yaml_config_box_data_keys(self):
        self.assertIsNotNone(self.yaml_config.content.get('osisaf').keys())

    def test_yaml_config_keys(self):
        self.assertIsNotNone(self.yaml_config.leaf_keys)

        self.assertEqual(
            (
                # noqa: W503
                len(self.yaml_config['credentials'])
                + len(self.yaml_config['osisaf'])
                + len(self.yaml_config['smtp'])
            ),
            len(self.yaml_config.leaf_keys)
        )

    def test_get_item_methods_equivalence(self):
        dotted_osisaf_in_bkp = 'osisaf.in_bkp'
        self.assertEqual(self.yaml_config['osisaf']['in_bkp'], self.yaml_config[dotted_osisaf_in_bkp])

        self.assertEqual(self.yaml_config[dotted_osisaf_in_bkp], self.yaml_config.content.get('osisaf').get('in_bkp'))

        self.assertTrue('osisaf' in self.yaml_config)
        self.assertTrue(dotted_osisaf_in_bkp in self.yaml_config)
        self.assertFalse('osisafin_bkp' in self.yaml_config)

    def test_dict_transformation(self):
        self.assertIsNotNone(self.yaml_config.leafs())
        self.assertEqual(len(self.yaml_config.leafs()), len(self.yaml_config.leaf_keys))

    def test_yaml_to_ini_converter(self):

        ini_content = ifr_yaml.YamlConfig(
            file=self.get_resource_path('yaml_tests_files', 'cfo_sca_l3ice__antarctic_hh.yaml')
        ).to_ini()

        expected_ini = """[general]
resolution = RES12
sigma0_adjustment_fit_resolution = RES25
reference_incidence = 40.
output_pattern = CFO_IWWOC_SCA_L3ICE__%Y%m%d_ANT_1D_012_HH.nc

[data]
product_id = CFO_SCA_L1B
input_pattern = ${cersat.public_data}/cfosat/data/simulation/chinese_mission_center/l1b/%Y/%j/SCA_%Y%m%dT000000_*_H.nc
mapper = scatl1bncfile.SCATL1BNCFile
reader = SCATReader
none_property = 
other = 1"""

        self.assertEqual(ini_content, expected_ini)

    def test_load_no_config(self):
        yaml_config = ifr_yaml.YamlConfig()
        self.assertIsInstance(yaml_config, ifr_yaml.YamlConfig)
        self.assertIsNone(yaml_config.content)

    def test_merge_with_bad_type(self):
        # Merge with instance other than ifr_yaml.YamlConfig or dict should raise a BadTypeException
        with self.assertRaises(ifr_exception.BadTypeException):
            ifr_yaml.YamlConfig().merge(["list_item1", "list_item2"])

    def test_merge_with_none(self):
        # Merge ifr_yaml.YamlConfig with None should have no effect on _source ifr_yaml.YamlConfig
        yaml_config = ifr_yaml.YamlConfig()
        self.assertIsNone(yaml_config.merge(None))
        self.assertIsNotNone(yaml_config)

    def test_merge_empty_with_dict(self):
        # Merge empty ifr_yaml.YamlConfig with content should preserve content
        yaml_config = ifr_yaml.YamlConfig()
        yaml_config.merge(self.yaml_config.content)
        self.assertDictEqual(yaml_config.content, self.yaml_config.content)

    def test_merge_configs(self):
        first_dict = {"att1": "value1", "att2": "value2", "att3": "value3"}
        second_dict = {"att4": "value4", "att5": "value5", "att6": "value6"}

        # Merge ifr_yaml.YamlConfig with other ifr_yaml.YamlConfig (no common attributes)
        first_config = ifr_yaml.YamlConfig(content=first_dict)
        second_config = ifr_yaml.YamlConfig(content=second_dict)
        first_config.merge(second_config)
        self.assertTrue(set(second_dict.items()).issubset(first_config.content.items()))
        self.assertTrue(set(first_dict.items()).issubset(first_config.content.items()))

        # Merge with common keys and an orphan key
        third_dict = {"att1": "other_value1", "att5": "other_value5"}
        orphan_keys = [{"att7": "${orphan_key}"}]
        for element in orphan_keys:
            third_dict.update(element)

        first_config.merge(third_dict)
        self.assertFalse(set(second_dict.items()).issubset(first_config.content.items()))
        self.assertFalse(set(first_dict.items()).issubset(first_config.content.items()))
        self.assertTrue(set(third_dict.items()).issubset(first_config.content.items()))
        self.assertEqual(len(first_config.leaf_keys), len(first_dict) + len(second_dict) + len(orphan_keys))
        self.assertEqual(len(first_config.orphan_keys), len(orphan_keys))
        self.assertIn('orphan_key', first_config.orphan_keys)

    def test_load_bad_config(self):
        with self.assertRaises(ifr_exception.SyntaxException):
            ifr_yaml.YamlConfig(self.__bad_config_as_str)

    def test_load_unexisting_file(self):
        with self.assertRaises(ifr_exception.FileNotFoundException):
            ifr_yaml.YamlConfig(file='fake.yaml')

    def test_load_dotted_keys_file(self):
        config = ifr_yaml.YamlConfig(content="""\
            dotted_keys:
              "key.1": "value1"
              "key.2": "value2"
              "key.3": "value3"
            """)

        config.merge(
            ifr_yaml.YamlConfig(content="""
                dotted_keys:
                  "key.1": "value8"
                  "key.5": "value5"
                """)
        )

        self.assertDictEqual(
            config.as_dict(),
            {
                "dotted_keys": {
                    "key.1": "value8",
                    "key.2": "value2",
                    "key.3": "value3",
                    "key.5": "value5"
                }
            }
        )

    def test_load_formats(self):
        a = ifr_yaml.YamlConfig(self.__default_config_as_str)
        b = ifr_yaml.YamlConfig({
            "name": {
                "family": "Smith",
                "given": "Alice",
            }
        })
        import os
        import tempfile
        (file, path) = tempfile.mkstemp()
        try:
            with os.fdopen(file, "w") as s:
                s.write(self.__default_config_as_str)
            c = ifr_yaml.YamlConfig(file=path)

            self.assertEqual(a.as_dict(), self.__default_config_as_dict)
            self.assertEqual(a.as_dict(), b.as_dict())
            self.assertEqual(a.as_dict(), c.as_dict())
        finally:
            try:
                os.remove(path)
            except OSError:
                pass

    def test_load_conf_with_envvar(self):
        user_profile_value = os.getenv('USERPROFILE')
        tmp_value = os.getenv('TEMP')

        if not (user_profile_value and tmp_value):
            # skip tests if envvars are unset
            self.assertTrue(True)
            return

        config = ifr_yaml.YamlConfig(file=self.get_resource_path('yaml_tests_files', 'envvar_conf.yaml'))
        self.assertDictEqual(
            config.orphan_keys,
            Counter({'env:BLABLA': 1, 'env:': 1})
        )
        self.assertDictEqual(
            config.as_dict(),
            OrderedDict([
                ('user_profile', user_profile_value),
                ('unexisting', "${env('BLABLA')}"),
                ('void', "${env()}"),
                ('tmp1', tmp_value + '/dir1'),
                ('tmp2', tmp_value + '/dir2')
            ])
        )

    def test_contains(self):
        yaml_config = ifr_yaml.YamlConfig(self.__default_config_as_str)
        self.assertTrue(yaml_config.__contains__('name.family'))
        self.assertTrue(yaml_config.contains('name.family'))
        self.assertTrue(yaml_config.__contains__('name.given'))
        self.assertTrue(yaml_config.contains('name.given'))
        self.assertFalse(yaml_config.__contains__('wrong_key'))
        self.assertFalse(yaml_config.contains('wrong_key'))

    def test_replacements(self):
        yaml_config = ifr_yaml.YamlConfig(
            content=self.__default_config_with_variables_as_str,
            replacements={"myfamily.name": "Smith"}
        )
        self.assertEqual(yaml_config['name.family'], "Smith")


if __name__ == '__main__':
    import unittest
    unittest.main()
