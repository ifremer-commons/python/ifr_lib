#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
import shutil
import tempfile
import unittest


class IfrTestCase(unittest.TestCase):

    @staticmethod
    def get_resources_root_path():
        root_path = os.path.abspath("")
        if os.path.basename(root_path) != "tests":
            root_path = os.path.join("tests")
        return os.path.join(root_path, "resources")

    @staticmethod
    def create_temporary_copy(src):
        temp_dir = tempfile.gettempdir()
        temp_path = os.path.join(temp_dir, os.path.basename(src))
        shutil.copy2(src, temp_path)
        return temp_path

    @classmethod
    def get_temp_dir(cls):
        return tempfile.mkdtemp(prefix=cls.__name__)

    def get_expected_root_path(self) -> str:
        return os.path.join(
            self.get_resources_root_path(),
            "expected",
            re.sub(r'Test$', '', self.__class__.__name__)
        )

    def get_expected_file_path(self, *relative_path) -> str:
        return os.path.join(self.get_expected_root_path(), *relative_path)

    def get_resource_path(self, *relative_path) -> str:
        return os.path.join(self.get_resources_root_path(), *relative_path)
