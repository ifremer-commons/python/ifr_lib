#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import os

from tests import IfrTestCase
from ifr_lib import ifr_json

# ##############################################
# CONSTANTS
# ##############################################
# The tests resources directory
RESOURCES_DIR = os.path.join(os.path.dirname(__file__), 'resources')


class JsonTestCase(IfrTestCase):
    """
    This class is a set of tests for the files methods of ifr_lib.
    """

    def test_JsonSerializable(self):

        class Person(ifr_json.JsonSerializable):

            def __init__(self, first_name, last_name, addresses):
                super(Person, self).__init__()
                self.first_name = first_name
                self.last_name = last_name
                self.addresses = addresses

        addresses = ['first address', 'second address', 'third address']

        json_person = Person('John', 'Doe', addresses).to_json(indent=2)

        expected_json_python3 = '{\n' \
                                '  \"first_name\": \"John\",\n' \
                                '  \"last_name\": \"Doe\",\n' \
                                '  \"addresses\": [\n' \
                                '    \"first address\",\n' \
                                '    \"second address\",\n' \
                                '    \"third address\"\n' \
                                '  ]\n' \
                                '}'

        expected_json_python2 = "{\n" \
                                "  \"first_name\": \"John\", \n" \
                                "  \"last_name\": \"Doe\", \n" \
                                "  \"addresses\": [\n" \
                                "    \"first address\", \n" \
                                "    \"second address\", \n" \
                                "    \"third address\"\n" \
                                "  ]\n" \
                                "}"

        try:
            self.assertEqual(bytes(json_person), bytes(expected_json_python2))
        except TypeError:
            self.assertEqual(bytes(json_person, "utf8"), bytes(expected_json_python3, "utf8"))

        json_person = Person.from_json(
            '{\n'
            '  \"first_name\": \"John\",\n'
            '  \"last_name\": \"Doe\",\n'
            '  \"addresses\": [\n'
            '    \"first address\",\n'
            '    \"second address\",\n'
            '    \"third address\"\n'
            '  ]\n'
            '}'
        ).to_json(indent=2)

        try:
            self.assertEqual(bytes(json_person), bytes(expected_json_python2))
        except TypeError:
            self.assertEqual(bytes(json_person, "utf8"), bytes(expected_json_python3, "utf8"))

    def test_json_dumps(self):
        date_event_dict = {
            "event_name": "first_event",
            "event_date": datetime.datetime(year=2019, month=4, day=10, hour=9, minute=10, second=15)
        }
        self.assertEqual(ifr_json.json_loads(ifr_json.json_dumps(date_event_dict)), date_event_dict)

    def test_json_loads(self):
        date_event_dict = '{"event_name": "first_event", "event_date": "2019-04-10T09:10:15"}'
        self.assertEqual(ifr_json.json_dumps(ifr_json.json_loads(date_event_dict)), date_event_dict)


if __name__ == '__main__':
    import unittest
    unittest.main()
