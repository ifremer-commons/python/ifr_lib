#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ifr_lib import ifr_numbers, ifr_exception
from tests import IfrTestCase


class NumberTestCase(IfrTestCase):

    def test_is_number(self):

        # integer
        self.assertTrue(ifr_numbers.is_number('026'))
        self.assertTrue(ifr_numbers.is_number(26))

        # float
        self.assertTrue(ifr_numbers.is_number('26.05'))
        self.assertTrue(ifr_numbers.is_number(26.05))
        self.assertTrue(ifr_numbers.is_number('26E01'))

        # string
        self.assertFalse(ifr_numbers.is_number('26,05'))
        self.assertFalse(ifr_numbers.is_number('abc'))

    def test_to_int(self):
        # integer
        self.assertEqual(ifr_numbers.to_int('026'), 26)
        self.assertEqual(ifr_numbers.to_int(26), 26)

        # float
        self.assertEqual(ifr_numbers.to_int('26.07'), 26)
        self.assertEqual(ifr_numbers.to_int(26.07), 26)
        self.assertEqual(ifr_numbers.to_int('26E1'), 260)

        # string
        with self.assertRaises(ifr_exception.ConversionException):
            ifr_numbers.to_int('26,07')

        with self.assertRaises(ifr_exception.ConversionException):
            ifr_numbers.to_int('abc')

    def test_to_float(self):
        # integer
        self.assertEqual(ifr_numbers.to_float('026'), 26)
        self.assertEqual(ifr_numbers.to_float(26), 26)

        # float
        self.assertEqual(ifr_numbers.to_float('26.09'), 26.09)
        self.assertEqual(ifr_numbers.to_float(26.12), 26.12)
        self.assertEqual(ifr_numbers.to_float('26E1'), 260)

        # string
        with self.assertRaises(ifr_exception.ConversionException):
            ifr_numbers.to_float('26,29')

        with self.assertRaises(ifr_exception.ConversionException):
            ifr_numbers.to_float('abc')


if __name__ == '__main__':
    import unittest
    unittest.main()
