#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from ifr_lib import ifr_misc
from tests import IfrTestCase


def random_envvar():
    for key in os.environ.keys():
        return key, os.environ[key]


class MiscTestCase(IfrTestCase):
    """
    This class is a set of tests for the ifr_misc methods of ifr_lib.
    """

    def test_package_version(self):
        self.assertIsNotNone(ifr_misc.package_version("ifr_lib"))

    def test_get_value(self):

        # default
        self.assertEqual("my_default_value", ifr_misc.getvalue(None, None, "my_default_value"))
        self.assertEqual("my_default_value", ifr_misc.getvalue(None, "non_existing_env_var", "my_default_value"))

        # not default
        self.assertEqual("my_actual_value", ifr_misc.getvalue("my_actual_value", None, "my_default_value"))

        # envvar
        key, value = random_envvar()
        self.assertEqual(value, ifr_misc.getvalue(None, key, "my_default_value"))


if __name__ == '__main__':
    import unittest
    unittest.main()
