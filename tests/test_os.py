#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import shutil
from zipfile import ZipFile

from ifr_lib import ifr_os
from tests import IfrTestCase

# ##############################################
# CONSTANTS
# ##############################################
# The tests resources directory
OS_FILE_DIR = 'os_test_files'


class OsTestCase(IfrTestCase):

    def setUp(self):
        # Method executed before each tests method
        pass

    def test_mk_dirs(self):
        new_dir_path = self.get_resource_path(OS_FILE_DIR, 'new_dir')
        ifr_os.mkdirs(new_dir_path, exist_ok=True)
        ifr_os.mkdirs(new_dir_path, exist_ok=True)
        self.assertTrue(os.path.isdir(new_dir_path))

        with self.assertRaises(OSError):
            ifr_os.mkdirs(new_dir_path, exist_ok=False)

        os.rmdir(new_dir_path)

    def test_execution_info(self):
        keys_list = ['pid', 'hostname', 'system', 'nb_proc', 'user']
        execution_info_result = ifr_os.execution_info()

        for key in keys_list:
            self.assertIn(key, execution_info_result)
            self.assertIsNotNone(execution_info_result[key])

    def test_rmdirs(self):

        sandbox_path = self.get_resource_path(OS_FILE_DIR, 'sandbox')

        do_not_delete_zip = ZipFile(self.get_resource_path(OS_FILE_DIR, 'do_not_delete.zip'))
        do_not_delete_zip.extractall(sandbox_path)

        try:
            my_path = os.path.join(sandbox_path, 'export', 'home', 'my_path')
            ifr_os.rmdirs(os.path.join(my_path, 'dir2', 'dir21', 'dir211'), stop_dirpath=my_path)
            self.assertFalse(os.path.isdir(os.path.join(my_path, 'dir2')))
            self.assertTrue(os.path.isdir(os.path.join(my_path, 'dir1', 'dir12')))
            self.assertTrue(os.path.isdir(os.path.join(my_path, 'dir1', 'dir13')))
            self.assertTrue(os.path.isfile(os.path.join(my_path, 'dir1', 'dir11', 'file1.txt')))

            ifr_os.rmdirs(os.path.join(my_path, 'dir1', 'dir12'), stop_dirpath=my_path)
            ifr_os.rmdirs(os.path.join(my_path, 'dir1', 'dir13'), stop_dirpath=my_path)
            os.remove(os.path.join(my_path, 'dir1', 'dir11', 'file1.txt'))
            ifr_os.rmdirs(os.path.join(my_path, 'dir1', 'dir11'), stop_dirpath=my_path)
            self.assertFalse(os.path.isdir(os.path.join(my_path, 'dir1')))
            self.assertTrue(os.path.isdir(my_path))
        finally:
            shutil.rmtree(sandbox_path, ignore_errors=True)


if __name__ == '__main__':
    import unittest
    unittest.main()
