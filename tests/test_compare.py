#!/usr/bin/env python
# -*- coding: utf-8 -*-
# pylint: disable=no-member

import tempfile
import os
from testfixtures import compare

from tests import IfrTestCase
from ifr_lib import ifr_compare, ifr_path


class CompareTestCase(IfrTestCase):

    # for each tests
    def setUp(self) -> None:
        txt_extension_filter = "*.txt"
        self.tmp_dir = ifr_path.Folder.from_parts(tempfile.gettempdir(), "ifr_lib", "ifr_compare").clean_or_create()
        self.dir1 = self.tmp_dir.new_folder("dir1").create()
        self.dir1.new_file("CFO_20200110T1520.nc").touch()
        self.dir1.new_file("CFO_20200110T1620.nc").touch()

        self.dir2 = self.tmp_dir.new_folder("dir2").create()
        self.dir2.new_file("CFO_20200110T1520.txt").touch()
        self.dir2.new_file("CFO_20200110T1620.txt").touch()

        self.dir3 = self.tmp_dir.new_folder("dir3").create()
        self.dir3.new_file("CFO_20200110T1520.other").touch()

        # build path finders
        self.pf1 = ifr_compare.PathFinder(self.dir1.path, ifr_path.BasenameFnmatchFilter("*.nc"))
        self.pf2 = ifr_compare.PathFinder(self.dir2.path, ifr_path.BasenameFnmatchFilter(txt_extension_filter))
        self.pf3 = ifr_compare.PathFinder(self.dir3.path, ifr_path.BasenameFnmatchFilter(txt_extension_filter))

        self.fake_pf = ifr_compare.PathFinder(
            os.path.join(self.dir3.path, "fake"),
            ifr_path.BasenameFnmatchFilter(txt_extension_filter)
        )

    # for each tests
    def tearDown(self) -> None:
        self.tmp_dir.rmtree()

    def test_number_operator(self):
        self.assertFalse(ifr_compare.BasicOperator("eq").eval(1, 2))
        self.assertTrue(ifr_compare.BasicOperator("eq").eval(1, 1))
        self.assertTrue(ifr_compare.BasicOperator("eq").eval(1, 1))

        self.assertFalse(ifr_compare.BasicOperator.GREATER_THAN.eval(1, 2))
        self.assertTrue(ifr_compare.BasicOperator.GREATER_THAN.eval(2, 1))

        self.assertTrue(ifr_compare.BasicOperator.LESS_THAN.eval(1, 2))
        self.assertFalse(ifr_compare.BasicOperator.LESS_THAN.eval(2, 1))

    def test_filelist_operator(self):
        self.assertTrue(ifr_compare.PathListOperator.MATCH_BASENAME.eval(self.pf1.paths, self.pf1.paths))
        self.assertFalse(ifr_compare.PathListOperator.MATCH_BASENAME.eval(self.pf1.paths, self.pf2.paths))
        self.assertFalse(ifr_compare.PathListOperator.MATCH_BASENAME.eval(self.pf1.paths, self.pf3.paths))

        self.assertTrue(
            ifr_compare.PathListOperator.MATCH_BASENAME_WITHOUT_EXTENSION.eval(self.pf1.paths, self.pf1.paths)
        )
        self.assertTrue(
            ifr_compare.PathListOperator.MATCH_BASENAME_WITHOUT_EXTENSION.eval(self.pf1.paths, self.pf2.paths)
        )
        self.assertFalse(
            ifr_compare.PathListOperator.MATCH_BASENAME_WITHOUT_EXTENSION.eval(self.pf1.paths, self.pf3.paths)
        )

    def test_filecount_comparator(self):
        self.assertTrue(ifr_compare.FileCountComparator(self.pf1, ifr_compare.BasicOperator.EQUAL_TO, 2).eval())
        self.assertTrue(ifr_compare.FileCountComparator(self.pf1, ifr_compare.BasicOperator.GREATER_THAN, 0).eval())
        self.assertTrue(ifr_compare.FileCountComparator(self.pf1, ifr_compare.BasicOperator.LESS_THAN, 3).eval())
        self.assertFalse(ifr_compare.FileCountComparator(self.pf1, ifr_compare.BasicOperator.EQUAL_TO, 4).eval())
        self.assertFalse(ifr_compare.FileCountComparator(self.fake_pf, ifr_compare.BasicOperator.EQUAL_TO, 4).eval())

    def test_filelist_comparator(self):

        self.assertTrue(
            ifr_compare.FileListComparator(self.pf1, ifr_compare.PathListOperator.MATCH_BASENAME, self.pf1).eval()
        )
        self.assertFalse(
            ifr_compare.FileListComparator(self.pf1, ifr_compare.PathListOperator.MATCH_BASENAME, self.pf2).eval()
        )
        self.assertFalse(
            ifr_compare.FileListComparator(self.pf1, ifr_compare.PathListOperator("mb"), self.pf3).eval()
        )

        self.assertTrue(
            ifr_compare.FileListComparator(
                self.pf1,
                ifr_compare.PathListOperator.MATCH_BASENAME_WITHOUT_EXTENSION,
                self.pf1
            ).eval()
        )

        self.assertTrue(
            ifr_compare.FileListComparator(
                self.pf1,
                ifr_compare.PathListOperator.MATCH_BASENAME_WITHOUT_EXTENSION,
                self.pf2
            ).eval()
        )
        self.assertFalse(
            ifr_compare.FileListComparator(
                self.pf1,
                ifr_compare.PathListOperator("mbwe"),
                self.pf3
            ).eval()
        )

        self.assertFalse(
            ifr_compare.FileListComparator(
                self.fake_pf,
                ifr_compare.PathListOperator("mbwe"),
                self.pf3
            ).eval()
        )

    def test_logical_comparator(self):

        self.assertTrue(
            ifr_compare.AndComparator(
                ifr_compare.FileListComparator(self.pf1, ifr_compare.PathListOperator.MATCH_BASENAME, self.pf1),
                ifr_compare.FileListComparator(
                    self.pf1,
                    ifr_compare.PathListOperator.MATCH_BASENAME_WITHOUT_EXTENSION,
                    self.pf1
                ),
                ifr_compare.FileListComparator(
                    self.pf1,
                    ifr_compare.PathListOperator.MATCH_BASENAME_WITHOUT_EXTENSION,
                    self.pf2
                )
            ).eval()
        )

        self.assertTrue(
            ifr_compare.OrComparator(
                ifr_compare.FileListComparator(self.pf1, ifr_compare.PathListOperator.MATCH_BASENAME, self.pf1),
                ifr_compare.FileListComparator(self.pf1, ifr_compare.PathListOperator.MATCH_BASENAME, self.pf2),
                ifr_compare.FileListComparator(self.pf1, ifr_compare.PathListOperator.MATCH_BASENAME, self.pf3),
            ).eval()
        )

        cp = ifr_compare.AndComparator()
        cp.append(ifr_compare.FileListComparator(self.pf1, ifr_compare.PathListOperator.MATCH_BASENAME, self.pf1))
        cp.append(ifr_compare.FileListComparator(self.pf1, ifr_compare.PathListOperator.MATCH_BASENAME, self.pf2))
        cp.append(ifr_compare.FileListComparator(self.pf1, ifr_compare.PathListOperator.MATCH_BASENAME, self.pf3))

        self.assertFalse(cp.eval())

        report = cp.report()
        self.assertTrue(report is not None and len(report) > 0)
        block_separator = "*" * 36

        print(block_separator)
        print(*report, sep="\n")
        print(block_separator)

        cp2 = ifr_compare.AndComparator(
            ifr_compare.FileListComparator(self.fake_pf, ifr_compare.PathListOperator.MATCH_BASENAME, self.pf1),
        )
        self.assertFalse(cp2.eval())
        report = cp2.report()
        self.assertTrue(report is not None and len(report) > 0)

        print(block_separator)
        print(*report, sep="\n")
        print(block_separator)

    def test_comparator_serialization(self):
        precondition_rule = ifr_compare.AndComparator()
        precondition_rule.append(ifr_compare.FileCountComparator(
            ifr_compare.PathFinder(
                folder='/home/folder1/%Y/%j',
                filter=ifr_path.BasenameRegexFilter('CFO_OPER_SWI_L1A*.nc')
            ),
            ifr_compare.BasicOperator("gt"),
            0
        ))

        precondition_rule.append(ifr_compare.FileCountComparator(
            ifr_compare.PathFinder(
                folder='/home/folder2',
                filter=ifr_path.BasenameFnmatchFilter('.*\\.nc')
            ),
            ifr_compare.BasicOperator("eq"),
            3
        ))

        precondition_rule.append(ifr_compare.FileListComparator(
            ifr_compare.PathFinder(
                folder='/home/folder3/%Y/%j',
                filter=ifr_path.AndFilter(
                    ifr_path.BasenameRegexFilter('CFO_OPER_SWI_L1A*.nc'),
                    ifr_path.TimeFilter(
                        field=ifr_path.TimeFilter.FILE_MTIME,
                        delta_value=10,
                        delta_unit=ifr_path.TimeFilter.DELTA_UNIT_MINUTES
                    )
                )
            ),
            ifr_compare.PathListOperator("mbwe"),
            ifr_compare.PathFinder(
                folder='/home/folder4/%Y/%j',
                filter=ifr_path.BasenameRegexFilter('CFO_OPER_SWI_L1A*.txt')
            )
        ))

        yaml1 = precondition_rule.to_yaml()
        comparator = ifr_compare.Comparator.from_yaml(yaml1)
        compare(precondition_rule, comparator)

        yaml2 = comparator.to_yaml()
        self.assertEqual(yaml1, yaml2)
        self.assertFalse(ifr_compare.Comparator.from_yaml(yaml2).eval())

        yaml3 = ifr_compare.OrComparator(
            ifr_compare.FileListComparator(self.pf1, ifr_compare.PathListOperator.MATCH_BASENAME, self.pf1),
            ifr_compare.FileListComparator(self.pf1, ifr_compare.PathListOperator.MATCH_BASENAME, self.pf2),
            ifr_compare.FileListComparator(self.pf1, ifr_compare.PathListOperator.MATCH_BASENAME, self.pf3),
        ).to_yaml()
        self.assertTrue(ifr_compare.Comparator.from_yaml(yaml3).eval())


if __name__ == '__main__':
    import unittest
    unittest.main()
