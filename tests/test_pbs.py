#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest

from ifr_lib.ifr_pbs import build_pbs_script, build_qsub_command, PbsOption


class PbsTestCase(unittest.TestCase):
    """
    This class is a set of tests for the methods of ifr_pbs.
    """

    def setUp(self):
        # Method executed before each tests method
        self.pbs_options_list = [
            PbsOption("pbs_queue", "-q", default_value='sequentiel'),
            PbsOption("pbs_mem", "-l mem", default_value='30G'),
            PbsOption("pbs_walltime", "-l walltime", default_value='00:00:05'),
            PbsOption("pbs_ncpus", "-l ncpus", default_value='14'),
            PbsOption("pbs_mail_addr", "-M", default_value='ebodere@ifremer.fr')
        ]
        self.expected_pbs_script = """#!/usr/bin/env bash
#PBS -q sequentiel
#PBS -l mem 30G
#PBS -l walltime 00:00:05
#PBS -l ncpus 14
#PBS -M ebodere@ifremer.fr

set -e

echo "Set cwd to my_working_dir"
cd my_working_dir

echo "Command - start"
echo 'Hello World'
echo "Command - end"

exit 0"""
        self.expected_pbs_command =\
            'qsub -v key1="value1",key2="value2",key3="value3" -o pbs_log_file_path_value pbs_file_path_value'

    def test_build_pbs_script(self):
        actual_pbs_script = build_pbs_script("echo 'Hello World'", "my_working_dir", self.pbs_options_list)
        self.assertEqual(actual_pbs_script, self.expected_pbs_script)

    def test_build_qsub_command(self):
        actual_qsub_command = build_qsub_command("pbs_file_path_value",
                                                 "pbs_log_file_path_value",
                                                 {'key1': 'value1', 'key2': 'value2', 'key3': 'value3'})
        self.assertEqual(actual_qsub_command, self.expected_pbs_command)
