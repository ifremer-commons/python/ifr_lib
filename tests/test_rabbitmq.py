#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest

from ifr_lib.ifr_rabbitmq import RabbitMQService, RabbitMQConfig


class RabbitMqTestCase(unittest.TestCase):

    def test_rabbitmq_publication(self):

        config = RabbitMQConfig(
            host="rabbitmq-cluster-val.ifremer.fr",
            port=5672,
            ssl=False,
            user="cersat-processing",
            password="Je dynamite un cordon bleu pour la CIA",
            virtual_host="cersat-processing",
            queue_name="cs-emm"
        )

        rmq = RabbitMQService(config)
        self.assertIsNotNone(rmq)

        rmq.connect_rabbitmq()
        self.assertTrue(rmq._RabbitMQService__connection.is_open)

        msg = {
            "core": "Adipisicing occaecat proident velit occaecat excepteur est aliquip est laboris "
                    "deserunt qui occaecat commodo nulla consequat ea est ut nulla eiusmod sit dolore."
        }
        rmq.publish_message(msg, "application/text")

        rmq.disconnect_rabbitmq()
        self.assertTrue(rmq._RabbitMQService__connection.is_closed)


if __name__ == '__main__':
    unittest.main()
