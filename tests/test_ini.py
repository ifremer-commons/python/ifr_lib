#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ifr_lib import ifr_ini
from tests import IfrTestCase


class IniTestCase(IfrTestCase):

    def test_dict_to_ini(self):
        data1 = {
            "key1": {
                "key3": "value3",
                "key2": "value2"
            }
        }
        self.assertEqual(data1, ifr_ini.ini_to_dict(ifr_ini.dict_to_ini(data1)))

        data2 = {
            "key1": {
                "key3": "value3",
                "key2": {
                    "key4": "value"
                },
                "key5": "value"
            }
        }
        self.assertEqual(data2, ifr_ini.ini_to_dict(ifr_ini.dict_to_ini(data2)))


if __name__ == '__main__':
    import unittest
    unittest.main()
