#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ifr_lib import ifr_soap
from tests import IfrTestCase

SOAP_URL = 'http://seadatanet.maris2.nl/ws/ws_edmo.asmx?wsdl'
SOAP_WRONG_URL = 'http://seadatanet.maris2.nl/ws/ws_edmo.asmx?wsd'
SOAP_NON_EXISTING_URL = 'http://non_existing?wsdl'
SOAP_WRONG_WS = 'http://seadatanet.maris2.nl/ws/ws_wrong.asmx?wsdl'


class SoapClientTestCase(IfrTestCase):

    def setUp(self):
        # Method executed before each tests method
        pass

    def test_create_client(self):
        service = ifr_soap.create_client(SOAP_URL)
        result = service.ws_edmo_get_list()
        self.assertIsNotNone(result)

    def test_create_client_wrong_url(self):
        with self.assertRaises(ifr_soap.SoapException):
            ifr_soap.create_client(SOAP_WRONG_URL)  # ZeepXMLSyntaxError

    def test_create_client_non_existing_url(self):
        with self.assertRaises(ifr_soap.SoapException):
            ifr_soap.create_client(SOAP_NON_EXISTING_URL)  # ConnectionError

    def test_create_client_wrong_ws(self):
        with self.assertRaises(ifr_soap.SoapException):
            ifr_soap.create_client(SOAP_WRONG_WS)  # HttpError

    def test_create_client_non_existing_method(self):
        with self.assertRaises(AttributeError):
            service = ifr_soap.create_client(SOAP_URL)
            service.ws_non_existing_method()


if __name__ == '__main__':
    import unittest
    unittest.main()
