#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import shutil

from ifr_lib import ifr_zip
from tests import IfrTestCase

TEST_ZIP_FILE = "tests.zip"
FIRST_FILE = "1.txt"
SECOND_FILE = "2.txt"


class ZipTestCase(IfrTestCase):

    def setUp(self) -> None:
        # Method executed before each tests method
        if os.path.exists(self.get_resource_path("zip_test_files", TEST_ZIP_FILE)):
            os.remove(self.get_resource_path("zip_test_files", TEST_ZIP_FILE))

        if os.path.exists(self.get_resource_path("zip_test_files", "do_not_remove.zip")):
            shutil.copy2(self.get_resource_path("zip_test_files", "do_not_remove.zip"),
                         os.path.join(self.get_resource_path("zip_test_files"), TEST_ZIP_FILE))

    def get_zip_size(self):
        return os.path.getsize(self.get_resource_path("zip_test_files", TEST_ZIP_FILE))

    def tearDown(self) -> None:
        if os.path.exists(self.get_resource_path("zip_test_files", TEST_ZIP_FILE)):
            os.remove(self.get_resource_path("zip_test_files", TEST_ZIP_FILE))

    def test_add_to_zip(self):

        ifr_zip.add_to_zip(
            os.path.join(self.get_resource_path("zip_test_files"), TEST_ZIP_FILE),
            self.get_resource_path("zip_test_files", FIRST_FILE),
            self.get_resource_path("zip_test_files", SECOND_FILE)
        )
        size_after2 = self.get_zip_size()
        ifr_zip.add_to_zip(
            self.get_resource_path("zip_test_files", TEST_ZIP_FILE),
            self.get_resource_path("zip_test_files", "3.txt")
        )
        size_after3 = self.get_zip_size()
        self.assertGreater(size_after3, size_after2)

    def test_remove_from_zip(self):
        ifr_zip.add_to_zip(
            os.path.join(self.get_resource_path("zip_test_files"), TEST_ZIP_FILE),
            self.get_resource_path("zip_test_files", FIRST_FILE),
            self.get_resource_path("zip_test_files", SECOND_FILE),
            self.get_resource_path("zip_test_files", "3.txt")
        )
        size_before = self.get_zip_size()
        ifr_zip.remove_from_zip(
            self.get_resource_path("zip_test_files", TEST_ZIP_FILE),
            FIRST_FILE,
            SECOND_FILE
        )
        size_after = self.get_zip_size()
        self.assertLess(size_after, size_before)

    def test_add_or_update_to_zip(self):
        size_before = self.get_zip_size()
        ifr_zip.add_or_update_to_zip(
            self.get_resource_path("zip_test_files", TEST_ZIP_FILE),
            self.get_resource_path("zip_test_files", FIRST_FILE),
            self.get_resource_path("zip_test_files", SECOND_FILE)
        )
        size_after = self.get_zip_size()
        self.assertGreater(size_after, size_before)


if __name__ == '__main__':
    import unittest
    unittest.main()
