#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests

from ifr_lib import ifr_http
from tests import IfrTestCase
import os

CAS_HOST = 'auth.ifremer.fr'
REST_ENDPOINT = '/v1/tickets/'
CAS_USER = os.environ.get("CAS_USER")
CAS_PWD = os.environ.get("CAS_PWD")
SERVICE_URL = 'https://www.ifremer.fr/testcas/test-extranet.php'


class HttpClientTestCase(IfrTestCase):

    def test_cas_client(self):

        # skip tests if password is unset (avoid to set password in git)
        if CAS_PWD is None and CAS_USER is None:
            print("No credentials filled in !")
            self.assertTrue(True)
            return

        cas_client = ifr_http.CasClient(
            cas_host=CAS_HOST,
            rest_endpoint=REST_ENDPOINT,
            username=CAS_USER,
            password=CAS_PWD,
            service_url=SERVICE_URL
        )

        cas_client.connect()
        self.assertTrue(cas_client.is_authenticated())

        self.assertTrue(cas_client.ticket_granting_ticket() is not None)
        self.assertTrue(cas_client.service_ticket() is not None)

        response = cas_client.get(SERVICE_URL)

        self.assertTrue(response.status_code == requests.codes.ok)
        self.assertTrue(CAS_USER in response.content.decode("utf8"))

        cas_client.disconnect()
        self.assertFalse(cas_client.is_authenticated())


if __name__ == '__main__':
    import unittest
    unittest.main()

