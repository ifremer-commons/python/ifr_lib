#!/usr/bin/env python
# -*- coding: utf-8 -*-

import filecmp
import os
import tempfile

from lxml import etree

from ifr_lib import ifr_xml, ifr_exception
from tests import IfrTestCase

# ##############################################
# CONSTANTS
# ##############################################
# The tests resources directory
RESOURCES_DIR = os.path.join(os.path.dirname(__file__), "resources")

# The directory of the tests files used to tests xml from ifr_lib
CD_CATALOG_FILE_PATH = os.path.join(RESOURCES_DIR, "xml_test_files/cd_catalog.xml")
EXPECTED_JSON = "{\n" \
                "    \"catalog\": {\n" \
                "        \"CD\": [\n" \
                "            {\n" \
                "                \"TITLE\": \"Empire Burlesque\",\n" \
                "                \"ARTIST\": \"Bob Dylan\",\n" \
                "                \"COUNTRY\": \"USA\",\n" \
                "                \"COMPANY\": \"Columbia\",\n" \
                "                \"PRICE\": \"10.90\",\n" \
                "                \"YEAR\": \"1985\"\n" \
                "            },\n" \
                "            {\n" \
                "                \"TITLE\": \"Hide your heart\",\n" \
                "                \"ARTIST\": \"Bonnie Tylor\",\n" \
                "                \"COUNTRY\": \"UK\",\n" \
                "                \"COMPANY\": \"CBS Records\",\n" \
                "                \"PRICE\": \"9.90\",\n" \
                "                \"YEAR\": \"1988\"\n            },\n" \
                "            {\n" \
                "                \"TITLE\": \"Greatest Hits\",\n" \
                "                \"ARTIST\": \"Dolly Parton\",\n" \
                "                \"COUNTRY\": \"USA\",\n" \
                "                \"COMPANY\": \"RCA\",\n" \
                "                \"PRICE\": \"9.90\",\n" \
                "                \"YEAR\": \"1982\"\n" \
                "            }\n" \
                "        ]\n" \
                "    }\n" \
                "}"


class XMLTestCase(IfrTestCase):
    """
    This class is a set of tests for the XML methods of ifr_lib.
    """

    def setUp(self):
        # Method executed before each tests method
        pass

    def test_build_xml_node(self):
        book_node = ifr_xml.build_xml_node("book", "Learning Cooking TOME 1", {"id": "1", "category": "cooking"})
        self.assertEqual(book_node.tag, "book")
        self.assertEqual(book_node.text, "Learning Cooking TOME 1")
        self.assertEqual(book_node.attrib["category"], "cooking")
        self.assertEqual(book_node.attrib["id"], "1")

        with self.assertRaises(ifr_exception.MissingParameterException):
            ifr_xml.build_xml_node(node_name=None)

    def test_add_sub_nodes(self):
        book_node = ifr_xml.build_xml_node("book", "Learning Cooking TOME 2", {"category": "cooking", "id": "1"})
        title_node = ifr_xml.build_xml_node("title", "Everyday Italian", {"lang": "en"})
        author_node = ifr_xml.build_xml_node("author", "Giada De Laurentiis")
        year_node = ifr_xml.build_xml_node("year", "2005")
        price_node = ifr_xml.build_xml_node("price", "29.99")

        ifr_xml.add_sub_nodes(book_node, [title_node, author_node, year_node, price_node])
        expected = "<book category=\"cooking\" id=\"1\">Learning Cooking TOME 2" \
            "<title lang=\"en\">Everyday Italian</title>" \
            "<author>Giada De Laurentiis</author>" \
            "<year>2005</year>" \
            "<price>29.99</price>" \
                   "</book>"

        try:
            self.assertEqual(etree.tostring(book_node), bytes(expected, "utf8"))
        except TypeError:
            self.assertEqual(etree.tostring(book_node), bytes(expected))

        with self.assertRaises(ifr_exception.MissingParameterException):
            ifr_xml.add_sub_nodes(book_node, None)

    def test_read_non_existing_xml_file(self):
        with self.assertRaises(ifr_exception.FileNotFoundException):
            ifr_xml.read_xml_file("not_found.xml")

    def test_read_malformed_xml_file(self):
        with self.assertRaises(ifr_exception.SyntaxException):
            ifr_xml.read_xml_file(os.path.join(RESOURCES_DIR, "xml_test_files", "malformed_xml.xml"))

    def test_read_xml_file_with_comments(self):
        result_with_comments = ifr_xml.read_xml_file(CD_CATALOG_FILE_PATH, remove_comments=False)
        expected_with_comments = "<catalog>\n" \
                                 "    <!-- This is the first comment -->\n" \
                                 "    <CD>\n" \
                                 "        <TITLE>Empire Burlesque</TITLE>\n" \
                                 "        <ARTIST>Bob Dylan</ARTIST>\n" \
                                 "        <COUNTRY>USA</COUNTRY>\n" \
                                 "        <COMPANY>Columbia</COMPANY>\n" \
                                 "        <PRICE>10.90</PRICE>\n" \
                                 "        <YEAR>1985</YEAR>\n" \
                                 "    </CD>\n" \
                                 "    <!-- This is the second comment -->\n" \
                                 "    <CD>\n" \
                                 "        <TITLE>Hide your heart</TITLE>\n" \
                                 "        <ARTIST>Bonnie Tylor</ARTIST>\n" \
                                 "        <COUNTRY>UK</COUNTRY>\n" \
                                 "        <COMPANY>CBS Records</COMPANY>\n" \
                                 "        <PRICE>9.90</PRICE>\n" \
                                 "        <YEAR>1988</YEAR>\n" \
                                 "    </CD>\n" \
                                 "    <!-- This is the third comment -->\n" \
                                 "    <CD>\n" \
                                 "        <TITLE>Greatest Hits</TITLE>\n" \
                                 "        <ARTIST>Dolly Parton</ARTIST>\n" \
                                 "        <COUNTRY>USA</COUNTRY>\n" \
                                 "        <COMPANY>RCA</COMPANY>\n" \
                                 "        <PRICE>9.90</PRICE>\n" \
                                 "        <YEAR>1982</YEAR>\n" \
                                 "    </CD>\n</catalog>"
        try:
            self.assertEqual(etree.tostring(result_with_comments), bytes(expected_with_comments, "utf-8"))
        except TypeError:
            self.assertEqual(etree.tostring(result_with_comments), bytes(expected_with_comments))

    def test_read_xml_file_without_comments(self):
        result_without_comments = ifr_xml.read_xml_file(CD_CATALOG_FILE_PATH, remove_comments=True)
        expected_without_comments = "<catalog>\n" \
                                    "    \n" \
                                    "    <CD>\n" \
                                    "        <TITLE>Empire Burlesque</TITLE>\n" \
                                    "        <ARTIST>Bob Dylan</ARTIST>\n" \
                                    "        <COUNTRY>USA</COUNTRY>\n" \
                                    "        <COMPANY>Columbia</COMPANY>\n" \
                                    "        <PRICE>10.90</PRICE>\n" \
                                    "        <YEAR>1985</YEAR>\n" \
                                    "    </CD>\n" \
                                    "    \n" \
                                    "    <CD>\n" \
                                    "        <TITLE>Hide your heart</TITLE>\n" \
                                    "        <ARTIST>Bonnie Tylor</ARTIST>\n" \
                                    "        <COUNTRY>UK</COUNTRY>\n" \
                                    "        <COMPANY>CBS Records</COMPANY>\n" \
                                    "        <PRICE>9.90</PRICE>\n" \
                                    "        <YEAR>1988</YEAR>\n" \
                                    "    </CD>\n" \
                                    "    \n" \
                                    "    <CD>\n" \
                                    "        <TITLE>Greatest Hits</TITLE>\n" \
                                    "        <ARTIST>Dolly Parton</ARTIST>\n" \
                                    "        <COUNTRY>USA</COUNTRY>\n" \
                                    "        <COMPANY>RCA</COMPANY>\n" \
                                    "        <PRICE>9.90</PRICE>\n" \
                                    "        <YEAR>1982</YEAR>\n" \
                                    "    </CD>\n</catalog>"
        try:
            self.assertEqual(etree.tostring(result_without_comments), bytes(expected_without_comments))
        except TypeError:
            self.assertEqual(etree.tostring(result_without_comments), bytes(expected_without_comments, "utf8"))

    def test_write_xml_file(self):
        # tempfile.NamedTemporaryFile create and open the file
        writing_test_file_path = os.path.join(
            tempfile.gettempdir(),
            next(tempfile._get_candidate_names()) + ".xml"
        )

        xml_content = ifr_xml.read_xml_file(CD_CATALOG_FILE_PATH, remove_comments=False)
        ifr_xml.write_xml_file(
            writing_test_file_path,
            xml_content,
            encoding="utf8",
            xml_declaration=True,
            pretty_print=True
        )

        self.assertTrue(filecmp.cmp(CD_CATALOG_FILE_PATH, writing_test_file_path))

    def test_xml_to_json(self):
        xml_content = ifr_xml.read_xml_file(CD_CATALOG_FILE_PATH, remove_comments=False)
        xml_json = ifr_xml.xml_to_json(xml_content, indent=4)
        try:
            self.assertEqual(bytes(xml_json), bytes(EXPECTED_JSON))
        except TypeError:
            self.assertEqual(bytes(xml_json, "utf8"), bytes(EXPECTED_JSON, "utf8"))

    def test_conversion_exception_xml_to_json(self):
        with self.assertRaises(ifr_exception.ConversionException):
            ifr_xml.xml_to_json(None, indent=4)

    def test_xml_file_to_json(self):
        xml_json = ifr_xml.xml_file_to_json(CD_CATALOG_FILE_PATH, indent=4)
        try:
            self.assertEqual(bytes(xml_json), bytes(EXPECTED_JSON))
        except TypeError:
            self.assertEqual(bytes(xml_json, "utf8"), bytes(EXPECTED_JSON, "utf8"))

    def test_non_existing_xml_file_to_json(self):
        with self.assertRaises(ifr_exception.FileNotFoundException):
            ifr_xml.xml_file_to_json("not_found.xml", indent=4)

    def test_xml_values_list(self):
        xml_result = ifr_xml.read_xml_file(
            os.path.join(RESOURCES_DIR, "xml_test_files/xml_values_list_test.xml"),
            remove_comments=True
        )
        expected_values_list = sorted([
            'INTERNET', 'GEOCATALOGUE', 'DCSMM_INVENTAIRE_DISPOSITIFS_PDS',
            'DCSMM_DISPOSITIFS_RETENUS_PDS', 'ODATIS'
        ])
        values_list = sorted(ifr_xml.xml_values_list(xml_result, 'publishInGroup'))
        self.assertListEqual(values_list, expected_values_list)

        self.assertListEqual(ifr_xml.xml_values_list(xml_result, 'non_existing_tag'), [])
        self.assertListEqual(ifr_xml.xml_values_list(xml_result, ''), [])


if __name__ == "__main__":
    import unittest
    unittest.main()
