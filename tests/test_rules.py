#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ifr_lib import ifr_rules
from tests import IfrTestCase


class RulesTestCase(IfrTestCase):

    def test_rules(self):

        self.assertDictEqual(
            ifr_rules.process_rule(
                self.get_resource_path("rules_test_files", "rules.yaml"),
                request_host="ftp.ifremer.fr",
                request_path="/ifremer/argo/etc/abc.txt",
                user_name="anonymous"
            ),
            {
                'domain': 'ftp.ifremer.fr',
                'site': 'argo'
            }
        )

        self.assertDictEqual(
            ifr_rules.process_rule(
                self.get_resource_path("rules_test_files", "rules.yaml"),
                request_host="ftp.ifremer.fr",
                request_path="/etc/abc.txt",
                user_name="smoswind"
            ),
            {
                'domain': 'ftp.ifremer.fr',
                'site': 'smoswind'
            }
        )

        self.assertDictEqual(
            ifr_rules.process_rule(
                self.get_resource_path("rules_test_files", "rules.yaml"),
                request_host="w3.ifremer.fr",
                request_path="/sih/tst.html",
                user_name="none"
            ),
            {
                'domain': 'w3.ifremer.fr',
                'site': 'sih'
            }
        )


if __name__ == '__main__':
    import unittest
    unittest.main()
