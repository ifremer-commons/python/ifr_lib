#!/usr/bin/env python
# -*- coding: utf-8 -*-


import logging

from ifr_lib import ifr_logging, ifr_str
from tests import IfrTestCase


class LoggingTestCase(IfrTestCase):

    log = logging.getLogger(__name__)

    def test_logging_level_name(self):
        self.assertEqual(ifr_logging.level_name("WARN"), "WARNING")
        self.assertEqual(ifr_logging.level_name("WARNING"), "WARNING")
        self.assertEqual(ifr_logging.level_name(logging.WARNING), "WARNING")
        self.assertEqual(ifr_logging.level_name(30), "WARNING")
        self.assertEqual(ifr_logging.level_name("30"), "Level 30")

    def test_logging_level_code(self):
        self.assertEqual(ifr_logging.level_code("WARN"), logging.WARNING)
        self.assertEqual(ifr_logging.level_code("WARNING"), logging.WARNING)
        self.assertEqual(ifr_logging.level_code(logging.WARNING), logging.WARNING)
        self.assertEqual(ifr_logging.level_code('FAKE'), logging.NOTSET)

    def test_setup_logging(self):
        ifr_logging.setup_logging(root_level='DEBUG')
        self.log.debug("debug message")
        self.log.info("info message")
        self.log.warning("warning message")
        self.log.error("error message")
        self.log.critical("critical message")

    def test_logging_ajust(self):
        logging_conf = {
            'formatters': {
                'console_fmt': {
                    'fmt': '%(purple)s%(asctime)s%(reset)s | '
                           '%(log_color)s%(levelname).3s%(reset)s | '
                           '%(message_log_color)s%(message)-80s%(reset)s |'
                           '%(thin_white)s%(filename)s:%(lineno)d@%(funcName)s()%(reset)s'
                }
            }
        }
        ifr_logging.setup_logging(config=logging_conf)
        msg = "Message to print"
        self.log.info(f"info message : {msg}")
        self.log.info("*" * 90)
        self.log.info(ifr_str.colorize(f"info message : {msg}", "bold_blue", 80))
        self.log.info(f"info message : {msg}")


if __name__ == '__main__':
    import unittest
    unittest.main()
