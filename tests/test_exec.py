#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from ifr_lib import ifr_exec, ifr_exception
from tests import IfrTestCase


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()

cpt = 0


def failed():
    raise Exception("Failed function")


def success():
    return True


def random_success(value):
    global cpt
    cpt += 1
    res = ((value * cpt) % 2)
    if res != 0:
        raise ifr_exception.GenericException("Random exception !")
    return res


class MiscTestCase(IfrTestCase):
    """
    This class is a set of tests for the ifr_misc methods of ifr_lib.
    """

    def test_retry_block(self):

        with self.assertRaises(Exception):
            ifr_exec.retry_block(
                main_function=failed,
                logger=logger,
                retry_config=ifr_exec.RetryConfig(
                    extra_fix_delay=0.5,
                    nb_tries=4
                )
            )

        self.assertTrue(ifr_exec.retry_block(main_function=success, logger=logger))

        self.assertTrue(ifr_exec.retry_block(main_function=random_success, value=1, logger=logger) == 0)


if __name__ == '__main__':
    import unittest
    unittest.main()
