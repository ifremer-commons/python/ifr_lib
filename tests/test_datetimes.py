#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tests import IfrTestCase
from ifr_lib import ifr_datetimes, ifr_exception
import datetime

# ##############################################
# CONSTANTS
# ##############################################

# The date to tests ifr_lib datetimes methods
TEST_DATE = '20180424'
# The expected day of year for the tests date 20180424
EXPECTED_DAY_OF_YEAR = '114'
# The expected year for the tests date 20180424
EXPECTED_YEAR = '2018'
# The expected month for the tests date 20180424
EXPECTED_MONTH = '04'
# The expected dya for the tests date 20180424
EXPECTED_DAY = '24'
# The expected previous month for the tests date 20180124
EXPECTED_PREVIOUS_MONTH = '03'
# The expected year for the tests date 20180424
EXPECTED_SHORT_YEAR = '18'
# The elapsed time start date
ELAPSED_TIME_START_DATE = '2018-02-01T14:28:42'
# The elapsed time end date
ELAPSED_TIME_END_DATE = '2018-08-31T07:48:12'
# The expected elapsed time in days
EXPECTED_ELAPSED_TIME_DAYS = 210
DASHED_FULL_DATE_PATTERN = '%Y-%m-%dT%H:%M:%S'
DASHED_DATE_PATTERN = '%Y-%m-%d'
SHORT_DATE_PATTERN = '%Y%m%d'


class DatetimesTestCase(IfrTestCase):

    def test_datetime_to_str(self):
        date = ifr_datetimes.str_to_datetime('2018-02-03T14:28:42', pattern=DASHED_FULL_DATE_PATTERN)
        self.assertEqual(ifr_datetimes.datetime_to_str(date, pattern='%d/%m/%Y'), '03/02/2018')
        self.assertEqual(ifr_datetimes.datetime_to_str(ifr_datetimes.now(), pattern='foo'), 'foo')
        with self.assertRaises(ifr_exception.ConversionException):
            ifr_datetimes.datetime_to_str(ifr_datetimes.now(), pattern=123)

    def test_get_day_in_year(self):
        self.assertEqual(
            ifr_datetimes.day_in_year(ifr_datetimes.str_to_datetime(TEST_DATE, pattern=SHORT_DATE_PATTERN)),
            EXPECTED_DAY_OF_YEAR
        )

    def test_get_year(self):
        self.assertEqual(
            ifr_datetimes.year(ifr_datetimes.str_to_datetime(TEST_DATE, pattern=SHORT_DATE_PATTERN)),
            EXPECTED_YEAR
        )

    def test_get_month(self):
        self.assertEqual(
            ifr_datetimes.month(ifr_datetimes.str_to_datetime(TEST_DATE, pattern=SHORT_DATE_PATTERN)),
            EXPECTED_MONTH
        )

    def test_get_day(self):
        self.assertEqual(
            ifr_datetimes.day(ifr_datetimes.str_to_datetime(TEST_DATE, pattern=SHORT_DATE_PATTERN)),
            EXPECTED_DAY
        )

    def test_previous_month(self):
        self.assertEqual(
            ifr_datetimes.previous_month(ifr_datetimes.str_to_datetime(TEST_DATE, pattern=SHORT_DATE_PATTERN)),
            EXPECTED_PREVIOUS_MONTH
        )

    def test_short_year(self):
        self.assertEqual(
            ifr_datetimes.short_year(ifr_datetimes.str_to_datetime(TEST_DATE, pattern=SHORT_DATE_PATTERN)),
            EXPECTED_SHORT_YEAR
        )

    def test_elapsed_time(self):
        self.assertEqual(
            ifr_datetimes.elapsed_time(
                ifr_datetimes.str_to_datetime(ELAPSED_TIME_START_DATE, pattern=DASHED_FULL_DATE_PATTERN),
                ifr_datetimes.str_to_datetime(ELAPSED_TIME_END_DATE, pattern=DASHED_FULL_DATE_PATTERN)
            ).days,
            EXPECTED_ELAPSED_TIME_DAYS
        )

    def test_str_to_datetime(self):
        test_date = '2018-02-04T14:28:42'
        self.assertEqual(ifr_datetimes.str_to_datetime(test_date, pattern=DASHED_FULL_DATE_PATTERN).year, 2018)
        self.assertEqual(
            ifr_datetimes.month(
                ifr_datetimes.str_to_datetime(test_date, pattern=DASHED_FULL_DATE_PATTERN)
            ),
            '02'
        )
        self.assertEqual(
            ifr_datetimes.day(ifr_datetimes.str_to_datetime(test_date, pattern=DASHED_FULL_DATE_PATTERN)),
            '04'
        )
        self.assertEqual(ifr_datetimes.str_to_datetime(test_date, pattern=DASHED_FULL_DATE_PATTERN).hour, 14)
        self.assertEqual(ifr_datetimes.str_to_datetime(test_date, pattern=DASHED_FULL_DATE_PATTERN).minute, 28)
        self.assertEqual(ifr_datetimes.str_to_datetime(test_date, pattern=DASHED_FULL_DATE_PATTERN).second, 42)
        with self.assertRaises(ifr_exception.ConversionException):
            ifr_datetimes.str_to_datetime(test_date, pattern='foo')

    def test_datetime_to_iso(self):
        test_date = ifr_datetimes.str_to_datetime("20201008T160000")
        self.assertEqual(
            ifr_datetimes.datetime_to_iso(test_date),
            '20201008T160000'
        )

    def test_previous_day(self):
        self.assertEqual(
            ifr_datetimes.elapsed_time(
                ifr_datetimes.previous_day(ifr_datetimes.now()),
                ifr_datetimes.now()
            ).days,
            1
        )

    def test_year_month(self):
        separator = '%'
        self.assertEqual(
            ifr_datetimes.year_month(ifr_datetimes.now(), separator),
            ifr_datetimes.year(ifr_datetimes.now()) + separator + ifr_datetimes.month(ifr_datetimes.now())
        )

    def test_previous_year_month(self):
        self.assertEqual(
            ifr_datetimes.previous_year_month(
                ifr_datetimes.str_to_datetime('2018-02-05T14:28:42', pattern=DASHED_FULL_DATE_PATTERN),
                ''
            ),
            '201801'
        )

        self.assertEqual(
            ifr_datetimes.previous_year_month(
                ifr_datetimes.str_to_datetime('2019-01-01T14:28:42', pattern=DASHED_FULL_DATE_PATTERN),
                ''
            ),
            '201812'
        )

    def test_month_range(self):
        first, last = ifr_datetimes.month_range(
            ifr_datetimes.str_to_datetime('2020-02-15T00:00:00', pattern=DASHED_FULL_DATE_PATTERN)
        )
        self.assertEqual(ifr_datetimes.datetime_to_str(first, pattern='%Y/%m/%d'), '2020/02/01')
        self.assertEqual(ifr_datetimes.datetime_to_str(last, pattern='%Y/%m/%d'), '2020/02/29')

    def test_previous_year(self):
        self.assertEqual(
            ifr_datetimes.previous_year(ifr_datetimes.str_to_datetime('2020-02-29', pattern=DASHED_DATE_PATTERN)),
            '2019'
        )
        self.assertEqual(
            ifr_datetimes.previous_year(ifr_datetimes.str_to_datetime('0778-02-01', pattern=DASHED_DATE_PATTERN)),
            '0777'
        )

    def test_add_years(self):
        self.assertEqual(
            ifr_datetimes.add_years(ifr_datetimes.str_to_datetime('2019-01-04', pattern=DASHED_DATE_PATTERN), 5),
            ifr_datetimes.str_to_datetime('2024-01-04', pattern=DASHED_DATE_PATTERN)
        )
        self.assertEqual(
            ifr_datetimes.add_years(ifr_datetimes.str_to_datetime('2020-02-29', pattern=DASHED_DATE_PATTERN), 3),
            ifr_datetimes.str_to_datetime('2023-02-28', pattern=DASHED_DATE_PATTERN)
        )

    def test_sub_years(self):
        self.assertEqual(
            ifr_datetimes.sub_years(ifr_datetimes.str_to_datetime('2019-01-04', pattern=DASHED_DATE_PATTERN), 5),
            ifr_datetimes.str_to_datetime('2014-01-04', pattern=DASHED_DATE_PATTERN)
        )
        self.assertEqual(
            ifr_datetimes.sub_years(ifr_datetimes.str_to_datetime('2020-02-29', pattern=DASHED_DATE_PATTERN), 3),
            ifr_datetimes.str_to_datetime('2017-02-28', pattern=DASHED_DATE_PATTERN)
        )

    def test_parse_date(self):
        date = ifr_datetimes.str_to_datetime('2018-02-07T14:28:42', pattern=DASHED_FULL_DATE_PATTERN)
        with self.assertRaises(ifr_exception.ConversionException):
            ifr_datetimes.parse_date("aaa")
            ifr_datetimes.parse_date("20180207T14:28:42")

        self.assertEqual(ifr_datetimes.parse_date("2018-02-07T14:28:42"), date)
        self.assertEqual(ifr_datetimes.parse_date("20180207T14:28:42", date_formats='%Y%m%dT%H:%M:%S'), date)
        self.assertEqual(ifr_datetimes.parse_date("20180207T14:28:42", date_formats=['%Y%m%dT%H:%M:%S']), date)

    def test_days_between(self):
        self.assertListEqual(
            ifr_datetimes.days_between(
                ifr_datetimes.str_to_datetime("20191228", '%Y%m%d'),
                ifr_datetimes.str_to_datetime("20200103", '%Y%m%d')
            ),
            [
                datetime.datetime(2019, 12, 28, 0, 0),
                datetime.datetime(2019, 12, 29, 0, 0),
                datetime.datetime(2019, 12, 30, 0, 0),
                datetime.datetime(2019, 12, 31, 0, 0),
                datetime.datetime(2020, 1, 1, 0, 0),
                datetime.datetime(2020, 1, 2, 0, 0),
                datetime.datetime(2020, 1, 3, 0, 0)
            ]
        )

        with self.assertRaises(AssertionError):
            ifr_datetimes.days_between(
                ifr_datetimes.str_to_datetime("20200103", '%Y%m%d'),
                ifr_datetimes.str_to_datetime("20191228", '%Y%m%d')
            )

    def test_is_time_older_or_younger_than(self):
        # now - 8 days
        ref_time = (ifr_datetimes.now() - datetime.timedelta(days=8)).timestamp()

        # is ref time > 1 minutes
        self.assertTrue(ifr_datetimes.is_time_older_or_younger_than(ref_time, 1, 'minutes'))

        # is ref time < 10 days
        self.assertTrue(ifr_datetimes.is_time_older_or_younger_than(ref_time, -10, 'days'))

        # is ref time < 5 days
        self.assertFalse(ifr_datetimes.is_time_older_or_younger_than(ref_time, -5, 'days'))

        # is ref time > 5 days
        self.assertTrue(ifr_datetimes.is_time_older_or_younger_than(ref_time, 5, 'days'))

        with self.assertRaises(ifr_exception.BadParameterException):
            ifr_datetimes.is_time_older_or_younger_than(ref_time, 5, 'foo')

    def test_get_date(self):
        date = ifr_datetimes.get_date(
            'filenamesuffix_20180201_filenameprefix.extension',
            r"(?:^|[^\d])(?P<date>\d{8})(?:$|[^\d])"
        )
        self.assertIsInstance(date, datetime.date)
        self.assertEqual(date.year, 2018)
        self.assertEqual(date.month, 2)
        self.assertEqual(date.day, 1)

    def test_get_datetime(self):
        date = ifr_datetimes.get_datetime(
            'filenamesuffix_20180201_filenameprefix.extension',
            r".*([0-9]{8}).*"
        )
        self.assertIsInstance(date, datetime.datetime)
        self.assertEqual(date.year, 2018)
        self.assertEqual(date.month, 2)
        self.assertEqual(date.day, 1)

        with self.assertRaises(ifr_exception.PatternException):
            ifr_datetimes.get_datetime(
                'filenamesuffix_20190201_filenameprefix.extension',
                r".*([0-9]{10}).*"
            )


if __name__ == '__main__':
    import unittest
    unittest.main()
