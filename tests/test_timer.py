#!/usr/bin/env python
# -*- coding: utf-8 -*-


import datetime
import time

from ifr_lib import ifr_timer
from tests import IfrTestCase


class TimerTestCase(IfrTestCase):

    def test_timer_start(self):
        my_timer = ifr_timer.Timer()
        self.assertIsNone(my_timer.start_datetime)
        self.assertIsNone(my_timer.end_datetime)
        self.assertIsNone(my_timer.duration)

        self.assertTrue(my_timer.start())
        self.assertAlmostEqual((my_timer.start_datetime - datetime.datetime.now()).total_seconds(), 0.0, delta=0.001)
        self.assertIsNone(my_timer.end_datetime)
        self.assertIsNone(my_timer.duration)
        self.assertFalse(my_timer.start())

    def test_timer_stop(self):
        date_start = datetime.datetime.now()

        my_timer = ifr_timer.Timer()
        my_timer.start()

        time.sleep(0.21)

        self.assertTrue(my_timer.stop())
        self.assertAlmostEqual((my_timer.start_datetime - date_start).total_seconds(), 0.0, delta=0.1)
        self.assertAlmostEqual((my_timer.end_datetime - datetime.datetime.now()).total_seconds(), 0.0, delta=0.1)
        self.assertAlmostEqual(my_timer.duration.total_seconds(), 0.21, delta=0.1)

        self.assertFalse(my_timer.stop())

    def test_timer_reset(self):

        # reset on a non started timer
        my_timer = ifr_timer.Timer()
        my_timer.reset()
        self.assertIsNone(my_timer.start_datetime)
        self.assertIsNone(my_timer.end_datetime)
        self.assertIsNone(my_timer.duration)

        # reset on a started timer
        my_timer.start()
        self.assertIsNotNone(my_timer.start_datetime)
        self.assertIsNone(my_timer.end_datetime)
        self.assertIsNone(my_timer.duration)
        my_timer.reset()
        self.assertIsNone(my_timer.start_datetime)
        self.assertIsNone(my_timer.end_datetime)
        self.assertIsNone(my_timer.duration)

        # reset on a stopped timer
        my_timer.start()
        time.sleep(0.25)
        my_timer.stop()
        my_timer.reset()
        self.assertIsNone(my_timer.start_datetime)
        self.assertIsNone(my_timer.end_datetime)
        self.assertIsNone(my_timer.duration)

    def test_timers_add(self):
        my_timers = ifr_timer.Timers()
        self.assertEqual(len(my_timers.timers), 0)

        my_timers.add('first')
        my_timers.add('second')
        self.assertEqual(len(my_timers.timers), 2)
        self.assertTrue(my_timers.contains('first'))
        self.assertTrue(my_timers.contains('second'))

    def test_timers_contains(self):
        from ifr_lib.ifr_timer import Timers

        my_timers = Timers()
        my_timers.add('first')
        my_timers.start('second')
        self.assertTrue(my_timers.contains('first'))
        self.assertTrue(my_timers.contains('second'))

    def test_timers_remove(self):
        my_timers = ifr_timer.Timers()
        my_timers.add('first')
        my_timers.add('second')
        self.assertEqual(len(my_timers.timers), 2)
        my_timers.remove('first')
        self.assertEqual(len(my_timers.timers), 1)

    def test_timers_start(self):
        my_timers = ifr_timer.Timers()
        my_timers.add('first')
        my_timers.add('second')
        self.assertEqual(len(my_timers.timers), 2)

        my_timers.start('third')
        self.assertEqual(len(my_timers.timers), 3)
        self.assertIsNone(my_timers.timers['first'].start_datetime)
        self.assertIsNone(my_timers.timers['first'].end_datetime)
        self.assertIsNone(my_timers.timers['first'].duration)
        my_timers.start('second')
        self.assertIsNotNone(my_timers.timers['second'].start_datetime)
        self.assertIsNone(my_timers.timers['second'].end_datetime)
        self.assertIsNone(my_timers.timers['second'].duration)
        self.assertIsNotNone(my_timers.timers['third'].start_datetime)
        self.assertIsNone(my_timers.timers['third'].end_datetime)
        self.assertIsNone(my_timers.timers['third'].duration)

    def test_timers_durations(self):
        time_delta_first_second = 0.12
        time_delta_second_third = 0.31

        my_timers = ifr_timer.Timers()
        my_timers.start('first')
        my_timers.start('second')
        my_timers.start('third')
        self.assertIsNotNone(my_timers.durations())
        self.assertIsNone(my_timers.duration('first'))
        self.assertIsNone(my_timers.duration('second'))
        self.assertIsNone(my_timers.duration('third'))

        time.sleep(0.08)
        my_timers.stop('first')
        time.sleep(time_delta_first_second)
        my_timers.stop('second')
        time.sleep(time_delta_second_third)
        my_timers.stop('third')
        self.assertIsNotNone(my_timers.duration('first'))
        self.assertIsNotNone(my_timers.duration('second'))
        self.assertIsNotNone(my_timers.duration('third'))

        self.assertAlmostEqual(
            (my_timers.duration('second') - my_timers.duration('first')).total_seconds(),
            time_delta_first_second,
            delta=0.002)
        self.assertAlmostEqual(
            (my_timers.duration('third') - my_timers.duration('second')).total_seconds(),
            time_delta_second_third,
            delta=0.002)
        self.assertAlmostEqual(
            (my_timers.duration('third') - my_timers.duration('first')).total_seconds(),
            time_delta_first_second + time_delta_second_third,
            delta=0.002)

    def test_timers_stop(self):
        my_timers = ifr_timer.Timers()
        my_timers.start('first')
        my_timers.start('second')
        self.assertEqual(len(my_timers.timers), 2)

        self.assertFalse(my_timers.stop('third'))
        self.assertTrue(my_timers.stop('second'))
        self.assertEqual(len(my_timers.timers), 2)


if __name__ == '__main__':
    import unittest
    unittest.main()
