#!/usr/bin/env python
# -*- coding: utf-8 -*-
import filecmp
import os
import time

from ifr_lib import ifr_files, ifr_exception
from ifr_lib.ifr_files import parent_dir
from tests import IfrTestCase

# ##############################################
# CONSTANTS
# ##############################################
# The tests resources directory
RESOURCES_DIR = os.path.join(os.path.dirname(__file__), 'resources')
INFO_DO_NOT_DELETE_FILE_NAME = "info_do_not_delete.log"
INFO_FILE_NAME = "info.log"
OLD_INFO_FILE_NAME = "old_info.log"
UNZIPPED_INFO_FILE_NAME = "unzipped_info.log"



class FilesTestCase(IfrTestCase):
    """
    This class is a set of tests for the files methods of ifr_lib.
    """

    def setUp(self):
        self.tmp_dir = self.get_temp_dir()

    def clean(self):
        os.remove(self.tmp_dir)

    def test_parent_dir(self):
        self.assertEqual(ifr_files.parent_dir("hello/world/file"), os.path.join(os.getcwd(), "hello", "world"))
        self.assertEqual(ifr_files.parent_dir("hello/world/../file"), os.path.join(os.getcwd(), "hello"))

    def test_parent_basename(self):
        self.assertEqual(ifr_files.parent_basename("hello/world/file"), "world")
        self.assertEqual(ifr_files.parent_basename("hello/world/../file"), "hello")

    def basename_splitext(self):
        self.assertEqual(ifr_files.strip_extension("hello/world/my_file.py"), "hello/world/my_file")
        self.assertEqual(ifr_files.strip_extension("my_file.py.bak"), "my_file.py")
        self.assertEqual(ifr_files.strip_extension(".hello"), ".hello")
        self.assertEqual(ifr_files.strip_extension(".hi.world"), ".hi")

    def test_last_modification(self):
        file_path = os.path.join(
            RESOURCES_DIR,
            "utils_test_file",
            "20180424133103-OSISAF-L2P_GHRSST-SSTsubskin-"
            "AVHRR_SST_METOP_B-sstmgr_metop01_20180424_133103-v02.0-fv01.0.nc"
        )

        with open(file_path, 'a'):
            os.utime(file_path, (time.time(), time.time() - 2.0))
        self.assertTrue(ifr_files.is_mtime_older_than(file_path, delta_value=1, delta_unit='seconds'))
        self.assertFalse(ifr_files.is_mtime_older_than(file_path, delta_value=10, delta_unit='seconds'))

    def test_is_file_empty(self):
        self.assertTrue(ifr_files.is_file_empty(os.path.join(RESOURCES_DIR, "utils_test_file", "empty_file.txt")))
        self.assertFalse(
            ifr_files.is_file_empty(os.path.join(RESOURCES_DIR, "utils_test_file", INFO_DO_NOT_DELETE_FILE_NAME))
        )

    def test_find_file_in_syspath(self):
        from ifr_lib.ifr_exception import ItemNotFoundException

        self.assertEqual(
            ifr_files.find_file_in_syspath(
                os.path.join(RESOURCES_DIR, "utils_test_file", INFO_DO_NOT_DELETE_FILE_NAME)
            ),
            os.path.abspath(os.path.join(RESOURCES_DIR, "utils_test_file", INFO_DO_NOT_DELETE_FILE_NAME))
        )
        with self.assertRaises(ItemNotFoundException):
            ifr_files.find_file_in_syspath(os.path.join(RESOURCES_DIR, "utils_test_file", "not_existing.log"))

    def test_find_dir_in_syspath(self):
        from ifr_lib.ifr_exception import ItemNotFoundException

        self.assertEqual(ifr_files.find_dir_in_syspath(os.path.join(RESOURCES_DIR, "utils_test_file")),
                         os.path.abspath(os.path.join(RESOURCES_DIR, "utils_test_file")))
        with self.assertRaises(ItemNotFoundException):
            ifr_files.find_dir_in_syspath(os.path.join(RESOURCES_DIR, "not_existing_dir"))

    def test_get_hash(self):

        with self.assertRaises(ifr_exception.FileNotFoundException):
            ifr_files.get_hash(os.path.join(RESOURCES_DIR, "utils_test_file", "not_existing.txt"))

        empty = ifr_files.get_hash(os.path.join(RESOURCES_DIR, "utils_test_file", "empty_file.txt"))
        self.assertIsNotNone(empty)

        test_file_hash_path = os.path.join(RESOURCES_DIR, "utils_test_file", "hash_test.txt")
        test_file_hash = ifr_files.get_hash(test_file_hash_path)
        self.assertIsNotNone(test_file_hash)

        with open(test_file_hash_path, 'a') as test_file:
            test_file.write(',')

        new_test_file_hash = ifr_files.get_hash(test_file_hash_path)
        self.assertIsNotNone(new_test_file_hash)
        self.assertNotEqual(test_file_hash, new_test_file_hash)

        with open(test_file_hash_path, 'r') as test_file:
            text = test_file.read()

        with open(test_file_hash_path, 'w') as test_file:
            test_file.write(text[:-1])

        self.assertEqual(test_file_hash, ifr_files.get_hash(test_file_hash_path))

    def test_to_unix_path(self):
        self.assertEqual(
            ifr_files.to_unix_path(r'C:\dev\aaaa\bb'),
            'C:/dev/aaaa/bb'
        )

        self.assertEqual(
            ifr_files.to_unix_path(r'\\mount\dev\aaaa\bb'),
            r'\\mount/dev/aaaa/bb'
        )

        self.assertEqual(
            ifr_files.to_unix_path(r'C:\dev\aaaa\bb/cc/'),
            r'C:/dev/aaaa/bb/cc/'
        )

        self.assertEqual(
            ifr_files.to_unix_path(r'/home/tests/test2'),
            r'/home/tests/test2'
        )

    def test_basename_splitext_files(self):
        self.assertListEqual(
            ifr_files.basename_splitext_files(["ifr_lb/file.py", "ifr_lb/file2.py"]),
            ['file', 'file2']
        )

    def test_zip_unzip_file(self):
        dest_log_zip_filepath = os.path.join(self.tmp_dir, INFO_FILE_NAME)
        dest_unzipped_log_filepath = os.path.join(self.tmp_dir, UNZIPPED_INFO_FILE_NAME)
        src_log_filepath = os.path.join(RESOURCES_DIR, "utils_test_file", INFO_DO_NOT_DELETE_FILE_NAME)

        # FIRST TEST : Zip file then Unzip in a given directory without deleting sources
        final_dest_log_zip_filepath = ifr_files.gzip_file(
            source_file_path=src_log_filepath,
            destination_file_path=dest_log_zip_filepath,
            delete_source=False
        )
        self.assertTrue(os.path.isfile(final_dest_log_zip_filepath))
        self.assertTrue(os.path.isfile(src_log_filepath))
        ifr_files.gunzip(
            source_file_path=final_dest_log_zip_filepath,
            destination_file_path=dest_unzipped_log_filepath,
            delete_source=False
        )

        self.assertTrue(os.path.isfile(dest_unzipped_log_filepath))
        self.assertTrue(os.path.isfile(final_dest_log_zip_filepath))
        # Compare original file, with the zipped->unzipped file
        self.assertTrue(filecmp.cmp(src_log_filepath, dest_unzipped_log_filepath))
        # Clean for next tests
        os.remove(final_dest_log_zip_filepath)

        # SECOND TEST : Zip file then Unzip in a given directory with deleting sources
        final_dest_log_zip_filepath = ifr_files.gzip_file(
            source_file_path=dest_unzipped_log_filepath,
            destination_file_path=dest_log_zip_filepath,
            delete_source=True
        )
        self.assertTrue(os.path.isfile(final_dest_log_zip_filepath))
        self.assertFalse(os.path.isfile(dest_unzipped_log_filepath))

        ifr_files.gunzip(
            source_file_path=final_dest_log_zip_filepath,
            destination_file_path=dest_unzipped_log_filepath,
            delete_source=True
        )

        self.assertTrue(os.path.isfile(dest_unzipped_log_filepath))
        self.assertFalse(os.path.isfile(final_dest_log_zip_filepath))
        self.assertTrue(filecmp.cmp(src_log_filepath, dest_unzipped_log_filepath))
        # Clean for next tests
        os.remove(dest_unzipped_log_filepath)

        # THIRD TEST : Zip file then Unzip without giving dest file and without deleting sources
        final_dest_log_zip_filepath = ifr_files.gzip_file(
            source_file_path=src_log_filepath,
            delete_source=False
        )
        self.assertTrue(os.path.isfile(final_dest_log_zip_filepath))
        self.assertTrue(os.path.isfile(src_log_filepath))
        ifr_files.gunzip(
            source_file_path=final_dest_log_zip_filepath,
            delete_source=False
        )
        guessed_dest_unzipped_log_filepath = os.path.splitext(final_dest_log_zip_filepath)[0]
        self.assertTrue(os.path.isfile(guessed_dest_unzipped_log_filepath))
        # Compare original file, with the zipped->unzipped file
        self.assertTrue(filecmp.cmp(src_log_filepath, guessed_dest_unzipped_log_filepath))
        # Clean for next tests
        os.remove(final_dest_log_zip_filepath)

        # FOURTH TEST : Zip file then Unzip without giving dest file and with deleting sources
        final_dest_log_zip_filepath = ifr_files.gzip_file(
            source_file_path=guessed_dest_unzipped_log_filepath,
            delete_source=True
        )
        self.assertTrue(os.path.isfile(final_dest_log_zip_filepath))
        self.assertFalse(os.path.isfile(guessed_dest_unzipped_log_filepath))

        ifr_files.gunzip(
            source_file_path=final_dest_log_zip_filepath,
            delete_source=True
        )
        guessed_dest_unzipped_log_filepath = os.path.splitext(final_dest_log_zip_filepath)[0]
        self.assertTrue(os.path.isfile(guessed_dest_unzipped_log_filepath))
        self.assertFalse(os.path.isfile(final_dest_log_zip_filepath))
        self.assertTrue(filecmp.cmp(src_log_filepath, guessed_dest_unzipped_log_filepath))

        # Clean will be handled by ifr_testCase.clean for the latest test

if __name__ == '__main__':
    import unittest
    unittest.main()
