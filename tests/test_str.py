#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ifr_lib import ifr_str
from tests import IfrTestCase


class StrTestCase(IfrTestCase):

    def test_colorize(self):
        expected_colorize = b'\x1b[34m\x1b[47mThe original text\x1b[0m'
        actual_colorize = ifr_str.colorize("The original text", 'blue,bg_white')
        try:
            self.assertEqual(bytes(actual_colorize, 'utf8'), expected_colorize)
        except TypeError:
            self.assertEqual(bytes(actual_colorize), expected_colorize)

    def test_colorize_wrong_color(self):
        with self.assertRaises(KeyError):
            ifr_str.colorize("my_text", 'cherry')

    def test_hash(self):
        value = "lorem ipsum dolor sit amet consectetur adipiscing elit"
        self.assertTrue(ifr_str.compare_digest(
            ifr_str.hash_value(value),
            "b84d76ebf3b5ab58866a1d95c3322d56"
        ))

        self.assertTrue(ifr_str.compare_digest(
            ifr_str.hash_value(value, digest_size=8, salt="1234"),
            "d72b18e128a65f1c")
        )


if __name__ == '__main__':
    import unittest
    unittest.main()
