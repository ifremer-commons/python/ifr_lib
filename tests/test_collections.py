#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tests import IfrTestCase
from ifr_lib import ifr_collections

SAMPLE_DICTIONARY = {"key1": "value1", "key3": "value3", "key2": "value2"}


class CollectionsTestCase(IfrTestCase):

    def test_merge_dict(self):
        principal_dict = {
            "key2": "value2",
            "key1": "value1",
            "key3": "value3"
        }

        secondary_dict = {
            "key1": "value1",
            "key2": "value2",
            "key3": "value3"
        }

        result = ifr_collections.merge_dict(principal_dict, secondary_dict)
        self.assertDictEqual(result, principal_dict)
        self.assertDictEqual(result, secondary_dict)

        secondary_dict_other_values = {
            "key1": "other_value1",
            "key2": "other_value2",
            "key3": "other_value3"
        }
        result_other_values = ifr_collections.merge_dict(principal_dict, secondary_dict_other_values)
        self.assertDictEqual(result_other_values, secondary_dict_other_values)

        secondary_dict_other_values_more_keys = {
            "key1": "other_value1",
            "key2": "other_value2",
            "key3": "other_value3",
            "key4": "key4"
        }
        result_add_keys_false = ifr_collections.merge_dict(
            principal_dict,
            secondary_dict_other_values_more_keys,
            add_keys=False
        )
        self.assertDictEqual(
            result_add_keys_false,
            {
                "key1": "other_value1",
                "key2": "other_value2",
                "key3": "other_value3"
            }
        )

        result_add_keys_true = ifr_collections.merge_dict(
            principal_dict,
            secondary_dict_other_values_more_keys,
            add_keys=True
        )
        self.assertDictEqual(result_add_keys_true, secondary_dict_other_values_more_keys)

    def test_merge_dict_with_none(self):

        self.assertDictEqual(
            ifr_collections.merge_dict({'key1': 'value'}, {'key2': None}),
            {
                'key1': 'value',
                'key2': None
            }
        )

        self.assertDictEqual(
            ifr_collections.merge_dict({'key1': 'value', 'key2': None}, {'key2': None}),
            {
                'key1': 'value',
                'key2': None
            }
        )

        self.assertDictEqual(
            ifr_collections.merge_dict({'key1': 'value', 'key2': 'essai'}, {'key2': None}),
            {
                'key1': 'value',
                'key2': 'essai'
            }
        )

    def test_merge_dict_with_dotted_keys(self):
        principal_dict = {"tests": [
            {"key.1": "value2"},
            {"key.2": "value1"},
            {"key.3": "value3"}
        ]}

        secondary_dict = {"tests": [
            {"key.1": "value1"},
            {"key.2": "value2"},
            {"key.3": "value3"}
        ]}

        self.assertDictEqual(
            ifr_collections.merge_dict(principal_dict, secondary_dict),
            secondary_dict
        )

    def test_list_difference(self):
        list1 = ["donald", "daisy", "mickey"]
        list2 = ["daisy", "mickey", "minnie"]
        self.assertListEqual(['donald'], ifr_collections.list_difference(list1, list2))
        self.assertListEqual(['minnie'], ifr_collections.list_difference(list2, list1))
        self.assertListEqual([], ifr_collections.list_difference(list1, list1))
        for element in ifr_collections.list_difference(list1, []):
            self.assertIn(element, list1)
        self.assertListEqual([], ifr_collections.list_difference([], list2))
        self.assertListEqual([], ifr_collections.list_difference([], []))

    def test_prettify_dict(self):
        block_separator = ('~' * 10)
        self.assertListEqual(
            ifr_collections.prettify_dict(
                SAMPLE_DICTIONARY,
                item_prefix='* ',
                key_value_separator=': ',
                title='TEST',
                header=('~' * 10),
                footer=('~' * 10),
                title_footer=('~' * 10),
                item_sorted=True,
                color_options=None
            ),
            [
                block_separator,
                'TEST',
                block_separator,
                '* key1: value1',
                '* key2: value2',
                '* key3: value3',
                block_separator
            ]
        )

        self.assertListEqual(
            ifr_collections.prettify_dict(
                SAMPLE_DICTIONARY,
                item_prefix='* ',
                key_value_separator=': ',
                header=None,
                footer=None,
                title_footer=None,
                item_sorted=True
            ),
            [
                '* key1: value1',
                '* key2: value2',
                '* key3: value3'
            ]
        )

    def test_dict_to_str(self):
        self.assertEqual(
            ifr_collections.dict_to_str(
                SAMPLE_DICTIONARY,
                line_separator='\n',
                item_prefix='- ',
                key_value_separator=': ',
                header=None,
                footer=None,
                title_footer=None,
                item_sorted=True
            ),
            '- key1: value1\n- key2: value2\n- key3: value3'
        )

    def test_counter(self):
        repeated_item = 'lorem.ipsum.key1'
        single_item = 'lorem.ipsum.key2'
        test_list = [repeated_item, repeated_item, single_item]
        result = ifr_collections.counter(test_list)
        self.assertEqual(len(result), 2)
        self.assertEqual(result[repeated_item], 2)
        self.assertEqual(result[single_item], 1)

    def test_dict_to_map(self):
        self.assertDictEqual(
            ifr_collections.dict_to_map(
                {
                    "key1": {
                        "key3": "value3",
                        "key2": {
                            "key4": "value"
                        },
                        "key5": "value"
                    },
                    "key2": "value"
                }
            ),
            {
                'key1.key3': 'value3',
                'key1.key2.key4': 'value',
                'key1.key5': 'value',
                'key2': 'value'
            }
        )

        self.assertDictEqual(
            ifr_collections.dict_to_map(
                {
                    "horse": {
                        "color": "brown",
                        "speed": 42.0,
                        "shoes": ["frontleft", "frontright", "rearleft"]
                    }
                },
            ),
            {
                'horse.color': 'brown',
                'horse.speed': 42.0,
                'horse.shoes.0': 'frontleft',
                'horse.shoes.1': 'frontright',
                'horse.shoes.2': 'rearleft',
            }
        )

    def test_list_equals(self):
        list1 = ["donald", "daisy", "mickey"]
        list2 = ["daisy", "mickey", "minnie"]
        self.assertFalse(ifr_collections.list_equals(list1, list2))

        list3 = ["mickey", "donald", "daisy", "mickey"]
        self.assertTrue(ifr_collections.list_equals(list1, list3))

    def test_map_to_dict(self):
        map = {
            "aa.bb": 1,
            "aa.cc": 2
        }

        self.assertDictEqual(
            ifr_collections.map_to_dict(map),
            {
                "aa": {
                    "bb": 1,
                    "cc": 2
                }
            }

        )


if __name__ == '__main__':
    import unittest
    unittest.main()
