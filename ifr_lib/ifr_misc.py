#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides unclassifiable utilities.
"""
import os
from typing import Optional


def getvalue(value: Optional[str] = None, env_key: Optional[str] = None, default_value: Optional[str] = None) -> str:
    """Retrieves a value from envvar or option

    Retrieves the value with the following order:
        1. if value is not empty return value
        2. if envvar exists and value is not empty return envvar value
        3. return default value

    Args:
        value (str, optional): a value
        env_key (str, optional): environment variable
        default_value (str, optional): default value

    Returns:
        The built token

    Examples:
        >>> import os
        >>> my_var=ttests
        >>> env_key='TEST_ENVVAR'
        >>> default_value='default'
        >>> print(getvalue(my_var, env_key, default_value))
        ttests
        >>> my_var=None
        >>> print(getvalue(my_var, env_key, default_value))
        default
        >>> os.environ["TEST_ENVVAR"] = "1"
        >>> print(getvalue(my_var, env_key, default_value))
        1
    """
    if value:
        return value

    if not env_key:
        return default_value

    return os.getenv(env_key, default_value)


def package_version(package_name: str) -> str:
    """Retrieves package version from name

    Args:
        package_name (str): the package name

    Returns:
        The package version

    Raises:
        ItemNotFoundException: if package is not found in python path
    """
    import pkg_resources

    from ifr_lib.ifr_exception import ItemNotFoundException

    try:
        return pkg_resources.get_distribution(package_name).version
    except pkg_resources.DistributionNotFound:
        raise ItemNotFoundException(f"Package {package_name} not found")
