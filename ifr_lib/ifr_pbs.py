#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides utilities to manage pbs jobs.
"""
# for windows users install windows-curses in your project interpreter
import curses
import re
import sys
import time
from datetime import datetime, timedelta
from typing import Any, List, Mapping, Optional

from ifr_lib import (ifr_datetimes, ifr_exception, ifr_json, ifr_subprocess,
                     ifr_template)

DEFAULT_PBS_TEMPLATE = """#!/usr/bin/env bash
{% for pbs_option in pbs_options -%}
{{ pbs_option }}
{% endfor %}
set -e
{% if working_dir is not none %}
echo "Set cwd to {{ working_dir }}"
cd {{ working_dir }}
{% endif %}
echo "Command - start"
{{ command }}
echo "Command - end"

exit 0"""

DEFAULT_CMD_TEMPLATE = \
    'qsub ' \
    '{% if env_vars %}-v {{ env_vars | join(",") }}{% endif %} ' \
    '{% if pbs_log_file_path %}-o {{ pbs_log_file_path }}{% endif %} ' \
    '{{ pbs_file_path }}'


class PbsException(ifr_exception.GenericException):
    def __init__(self, *args, **kwargs):
        ifr_exception.GenericException.__init__(self, *args, **kwargs)


class PbsOption(object):
    """Represents a pbs option"""
    def __init__(self,
                 name: str,
                 arg: Optional[str] = None,
                 separator: Optional[str] = " ",
                 default_value: Optional[str] = None):
        self.name = name
        self.arg = arg
        self.__default_value = default_value
        self.value = self.__default_value
        self.separator = separator

    def __repr__(self):
        return "" if self.value is None else f"#PBS {self.arg}{self.separator}{self.value}"

    def __str__(self):
        return self.__repr__()


class PbsJob(ifr_json.JsonSerializable):
    """Represents the features of a pbs job """

    STATE_RUNNING = 'Running'
    STATE_FINISHED = 'Finished'
    STATE_INVALID = 'Invalid'
    STATE_FAILED = 'Failed'

    def __init__(self, job_id):
        self.job_id: str = job_id
        self.state: Optional[str] = None
        self.exit_status: Optional[int] = None
        self.start_date: Optional[datetime] = None
        self.end_date: Optional[datetime] = None
        self.duration: Optional[timedelta] = None

        self.__report: Optional[str] = None

        try:
            self._retrieve_report()
            self.state = self.STATE_RUNNING
            self.start_date = self._parse_date_report("ctime")
        except PbsException:
            self.exit_status = 1
            self.state = self.STATE_INVALID

    def _retrieve_report(self) -> None:
        self.__report = None
        exit_code, output, err = ifr_subprocess.run(f"qstat -xf {self.job_id}")
        if exit_code != 0:
            raise PbsException(f"Call to qstat for job {self.job_id} : {err}")
        self.__report = output

    def _parse_report(self, pattern: str) -> str:
        if self.__report is None:
            raise PbsException("No report !")

        matches = re.search(pattern, self.__report)
        if not matches:
            raise PbsException(f"Pattern not found in report : {pattern}")

        return matches.group(1)

    def _parse_date_report(self, field: str) -> datetime:
        return ifr_datetimes.str_to_datetime(
            self._parse_report(field + r'\s=\s(\w{3}\s\w{3}\s{1,2}\d{1,2}\s\d{2}:\d{2}:\d{2}\s\d{4})'),
            pattern='%a %b %d %H:%M:%S %Y'
        )

    def update(self, loop_count: int, loop_block_size: int):
        if self.state in [self.STATE_INVALID, self.STATE_FINISHED, self.STATE_FAILED]:
            return

        try:
            if self._is_job_finished():
                self._retrieve_report()
                self.state = self.STATE_FINISHED
                self.end_date = self._parse_date_report("mtime")
                self.exit_status = int(self._parse_report(r'Exit_status\s=\s(\w+)'))
                self.duration = self.end_date - self.start_date
            if not loop_count % loop_block_size:
                self._retrieve_report()
                job_state = self._parse_report(r'job_state\s=\s(\w)')
                if job_state == 'H':
                    self.state = self.STATE_INVALID
                    self.exit_status = 1
        except PbsException:
            self.state = self.STATE_FAILED

    def _is_job_finished(self) -> bool:
        exit_code, output, err = ifr_subprocess.run(f"Isfinished {self.job_id}")
        if exit_code == 0:
            return re.findall(r"\d+", output)[0] == '1'
        raise PbsException(f"Call to Isfinished failed : {err}")

    def __repr__(self):
        return "{" \
               f"'jobid': '{self.job_id}', " \
               f"'state': '{self.state}', " \
               f"'exit_status' : {self.exit_status}, " \
               f"'start_date': {self.start_date}, " \
               f"'end_date': {self.end_date}, " \
               f"'duration': '{self.duration}'" \
               "}"

    def __str__(self):
        return self.__repr__()


def build_pbs_script(
        command: str,
        working_dir: Optional[str] = None,
        pbs_options: Optional[List[PbsOption]] = None,
        pbs_template: Optional[Any] = None) -> str:
    """Builds a pbs script using the passed template or the default one.

    Args:
        command (str): The command
        working_dir (str, optional): The working directory
        pbs_options (List[PbsOption], optional): The pbs options list
        pbs_template (Any, optional): The pbs template, DEFAULT_PBS_TEMPLATE by default

    Returns:
        The built script
    """

    return ifr_template.BaseTemplateRenderer().interpolate(
       content=DEFAULT_PBS_TEMPLATE if pbs_template is None else pbs_template,
       replacements={
         "command": command,
         "working_dir": working_dir,
         "pbs_options": list() if pbs_options is None else pbs_options
       }
    )


def build_qsub_command(pbs_file_path: str,
                       pbs_log_file_path: str,
                       env_vars: Mapping,
                       cmd_template: Optional[Any] = None
                       ) -> str:
    """Builds a qsub command using the passed template or the default one.

    Args:
        pbs_file_path (str): The pbs file path
        pbs_log_file_path (str): The pbs log file path
        env_vars (Mapping): The environment variables mapping
        cmd_template (Any, optional): The command template, DEFAULT_CMD_TEMPLATE by default

    Returns:
        The built qsub command
    """

    return ifr_template.BaseTemplateRenderer().interpolate(
        content=DEFAULT_CMD_TEMPLATE if cmd_template is None else cmd_template,
        replacements={
            "pbs_file_path": pbs_file_path,
            "pbs_log_file_path": pbs_log_file_path,
            "env_vars": list() if env_vars is None else [f'{k}="{v}"' for k, v in env_vars.items()]
        }
    )


def run_pbs_job(qsub_command: str) -> Optional[PbsJob]:
    """Runs a pbs job from the passed qsub command

    Args:
        qsub_command (str): The qsub command to be launched via pbs

    Returns:
        The launched pbs job details
    """
    (exitcode, output, err) = ifr_subprocess.run(qsub_command)
    if exitcode > 0:
        return None
    return PbsJob(output.replace('\n', '').strip())


def run_and_monitor(qsub_command: str, check_interval: int) -> List[PbsJob]:
    """Runs and monitors a pbs job from the passed qsub command and check interval

    Args:
        qsub_command (str): The qsub command to be launched via pbs
        check_interval: the check interval

    Returns:
        The launched pbs job details
    """
    pbs_job = run_pbs_job(qsub_command)
    return check_jobfiles([pbs_job], interval=check_interval, silent=True)


def check_jobfiles(
    jobs_list: list,
    interval: int = 60,
    loop_block_size: int = 10,
    silent: bool = False,
) -> List[PbsJob]:
    """Checks the passed jobs list states at the defined or default interval.
    Returns the updated pbs jobs list after completion of all jobs.

    Args:
        jobs_list (list): The pbs jobs list to be checked
        interval (int, optional): The check interval, 60 seconds by default
        loop_block_size (int, optional): Check job state every x loop to track 'Hold' status jobs, 10 by default
        silent (bool): The option indicating whether a screen display is wanted

    Returns:
        The updated pbs jobs list
    """

    def is_still_running(loop_count: int, loop_block_size: int) -> bool:
        running = False
        for job in jobs_list:
            job.update(loop_count=loop_count, loop_block_size=loop_block_size)
            if not running and job.state == PbsJob.STATE_RUNNING:
                running = True
        return running

    screen = None
    try:
        if not silent:
            screen = curses.initscr()
            curses.cbreak()

        loops = 1
        while is_still_running(loop_count=loops, loop_block_size=loop_block_size):
            loops += 1
            if not silent:
                display_jobs(jobs_list, screen, interval)
            time.sleep(interval)
        return jobs_list
    finally:
        if not silent:
            curses.endwin()


def display_jobs(jobs_list: List[PbsJob], screen, interval: int):
    """Displays the passed jobs list execution on screen at defined time intervals.

    Args:
        jobs_list (List[PbsJob]): The jobs list to be displayed
        screen: The screen to display execution details on
        interval (int): The display time interval in seconds
    """
    screen.clear()
    screen.addstr(0, 0, f"Waiting all jobs to be finished. Check every {interval} seconds")
    screen.addstr(1, 0, f"Time : {str(datetime.now())}")
    screen.addstr(3, 0, "| Job ID")
    screen.addstr(3, 20, "| State")
    screen.addstr(3, 30, "| Exit_status")
    screen.addstr(3, 45, "| Creation date")
    screen.addstr(3, 67, "|")
    screen.addstr(4, 0, 68 * "-")
    i = 5
    for job in jobs_list:
        screen.addstr(i, 0, f"| {job.job_id}")
        screen.addstr(i, 20, f"| {job.state}")
        screen.addstr(i, 30, f"| {job.exit_status}")
        screen.addstr(i, 45, f"| {ifr_datetimes.datetime_to_iso(job.start_date)} |")
        i = i + 1
    screen.refresh()


def compute_exit_status(jobs_list: List[PbsJob]) -> int:
    """Computes a common exit status from the passed pbs jobs list.

    Args:
        jobs_list (List[PbsJob]): The pbs jobs list

    Returns:
        0 in case every job in the list succeeded, 1 otherwise
    """
    if len(jobs_list) == 1:
        return jobs_list[0].exit_status
    else:
        for job in jobs_list:
            if job.exit_status != 0:
                return 1
        return 0


def qmonitor():
    import argparse

    job_ids_stdin = None

    # build parser
    parser = argparse.ArgumentParser(description="Supervise the end of a jobId list")
    if not sys.stdin.isatty():
        job_ids_stdin = re.findall(r"[\w.']+", sys.stdin.read())
    else:
        parser.add_argument("job_id", type=str, nargs="+", help="A PBS job ID (ex: 8391934.datarmor0)")

    parser.add_argument("--interval", type=int, default=30, help="Time in seconds between two analysis")
    parser.add_argument("--silent", action='store_true', help="Print only final message")
    parser.add_argument("--pretty", action='store_true', help="Prettify final message")

    # retrieve args
    args = parser.parse_args()

    # follow jobs
    jobs = check_jobfiles(
        list(map(lambda x: PbsJob(x), job_ids_stdin if job_ids_stdin else args.job_id)),
        interval=args.interval,
        silent=args.silent
    )

    # compute exit status
    exit_code = compute_exit_status(jobs)

    # print the result
    print(ifr_json.json_dumps(
        {'exit_status': exit_code, 'jobs': jobs},
        only_public_members=True,
        indent=2 if args.pretty else None)
    )

    # exit
    sys.exit(exit_code)
