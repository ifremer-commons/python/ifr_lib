#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides some utilities to manage date and time.
"""
import datetime
import re
import time
from calendar import monthrange
from typing import List, Optional, Tuple

import dateparser

from ifr_lib import ifr_numbers
from ifr_lib.ifr_exception import BadParameterException, ConversionException, PatternException, BadValueException

DEFAULT_ISO_FORMAT = '%Y%m%dT%H%M%S'


def now() -> datetime.datetime:
    """Retrieves the current datetime.

    Returns:
        The current datetime object
    """
    return datetime.datetime.now()


def elapsed_time(date1: datetime.datetime, date2: datetime.datetime) -> datetime.timedelta:
    """Calculates the elapsed time between the two dates

    Args:
        date1 (datetime.datetime): a date
        date2 (datetime.datetime): a date

    Returns:
        The elapsed time between date2 and date1

    Examples:
            >>> date1 = str_to_datetime('2018-02-01T14:28:42+0200')
            >>> date2 = str_to_datetime('2018-08-31T07:48:12+0200')
            >>> print(elapsed_time(date1, date2))
            210 days, 17:19:30
    """
    return date2 - date1


def datetime_to_str(date: Optional[datetime.datetime] = now(), pattern: Optional[str] = '%d/%m/%Y') -> str:
    """Converts a datetime to a str.

    Args:
        date (datetime.datetime): The datetime object to convert. Defaults to `now()`
        pattern: The string representation pattern. Defaults to `%d/%m/%Y`

    Returns:
        The datetime object converted to string

    Raises:
        ConversionException: if conversion failed (bad pattern, ...)

    Examples:
            >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
            >>> print(datetime_to_str(date))
            01/02/2018
    """
    try:
        return date.strftime(pattern)
    except (TypeError, ValueError) as err:
        raise ConversionException(f"Cannot convert {date} to string with the format {pattern}. "
                                  f"'The following error occurred : {err}")


def datetime_to_iso(date: Optional[datetime.datetime] = now()) -> str:
    """Retrieves a date formatted to ISO format

    Args:
        date (datetime.datetime): The datetime object to convert. Defaults to `now()`

    Returns:
        The datetime object converted to string

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(datetime_to_iso(date))
        20180201T14:28:42+02:00
    """
    return datetime_to_str(date=date, pattern=DEFAULT_ISO_FORMAT)


def parse_date(date_string: str,
               date_formats: Optional[List[str]] = None,
               languages: Optional[List[str]] = None,
               locales: Optional[List[str]] = None,
               region: Optional[str] = None,
               settings: Optional[dict] = None
               ) -> datetime.datetime:
    """Parse date and time from given date string.

    @see dateparser.parse for more information
    """
    def format_list_opt(opt):
        return opt if opt is None or isinstance(opt, list) else [opt]
    try:
        res = dateparser.parse(
            date_string,
            date_formats=format_list_opt(date_formats),
            languages=format_list_opt(languages),
            locales=format_list_opt(locales),
            region=region,
            settings=settings)
        if res is not None:
            return res

        raise ConversionException(f"Cannot convert {date_string} to datetime")
    except ValueError as err:
        raise ConversionException(
            f"Cannot convert {date_string} to datetime. The following error occurred : {err}")


def str_to_datetime(value: str, pattern: Optional[str] = DEFAULT_ISO_FORMAT) -> datetime.datetime:
    """Converts a string to a datetime from a pattern.
    [See reference](https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior)

    Args:
        value (str): A string representing the date to convert
        pattern (str): The format of the date to convert.
                       Defaults to `%Y-%m-%dT%H:%M:%S%z` or `%Y-%m-%dT%H:%M:%S`
                       depending on python version.

    Returns:
        The converted datetime

    Raises:
        ConversionException: if conversion failed (bad pattern, ...)

    Examples:
            >>> print(str_to_datetime('20180201','%Y%m%d'))
            2018-02-01 00:00:00

            >>> print(str_to_datetime('2018-02-01T14:28:42+0200'))
            2018-02-01 14:28:42+02:00
    """
    try:
        return datetime.datetime.strptime(value, pattern)
    except ValueError as err:
        raise ConversionException(f"Cannot convert {value} to datetime with the format {pattern}."
                                  f" The following error occurred : {err}")


def day(date: Optional[datetime.date] = now()) -> str:
    """Retrieves the number of day in the month of a date.

    Args:
        date (datetime.date): a date object. Defaults to `now()`

    Returns:
        The number of the day in the month (2 digits)

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(day(date))
        01
    """
    return date.strftime('%d')


def previous_day(date: Optional[datetime.datetime] = now()) -> datetime.datetime:
    """Retrieves the day before a date.

    Args:
        date (datetime.datetime): a datetime object. Defaults to `now()`

    Returns:
        The day before a date

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(previous_day(date))
        2018-01-31 14:28:42+02:00
    """
    return date - datetime.timedelta(days=1)


def day_in_year(date: Optional[datetime.date] = now()) -> str:
    """Retrieves the number of the day in the year of a date.

    Args:
        date (datetime.date): a datetime object. Defaults to `now()`

    Returns:
        The number of the day in the year (3 digits)

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(day_in_year(date))
        032
    """
    return date.strftime('%j')


def month(date: Optional[datetime.date] = now()) -> str:
    """Retrieves the number of the month in the year of a date.

    Args:
        date (datetime.date): a datetime object. Defaults to `now()`

    Returns:
        The number of the month in the year (2 digits)

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(month(date))
        02
    """
    return date.strftime('%m')


def previous_month(date: Optional[datetime.date] = now()) -> str:
    """Retrieves the previous month of a date.

    Args:
        date (datetime.date): a date object. Defaults to `now()`

    Returns:
        The number of the month in the year (2 digits)

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(previous_month(date))
        01
    """
    return str(date.month - 1 if date.month > 1 else 12).zfill(2)


def year_month(date: Optional[datetime.date] = now(), separator: Optional[str] = '') -> str:
    """Retrieves a date formatted to `year[separator]month`

    Args:
        date (datetime.date): a date object. Defaults to `now()`
        separator (str) : a separator between year and month. Defaults to `''`

    Returns:
        The current month with the pattern `year[separator]month`

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(year_month(date))
        201802
    """
    return f"{year(date)}{separator}{month(date)}"


def previous_year_month(date: Optional[datetime.date] = now(), separator: Optional[str] = '') -> str:
    """Retrieves the previous month of a date formatted to `year[separator]month`.

    Args:
        date (datetime.date): a date object. Defaults to `now()`
        separator (str) : a separator between year and month. Defaults to `''`

    Returns:
        The previous month with the pattern `year[separator]month`

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(previous_year_month(date))
        201801
    """
    return year_month(date.replace(day=1) - datetime.timedelta(days=1), separator)


def month_range(date: Optional[datetime.date] = now()) -> Tuple[datetime.datetime, datetime.datetime]:
    """Retrieves the number of the day in the year of a date.

    Args:
        date (datetime.date): a date object. Defaults to `now()`

    Returns:
        datetime: the first day of the month
        datetime: the last day of the month

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(month_range(date))
        (datetime.date(2018, 2, 1), datetime.date(2018, 2, 28))
    """
    year_int = ifr_numbers.to_int(date.strftime('%Y'))
    month_int = ifr_numbers.to_int(date.strftime('%m'))
    ndays = monthrange(year_int, month_int)[1]
    return (
        datetime.datetime(year_int, month_int, 1),
        datetime.datetime(year_int, month_int, ndays)
    )


def year(date: Optional[datetime.date] = now()) -> str:
    """Retrieves the year of a date.

    Args:
        date (datetime.date): a date object. Defaults to `now()`

    Returns:
        The year (4 digits)

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(year(date))
        2018
    """
    return date.strftime('%Y')


def short_year(date: Optional[datetime.date] = now()) -> str:
    """Retrieves the year of a date.

    Args:
        date (datetime.date): a date object. Defaults to `now()`

    Returns:
        The year (2 digits)

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(short_year(date))
        18
    """
    return date.strftime('%y')


def previous_year(date: Optional[datetime.date] = now()) -> str:
    """Retrieves the previous year of a date.

    Args:
        date (datetime.date): a date object. Defaults to `now()`

    Returns:
        The previous year (4 digits)

    Examples:
        >>> date = str_to_datetime('2018-02-01T14:28:42+0200')
        >>> print(previous_year(date))
        2017
    """
    return str(date.year - 1).zfill(4)


def add_years(date: Optional[datetime.datetime] = now(), years: Optional[int] = 1) -> datetime.datetime:
    """Adds years to a date

    Args:
        date (datetime.datetime): a date object. Defaults to `now()`
        years (int): numbers of years to add

    Returns:
        The new date

    Examples:
        >>> date = str_to_datetime('2020-02-29T14:28:42+0200')
        >>> print(add_years(date, 1))
        2021-02-28 14:28:42+02:00
    """
    try:
        return date.replace(year=date.year + years)
    except ValueError:
        return date.replace(year=date.year + years, day=date.day - 1)


def sub_years(date: Optional[datetime.datetime] = now(), years: Optional[int] = 1) -> datetime.datetime:
    """Subtracts years to a date

    Args:
        date (datetime.datetime): a date object. Defaults to `now()`
        years (int): numbers of years to subtract

    Returns:
        The new date

    Examples:
        >>> date = str_to_datetime('2020-02-29T14:28:42+0200')
        >>> print(sub_years(date, 1))
        2019-02-28 14:28:42+02:00
    """
    return add_years(date, -years)


def days_between(start_date: datetime.datetime, end_date: datetime.datetime) -> List[datetime.datetime]:
    """Retrieve list of days in a period

    Args:
        start_date (datetime.datetime): start date
        end_date (datetime.datetime): end date

    Returns:
        A list of days
    """
    assert isinstance(start_date, datetime.date)
    assert isinstance(end_date, datetime.date)
    assert start_date <= end_date

    days = list()
    days.append(start_date)
    for i in range(1, (end_date - start_date).days + 1):
        days.append(start_date + datetime.timedelta(days=i))
    return days


def is_time_older_or_younger_than(
        ref_time: time.time, delta_value: Optional[int] = 1, delta_unit: Optional[str] = 'minutes') -> bool:
    """Compare if a time is older or younger than X units

    Args:
        ref_time (time.time): time to be compared
        delta_value (int): positive or negative value
        delta_unit (str): unit in seconds, minutes, hours, days

    Returns:
        True if condition is validated
    """
    conversion_dict = {'seconds': 1, 'minutes': 60, 'hours': 3600, 'days': 86400}

    # check delta unit
    if delta_unit not in conversion_dict:
        raise BadParameterException(f'"{delta_unit}" is not a valid time unit. '
                                    f'Time units list : {", ".join(conversion_dict.keys())}')

    # build time_value
    time_value = (time.time() - ref_time) / conversion_dict[delta_unit]

    # manage positive or negative delta value
    if delta_value < 0:
        return abs(delta_value) > time_value
    return time_value >= delta_value


def get_datetime(value: str,
                 regexp_pattern: Optional[str] = r"([0-9]{8})",
                 datetime_pattern: Optional[str] = '%Y%m%d',
                 replace: Optional[str] = None) -> datetime.datetime:
    """Extracts datetime from a string with a regular expression, using group named 'date'

    Args:
        value (str): string to extract date
        regexp_pattern (str): a regular expression to extract date.
            Must contain a group named 'date'.
            Defaults to `r".*([0-9]{8}).*"`
        datetime_pattern (str) : a datetime format pattern. Defaults to `%Y%m%d`
        replace (str): replace value

    Returns:
        The datetime object extracted from input value

    Raises:
        PatternException: if pattern not found in filename

    Examples:
        >>> print(get_datetime(
        >>>   'filenamesuffix_20180201_filenameprefix.extension',
        >>>   r".*([0-9]{8}).*"
        >>> ))
        2018-02-01 00:00:00
    """
    pattern = re.compile(regexp_pattern)
    if replace is None:
        res = pattern.findall(value)
        if not res:
            raise PatternException(f'Invalid pattern {regexp_pattern} for {value}.')
        date_str = res[0]
    else:
        if not pattern.match(value):
            raise PatternException(f'Invalid pattern {regexp_pattern} for {value}.')
        date_str = pattern.sub(replace, value)

    return str_to_datetime(date_str, datetime_pattern)


def get_date(value: str,
             regexp_pattern: Optional[str] = r"([0-9]{8})",
             datetime_pattern: Optional[str] = '%Y%m%d'
             ) -> datetime.date:
    """Extracts date from a string with a regular expression, using group named 'date'

    Args:
        value (str): string to extract date
        regexp_pattern (str): a regular expression to extract date.
            Must contain a group named 'date'.
            Defaults to `r".*([0-9]{8}).*"`
        datetime_pattern (str) : a datetime format pattern. Defaults to `%Y%m%d`

    Returns:
        The date object extracted from input value

    Raises:
        PatternException: if pattern not found in filename

    Examples:
        >>> print(get_date(
        >>>   'filenamesuffix_20180201_filenameprefix.extension',
        >>>   r"(?:^|[^\\d])(?P<date>\\d{8})(?:$|[^\\d])"
        >>> ))
        2018-02-01
    """
    return get_datetime(value, regexp_pattern, datetime_pattern).date()


def format_date(
        date_as_str: str,
        from_pattern: Optional[str] = '%Y-%m-%dT%H:%M:%S',
        to_pattern: Optional[str] = '%Y%m%d'
) -> str:
    """Formats a given date

    Args:
        date_as_str (str): The date as a string
        from_pattern (str, optional): The input date pattern
        to_pattern (str, optional): The output date pattern

    Returns:
        The formatted date
    """
    date = str_to_datetime(date_as_str, from_pattern)
    return date.strftime(to_pattern)


def parse_and_validate_period(
        start: str,
        end: Optional[str] = None,
        date_format: Optional[str] = '%Y%m%d'
) -> Tuple[datetime.datetime, datetime.datetime]:
    """Parses and validates a time period

    Args:
        start (str): The start date string
        end (str, optional): The end date string
        date_format (str, optional): The date format pattern

    Returns:
        A tuple containing the start and end datetimes
    """
    start_date = parse_date(start, date_format)

    if end is None:
        end_date = start_date
    else:
        end_date = parse_date(end, date_format)

    if end_date < start_date:
        raise BadValueException(
            f"End date must be greater than or equal to start date "
            f"(start_date='{datetime_to_str(start_date, '%Y%m%d')}' ;"
            f" end_date='{datetime_to_str(end_date, '%Y%m%d')}')"
        )

    return start_date, end_date
