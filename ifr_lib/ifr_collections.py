#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides utilities to manage collections.
"""
from collections import Counter
from typing import Mapping, Optional

from ifr_lib import ifr_exception, ifr_str


def merge_dict(dict1: dict, dict2: dict, add_keys: bool = True, __stack: list = None) -> dict:
    """ Recursive dict merge. Inspired by :meth:`dict.update()`, instead of
    updating only top-level keys, dict_merge recurses down into dicts nested
    to an arbitrary depth, updating keys. The `merge_dct` is merged into
    `dct`.

    This version will return a copy of the dictionary and leave the original
    arguments untouched.

    The optional argument `add_keys`, determines whether keys which are
    present in `merge_dict` but not `dct` should be included in the
    new dict.

    Types are conserved during merging, with the exception of None values. a
    None value in dict1 can be overridden by any type, and a None value in
    dict2 is skipped.

    Args:
        dict1 (dict): dct onto which the merge is executed
        dict2 (dict): dct merged into dct
        add_keys (bool): whether to add new keys
        __stack (list):

    Returns:
        updated dict
    """
    if dict2 is None:
        return dict1

    result_dict = dict1.copy()
    if not add_keys:
        dict2 = {
            k: dict2[k]
            for k in set(result_dict).intersection(set(dict2))
        }

    for k, v in dict2.items():
        if k in result_dict and isinstance(result_dict[k], dict) and isinstance(dict2[k], Mapping):
            # Recursively merge
            stack = (__stack + [k]) if __stack is not None else [k]
            result_dict[k] = merge_dict(result_dict[k], dict2[k], add_keys=add_keys, __stack=stack)
        else:
            if k not in result_dict or result_dict[k] is None \
                    or isinstance(dict2[k], type(result_dict[k])) \
                    or isinstance(result_dict[k], type(dict2[k])):
                # add or overwrite key/value
                result_dict[k] = dict2[k]
            elif v is not None:
                stack = (__stack + [k]) if __stack is not None else [k]
                raise ifr_exception.BadTypeException(
                    f"Type mismatch while merging key {'.'.join(stack)}: "
                    f"trying to overwrite {type(result_dict[k])} with {type(dict2[k])}")

    return result_dict


def to_dict(key: str, value: str, split_separator: str = '.') -> dict:
    """Transforms a key/value pair to dict

    Args:
        key (str): a simple key or a namespace
        value (str): the value of the leaf element
        split_separator (str): the separator of items in the key, dot by default

    Returns:
        a dictionary

    Examples:
        >>> print(to_dict("toto", 'tutu'))
        {'toto': 'tutu'}

        >>> print(to_dict("toto.tata.titi", 'tutu'))
        {'toto': {'tata': {'titi': 'tutu'}}}
    """
    if split_separator is None:
        split_separator = '.'

    if split_separator not in key:
        return {key: value}

    def walk_items(items):
        d = dict()
        key = items.pop(0)
        if len(items) > 0:
            d[key] = walk_items(items)
        else:
            d[key] = value
        return d

    return walk_items(key.split(split_separator))


def dict_to_map(dictionary: dict) -> dict:
    """Flattens a dictionary to dotted key/value

    Args:
        dictionary (dict): a dictionary

    Returns:
        a (dotted key , value) map
    """
    assert isinstance(dictionary, (dict, Mapping))

    def process(value, path=None, dict_as_map=None):
        if dict_as_map is None:
            dict_as_map = dict()

        if isinstance(value, (dict, Mapping)):
            for key, value in value.items():
                dict_as_map.update(
                    process(value, ("" if path is None else (path + ".")) + key, dict_as_map)
                )
        elif isinstance(value, list):
            for i in range(0, len(value)):
                dict_as_map.update(process(value[i], ("" if path is None else (path + ".")) + str(i), dict_as_map))
        else:
            return [(path, value)]
        return dict_as_map

    return process(dictionary)


def map_to_dict(map_dict: dict) -> dict:
    final_dict = dict()
    for k, v in map_dict.items():
        final_dict = merge_dict(final_dict, to_dict(k, v))
    return final_dict


def list_difference(list1: list, list2: list) -> list:
    """Checks elements from list1 not present in list2.

    Args:
        list1 (list): the main list
        list2 (list): the list to compare content

    Returns:
        a list of missing items in list2

    Examples:
        >>> list1 = ["donald", "daisy", "mickey"]
        >>> list2 = ["daisy", "mickey", "minnie"]
        >>> print(list_difference(list1, list2))
        ['donald']
        >>> print(list_difference(list2, list1))
        ['minnie']
    """

    return sorted(set(list1).difference(list2))


def list_equals(list1: list, list2: list) -> bool:
    """Checks two lists are equals

    Args:
        list1 (list): the first list
        list2 (list): the second list

    Returns:
        True if lists are the same, otherwise False

    Examples:
        >>> list1 = ["donald", "daisy", "mickey"]
        >>> list2 = ["daisy", "mickey", "minnie"]
        >>> print(list_equals(list1, list2))
        False
        >>> list3 = ["mickey", "donald", "daisy", "mickey"]
        >>> print(list_equals(list1, list3))
        True
    """
    return set(list1) == set(list2)


def prettify_dict(dictionary: dict,
                  item_prefix: str = '- ',
                  key_value_separator: Optional[str] = ': ',
                  title: Optional[str] = None,
                  header: Optional[str] = ('-' * 70),
                  footer: Optional[str] = ('-' * 70),
                  title_footer: Optional[str] = ('-' * 70),
                  item_sorted: bool = False,
                  color_options: Optional[str] = None
                  ) -> list:
    """Converts a dictionary to a list of string representation.

    Args:
        dictionary (dict): a dictionary
        title (str, optional): a title to display before the list of elements
        item_prefix (str, optional): a prefix before each item. Default to '- '
        key_value_separator (str, optional): a separator between key and value. Default to ': '
        header (str, optional): a separator before the title.
        footer (str, optional): a separator before the title.
        title_footer (str, optional): a separator after the title.
        item_sorted (bool, optional): if true, sort the items by the key, otherwise keep the default sort
        color_options (str, optional): color options (ex: 'blue', 'bold_blue,bg_white', ...)

    Returns:
        a prettified representation of the dictionary

    Examples:
        >>> dictionary = {"key1": "value1", "key3": "value3", "key2": "value2"}
        >>> print(prettify_dict(dictionary))
        [
            '- key1: value1',
            '- key3: value3',
            '- key2: value2'
        ]

        >>> print(prettify_dict(dictionary, title="My dict", item_sorted=True))
        [
            '----------------------------------------------------------------------',
            'My dict',
            '----------------------------------------------------------------------',
            '- key1: value1', '
            - key2: value2',
            '- key3: value3'
        ]
    """

    def colorize_item(item):
        return ifr_str.colorize(item, color_options)

    def build_message(key, value):
        if "password" in key:
            value = "*" * len(value)
        return colorize_item(f'{item_prefix}{key}{key_value_separator}{value}')

    msg = list()

    if header:
        msg.append(colorize_item(header))

    if title:
        msg.append(colorize_item(f'{title}'))
        if title_footer:
            msg.append(colorize_item(title_footer))

    if not dictionary:
        return msg

    if item_sorted:
        for key, value in sorted(dictionary.items()):
            msg.append(build_message(key, value))
    else:
        for key, value in dictionary.items():
            msg.append(build_message(key, value))

    if footer:
        msg.append(colorize_item(footer))

    return msg


def dict_to_str(dictionary: dict,
                line_separator: str = '\n',
                item_prefix: Optional[str] = '- ',
                key_value_separator: Optional[str] = ' : ',
                title: Optional[str] = None,
                header: Optional[str] = ('-' * 70),
                footer: Optional[str] = ('-' * 70),
                title_footer: Optional[str] = ('-' * 70),
                item_sorted: bool = False,
                color_options: Optional[str] = None
                ) -> str:
    """Converts a dictionary to a list of string representation.

    Args:
        dictionary (dict): a dictionary
        line_separator (str): a line separator. Defaults to "\\n"
        title (str, optional): a title to display before the list of elements
        item_prefix (str, optional): a prefix before each item. Default to "- "
        key_value_separator (str, optional): a separator between key and value.
            Default to ": "
        header (str, optional): a separator before the title.
        footer (str, optional): a separator before the title.
        title_footer (str, optional): a separator after the title.
        item_sorted (bool, optional): if true, sort the items by the key,
            otherwise keep the default sort
        color_options (str, optional): color options (ex: 'blue', 'bold_blue,bg_white', ...)

    Returns:
        a prettified representation of the dictionary

    Examples:
        >>> dictionary = {"key1": "value1", "key3": "value3", "key2": "value2"}
        >>> print(dict_to_str(dictionary))
        - key1: value1
        - key3: value3
        - key2: value2

        >>> print(dict_to_str(dictionary, title="My dict", item_sorted=True))
        ----------------------------------------------------------------------
        My dict
        ----------------------------------------------------------------------
        - key1: value1
        - key2: value2
        - key3: value3
    """
    return line_separator.join(prettify_dict(
        dictionary=dictionary,
        item_prefix=item_prefix,
        key_value_separator=key_value_separator,
        title=title,
        header=header,
        footer=footer,
        title_footer=title_footer,
        item_sorted=item_sorted,
        color_options=color_options
    ))


def counter(list_of_elements: list) -> Counter:
    """Count the appearance number of each item in a list.

    Args:
        list_of_elements (list): a list of elements

    Returns:
        a dictionary of [element, number of occurrences]

    Examples:
        >>> dictionary = ['lorem.ipsum.key1', 'lorem.ipsum.key1', 'lorem.ipsum.key2']
        >>> print(counter(dictionary))
        Counter({'lorem.ipsum.key1': 2, 'lorem.ipsum.key2': 1})

        >>> print(dict_to_str(counter(dictionary), title='Number of occurrences'))
        ----------------------------------------------------------------------
        Number of occurrences
        ----------------------------------------------------------------------
        - lorem.ipsum.key1: 2
        - lorem.ipsum.key2: 1
    """
    return Counter(list_of_elements)
