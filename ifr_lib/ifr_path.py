#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides utilities for to manage paths
    [see](https://pypi.org/project/pathfinder/)
"""

import os
import shutil
import sys
import time
from datetime import datetime
from typing import Any, Callable, List, Optional

import pathfinder
from pathfinder.filters import (AndFilter, DirectoryFilter, FileFilter, Filter,
                                FnmatchFilter, NotFilter, OrFilter,
                                RegexFilter, SizeFilter)

from ifr_lib import ifr_datetimes
from ifr_lib.ifr_exception import (BadParameterException,
                                   FileNotFoundException,
                                   ItemNotFoundException)
from ifr_lib.ifr_yaml import IfrYamlCodec, YamlSerializable


def to_path(items):
    if items is None or len(items) == 0:
        return "*"
    return "{" + ','.join(items) + "}"


def build_path(path: str):
    if os.path.exists(path) and os.path.isfile(path):
        return File(path)
    return Folder(path)


class NoFilter(Filter):
    """ Accept every path. """
    def accepts(self, _) -> bool:
        """ Always returns True. """
        return True


class BasenameRegexFilter(RegexFilter):
    def accepts(self, file_path) -> bool:
        """Indicates whether the regular expression matches the file path

        Args:
            file_path: The file path

        Returns:
            True if the regular expression matches the file path
        """
        return super(BasenameRegexFilter, self).accepts(os.path.basename(file_path))


class BasenameFnmatchFilter(FnmatchFilter):
    def accepts(self, file_path):
        return super(BasenameFnmatchFilter, self).accepts(os.path.basename(file_path))


class BasenameDateFilter(Filter):
    EQUAL_TO = "eq"
    EQUAL_OR_GREATER_THAN = "egt"
    GREATER_THAN = "gt"
    EQUAL_OR_LESS_THAN = "lgt"
    LESS_THAN = "lt"

    def __init__(self,
                 file_date_pattern: str,
                 file_date_format: str,
                 operator: str,
                 search_date_value: str,
                 search_date_format: str):

        self.file_date_pattern = file_date_pattern
        self.file_date_format = file_date_format
        self.operator = operator
        self.search_date_value = search_date_value
        self.search_date_format = search_date_format

    def __extract_date_from_filename(self, file_path) -> datetime:
        return ifr_datetimes.get_datetime(
                os.path.basename(file_path),
                self.file_date_pattern,
                self.file_date_format
            )

    def accepts(self, filepath):
        search_date = ifr_datetimes.str_to_datetime(self.search_date_value, self.search_date_format)
        file_date = self.__extract_date_from_filename(filepath)
        comp = None
        if self.operator == BasenameDateFilter.LESS_THAN:
            comp = file_date < search_date
        if self.operator == BasenameDateFilter.EQUAL_OR_LESS_THAN:
            comp = file_date <= search_date
        if self.operator == BasenameDateFilter.GREATER_THAN:
            comp = file_date > search_date
        if self.operator == BasenameDateFilter.EQUAL_OR_GREATER_THAN:
            comp = file_date >= search_date
        if self.operator == BasenameDateFilter.EQUAL_TO:
            comp = file_date == search_date
        if comp is None:
            raise BadParameterException(f"Operator {self.operator} does not exists !")
        return comp


class TimeFilter(Filter):
    FILE_MTIME = "mtime"
    FILE_CTIME = "ctime"

    DELTA_UNIT_SECONDS = "seconds"
    DELTA_UNIT_MINUTES = "minutes"
    DELTA_UNIT_HOURS = "hours"
    DELTA_UNIT_DAYS = "days"

    def __init__(self, field: str = FILE_MTIME, delta_value: int = 1, delta_unit: str = DELTA_UNIT_MINUTES):
        self.delta_unit = delta_unit
        self.delta_value = delta_value
        self.field = field

    def _get_time(self, filepath):
        file = Path(filepath)
        if self.field == TimeFilter.FILE_CTIME:
            return file.ctime
        return file.mtime

    def accepts(self, filepath):
        return ifr_datetimes.is_time_older_or_younger_than(
            ref_time=self._get_time(filepath),
            delta_value=self.delta_value,
            delta_unit=self.delta_unit
        )


class LinkFilter(Filter):
    def accepts(self, file_path) -> bool:
        return os.path.islink(file_path)


class Path(object):
    @classmethod
    def from_parts(cls, *parts):
        # pylint: disable=E1120
        return cls(os.path.join(*parts))

    @classmethod
    def from_sys_path(cls, *parts):
        for dirname in sys.path:
            candidate = os.path.join(dirname, *parts)
            if os.path.exists(candidate):
                return cls(candidate)
        raise ItemNotFoundException(f"Can't find in syspath {os.sep.join(parts)}")

    def __init__(self, path):
        self.__path = path
        self._check_validity()

    def _check_validity(self):
        return

    @property
    def path(self):
        return self.__path

    @property
    def name(self):
        return os.path.basename(self.path)

    @property
    def stem(self):
        return os.path.splitext(self.name)[0]

    @property
    def extension(self):
        return os.path.splitext(self.name)[1]

    @property
    def mtime(self) -> float:
        return os.path.getmtime(self.path)

    @property
    def ctime(self) -> float:
        return os.path.getctime(self.path)

    def exists(self):
        return os.path.exists(self.path)

    def parts(self):
        return self.__path.split(os.sep)

    def __str__(self):
        return self.path

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        if isinstance(other, Path):
            return self.path == other.path
        if isinstance(other, str):
            return self.path == other
        return NotImplemented

    def _build_times(self, new_mtime: datetime):
        utime = time.mktime(new_mtime.timetuple())
        if not self.exists():
            return utime, utime
        st = os.stat(self.path)
        return st.st_atime, utime

    def create(self, create_parent: Optional[bool] = True):
        raise NotImplementedError

    def delete(self):
        raise NotImplementedError

    def touch(self, create: Optional[bool] = True, new_mtime: Optional[datetime] = None):
        """Create or modify update time of a path"""
        if not self.exists() and create is True:
            self.create()
        os.utime(self.__path, times=None if new_mtime is None else self._build_times(new_mtime))
        return self

    def parent(self):
        return Folder(os.path.dirname(self.path))

    def is_empty(self) -> bool:
        raise NotImplementedError


class Folder(Path):

    def _check_validity(self):
        if self.exists() and not os.path.isdir(self.path):
            raise BadParameterException(f"{self.path} is not a folder !")

    def create(self, create_parent: Optional[bool] = True):
        if self.exists():
            return self
        if create_parent is True:
            os.makedirs(self.path)
        else:
            os.mkdir(self.path)
        return self

    def delete(self):
        if self.exists():
            os.rmdir(self.path)
        return self

    def rmtree(self):
        if self.exists():
            shutil.rmtree(self.path)
        return self

    def clean(self):
        for folder in self.find(filter=DirectoryFilter(), format_fct=lambda x: Folder(x)):
            folder.rmtree()
        for file in self.find(filter=FileFilter(), format_fct=lambda x: File(x)):
            file.delete()
        return self

    def clean_or_create(self, create_parent: Optional[bool] = True):
        if self.exists():
            return self.clean()
        return self.create(create_parent)

    def find(self,
             filter: Filter = NoFilter(),
             ignore: Optional[Filter] = None,
             depth: Optional[int] = None,
             format_fct: Callable[[str], any] = build_path) -> List[Path]:
        return PathFinder(self.path, filter=filter, ignore=ignore, depth=depth).get_paths(format_fct=format_fct)

    def find_in_archive(self,
                        search_date: datetime,
                        date_folder_pattern: Optional[str] = '%Y/%j',
                        filter: Filter = NoFilter(),
                        ignore: Optional[Filter] = None,
                        depth: Optional[int] = None,
                        format_fct: Callable[[str], any] = build_path) -> List[Path]:

        return ArchivePathFinder(
            os.path.join(self.path, date_folder_pattern),
            search_date=search_date,
            filter=filter,
            ignore=ignore,
            depth=depth
        ).get_paths(format_fct=format_fct)

    def new_folder(self, *parts):
        return Folder.from_parts(self.path, *parts)

    def new_file(self, *parts):
        return File.from_parts(self.path, *parts)

    def is_empty(self) -> bool:
        if not self.exists():
            return True
        return len(self.find(depth=1)) == 0


class File(Path):

    def _check_validity(self):
        if self.exists() and not os.path.isfile(self.path):
            raise BadParameterException(f"{self.path} is not a file !")

    def create(self, create_parent: Optional[bool] = True):
        if self.exists():
            return self

        if create_parent is True:
            self.parent().create()

        with open(self.path, 'a'):
            os.utime(self.path)

        return self

    def delete(self):
        if self.exists():
            os.rmdir(self.path)
        return self

    def is_empty(self) -> bool:
        if not self.exists():
            return False
        return os.stat(self.path).st_size == 0


class PathFinder(YamlSerializable):

    def __init__(self,
                 folder: str,
                 filter: Filter = NoFilter(),
                 ignore: Optional[Filter] = None,
                 abspath: Optional[bool] = False,
                 depth: Optional[int] = None,
                 silent: Optional[bool] = False):

        self.folder = Folder(folder)
        self.filter = filter
        self.ignore = ignore
        self.abspath = abspath
        self.depth = depth
        self.silent = silent

        self._scanned = False
        self._found_paths = list()

    def __eq__(self, other):
        return self.folder.path == other.folder.path and \
            self.abspath == other.abspath and \
            self.depth == other.depth and \
            self.filter == other.filter

    def __str__(self):
        return str({
            'folder': self.folder,
            'filter': self.filter,
            'ignore': self.ignore,
            'abspath': self.abspath,
            'depth': self.depth
        })

    def exists(self) -> bool:
        if self.silent is True:
            return any(os.path.exists(p) and os.path.isdir(p) for p in self.search_path)
        return all(os.path.exists(p) and os.path.isdir(p) for p in self.search_path)

    @property
    def search_path(self) -> List[str]:
        return [self.folder.path]

    @property
    def paths(self) -> list:
        return self.get_paths()

    def get_paths(self, format_fct: Callable[[str], any] = None) -> list:
        if not self._scanned:
            self.scan()

        if format_fct is None:
            return self._found_paths

        return [format_fct(f) for f in self._found_paths]

    def scan(self):
        self._found_paths = list()
        for path in self.search_path:
            if not os.path.exists(path):
                if self.silent is False:
                    raise FileNotFoundException(f"Directory does not exist : {path}")
                continue

            self._found_paths.extend(pathfinder.walk_and_filter(
                path,
                self.filter,
                self.ignore,
                self.abspath,
                self.depth)
            )

        self._scanned = True
        return self

    def __len__(self) -> int:
        if not self._scanned:
            self.scan()
        return len(self.paths)


class ArchivePathFinder(PathFinder):

    def __init__(self,
                 folder: str,
                 search_date: datetime,
                 to_date: Optional[datetime] = None,
                 filter: Filter = NoFilter(),
                 ignore: Optional[Filter] = None,
                 abspath: Optional[bool] = False,
                 depth: Optional[int] = None,
                 silent: Optional[bool] = False):

        super(ArchivePathFinder, self).__init__(folder, filter, ignore, abspath, depth, silent)
        self.search_date = search_date
        self.to_date = to_date

    def __eq__(self, other):
        return super(ArchivePathFinder, self).__eq__(other) and \
            self.search_date == other.search_date and \
            self.to_date == other.to_date

    @property
    def search_path(self) -> List[str]:
        if self.to_date is not None:
            dates = ifr_datetimes.days_between(self.search_date, self.to_date)
            return [d.strftime(self.folder.path) for d in dates]

        return [self.search_date.strftime(self.folder.path)]


# ---------------------------------------------------------------
# YAML Codec
# ---------------------------------------------------------------
class PathFinderCodec(IfrYamlCodec):
    _yaml_prefix = "pathfinder"
    _eligible_classes = [PathFinder, ArchivePathFinder]

    @classmethod
    def to_yaml_dict(cls, obj) -> dict:
        # Encode the given object and also return the tag that it should have
        dct = {'folder': obj.folder.path}
        for path in ['filter', 'ignore', 'depth', 'search_date', 'abspath']:
            if hasattr(obj, path) and getattr(obj, path) is not None:
                dct[path] = getattr(obj, path)
        return dct


class FilterCodec(IfrYamlCodec):
    _yaml_prefix = "filter"
    _eligible_classes = [
        NoFilter, AndFilter, OrFilter, NotFilter,
        DirectoryFilter, FileFilter, LinkFilter,
        RegexFilter, FnmatchFilter,
        BasenameRegexFilter, BasenameFnmatchFilter, BasenameDateFilter,
        TimeFilter, SizeFilter
    ]

    @classmethod
    def to_yaml_dict(cls, obj) -> Any:
        if isinstance(obj, RegexFilter):
            return {'regex': obj.regex.pattern}
        return super(FilterCodec, cls).to_yaml_dict(obj)


# register codecs
FilterCodec.register()
PathFinderCodec.register()
