import logging
import time
from typing import Any, Callable, Optional

from ifr_lib.ifr_exception import GenericException


class RetryConfig(object):
    def __init__(self,
                 nb_tries: Optional[int] = 3,
                 delay: Optional[float] = 0.5,
                 max_delay: Optional[int] = None,
                 multiplier_delay: Optional[float] = 2,
                 extra_fix_delay: Optional[float] = 0):

        self.nb_tries = nb_tries if nb_tries else 3
        self.delay = delay if delay else 1
        self.max_delay = max_delay
        self.multiplier_delay = multiplier_delay if multiplier_delay else 2
        self.extra_fix_delay = extra_fix_delay if extra_fix_delay else 0


def retry_block(main_function: Callable,
                error_function: Optional[Callable] = None,
                exceptions=Exception,
                retry_config: Optional[RetryConfig] = None,
                logger: Optional[logging.Logger] = None,
                *args,
                **kwargs):

    __retry_config = retry_config if retry_config is not None else RetryConfig()
    __nb_tries = __retry_config.nb_tries
    __delay = __retry_config.delay
    __nb_attempt = 1

    while __nb_tries:
        try:
            return main_function(*args, **kwargs)
        except exceptions as e:
            __nb_tries -= 1
            __nb_attempt += 1
            if __nb_tries == 0:
                raise GenericException(e)

            if logger is not None:
                logger.warning(f"{e}\nretrying attempt number {__nb_attempt} in {__delay} seconds...")

            time.sleep(__delay)
            if error_function is not None:
                error_function
            __delay *= __retry_config.multiplier_delay
            __delay += __retry_config.extra_fix_delay

            if __retry_config.max_delay is not None:
                __delay = min(__delay, __retry_config.max_delay)
