#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides utilities for to compare objects
"""
from enum import Enum
from typing import Any, List, Optional

from ifr_lib import ifr_collections
from ifr_lib.ifr_exception import FileNotFoundException
from ifr_lib.ifr_path import Path, PathFinder
from ifr_lib.ifr_yaml import IfrYamlCodec, YamlSerializable


class Operator(Enum):
    """Abstract class to compare tow elements"""
    def eval(self, value1: any, value2: any) -> bool:
        """Evaluate first and second element"""
        return value1 == value2

    def expr(self, value1: any, value2: any) -> str:
        """Represent first and second element"""
        return f"{value1} == {value2}"


class BasicOperator(Operator):
    """Compare two numbers"""

    EQUAL_TO = "eq"
    EQUAL_OR_GREATER_THAN = "egt"
    GREATER_THAN = "gt"
    EQUAL_OR_LESS_THAN = "lgt"
    LESS_THAN = "lt"

    def eval(self, value1: any, value2: any) -> bool:
        comp = value1 == value2
        if self == BasicOperator.LESS_THAN:
            comp = value1 < value2
        elif self == BasicOperator.EQUAL_OR_LESS_THAN:
            comp = value1 <= value2
        elif self == BasicOperator.GREATER_THAN:
            comp = value1 > value2
        elif self == BasicOperator.EQUAL_OR_GREATER_THAN:
            comp = value1 >= value2
        return comp

    def expr(self, value1: any, value2: any) -> str:
        if self == BasicOperator.LESS_THAN:
            return f"{value1} < {value2}"
        if self == BasicOperator.EQUAL_OR_LESS_THAN:
            return f"{value1} <= {value2}"
        if self == BasicOperator.GREATER_THAN:
            return f"{value1} > {value2}"
        if self == BasicOperator.EQUAL_OR_GREATER_THAN:
            return f"{value1} >= {value2}"
        return f"{value1} == {value2}"


class PathListOperator(Operator):
    """Compare two list of paths"""
    MATCH_BASENAME = "mb"
    MATCH_BASENAME_WITHOUT_EXTENSION = "mbwe"

    def __format_file(self, path):
        path = path if isinstance(path, Path) else Path(path)
        if self == PathListOperator.MATCH_BASENAME_WITHOUT_EXTENSION:
            return path.stem
        return path.path

    def __format(self, paths):
        return [self.__format_file(f) for f in paths]

    def eval(self, value1: list, value2: list) -> bool:
        return ifr_collections.list_equals(self.__format(value1), self.__format(value2))

    def expr(self, value1: list, value2: list) -> str:
        return f"Compare list {self.__format(value1)} and {self.__format(value2)}"


class Comparator(YamlSerializable):
    """Abstract class to compare two elements"""
    def __init__(self, input: Any, operator: Operator, compare_to: Any):
        self.operator = operator
        self.input = input
        self.compare_to = compare_to
        self._res: Optional[bool] = None

    def eval(self) -> bool:
        self._res = self.operator.eval(self.input_value(), self.compare_to_value())
        return self._res

    @property
    def res(self):
        return self._res

    def report(self) -> list:
        return [{'res': self.res, 'expr': self.expr(), 'src': self.to_yaml()}]

    def expr(self) -> str:
        return self.operator.expr(self.input_value(), self.compare_to_value())

    def input_value(self) -> Any:
        return self.input

    def compare_to_value(self) -> Any:
        return self.compare_to


class FileComparator(Comparator):
    def eval(self) -> bool:
        try:
            return super(FileComparator, self).eval()
        except FileNotFoundException:
            self._res = False
            return False

    def expr(self) -> str:
        try:
            return super(FileComparator, self).expr()
        except FileNotFoundException as e:
            return str(e)


class FileCountComparator(FileComparator):
    """Compare number of elements in a folder with a number"""
    def __init__(self, input: PathFinder, operator: BasicOperator, compare_to: int):
        if isinstance(operator, str):
            operator = BasicOperator(operator)
        super(FileCountComparator, self).__init__(input, operator, compare_to)

    def input_value(self) -> int:
        return len(self.input)


class FileListComparator(FileComparator):
    """Compare list of elements in a folder with a list"""
    def __init__(self, input: PathFinder, operator: PathListOperator, compare_to: PathFinder):
        if isinstance(operator, str):
            operator = PathListOperator(operator)
        super(FileListComparator, self).__init__(input, operator, compare_to)

    def input_value(self) -> List[Path]:
        return self.input.paths

    def compare_to_value(self) -> List[Path]:
        return self.compare_to.paths


class LogicalComparator(Comparator, list):
    def __init__(self, *args):
        list.__init__(self, args)

    def __to_yaml_dict__(self):
        return [sub_comparator for sub_comparator in self]

    def report(self) -> list:
        res = list()
        for sub_comparator in self:
            sub_report = sub_comparator.report()
            if sub_report is not None:
                res.extend(sub_report)
        return res


class AndComparator(LogicalComparator):
    def eval(self) -> bool:
        """ Returns True if all of the filters in this filter return True. """
        return all(sub_comparator.eval() for sub_comparator in self)


class OrComparator(LogicalComparator):
    def eval(self) -> bool:
        """ Returns True if all of the filters in this filter return True. """
        return any(sub_comparator.eval() for sub_comparator in self)


# ---------------------------------------------------------------
# YAML Codec
# ---------------------------------------------------------------
class ComparatorCodec(IfrYamlCodec):
    _yaml_prefix = "comparator"
    _eligible_classes = [FileCountComparator, FileListComparator, AndComparator, OrComparator]

    @classmethod
    def to_yaml_dict(cls, obj) -> dict:
        if not isinstance(obj, LogicalComparator):
            return {
                'operator': obj.operator.value,
                'input': obj.input,
                'compare_to': obj.compare_to,
            }

        return super(ComparatorCodec, cls).to_yaml_dict(obj)


# register codecs
ComparatorCodec.register()
