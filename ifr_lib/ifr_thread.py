#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides utilities to manage threads.

"""
import ctypes
import inspect
import threading
from typing import Callable, Optional


def call_timeout(fct: Callable, timeout: Optional[float] = 0.5) -> bool:
    """Checks if a function is executed in the deadline.

    Args:
        fct (function): function to be called in a thread
        timeout (float): delay to wait for a response from the function

    Returns:
        True if the timeout is not exceeded. Otherwise False

    """
    thread = Thread(target=fct)
    thread.start()
    thread.join(timeout)
    try:
        thread.terminate()
    except threading.ThreadError:
        return True
    except Exception:  # pylint: disable=W0703
        pass

    return False


def _async_raise(tid, exctype):
    """raises the exception, performs cleanup if needed"""
    if not inspect.isclass(exctype):
        raise TypeError("Only types can be raised (not instances)")
    res = ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, ctypes.py_object(exctype))
    if res == 0:
        raise ValueError("invalid thread id")
    if res != 1:
        # """if it returns a number greater than one, you're in trouble,
        # and you should call it again with exc=NULL to revert the effect"""
        ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, 0)
        raise SystemError("PyThreadState_SetAsyncExc failed")


class Thread(threading.Thread):
    """Killable Thread"""

    _thread_id = None

    def _get_my_tid(self):
        """determines this (self's) thread id"""
        if not self.is_alive():
            raise threading.ThreadError("the thread is not active")

        # do we have it cached?
        if hasattr(self, "_thread_id"):
            return self._thread_id  # pylint: disable=E0203

        # no, look for it in the _active dict
        for tid, tobj in threading._active.items():  # pylint: disable=W0212
            if tobj is self:
                self._thread_id = tid
                return tid

        raise AssertionError("could not determine the thread's id")

    def raise_exc(self, exctype):
        """raises the given exception type in the context of this thread"""
        _async_raise(self._get_my_tid(), exctype)

    def terminate(self):
        """raises SystemExit in the context of the given thread, which should
        cause the thread to exit silently (unless caught)"""
        self.raise_exc(SystemExit)
