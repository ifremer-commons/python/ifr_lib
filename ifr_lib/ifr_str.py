#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Provides some utilities to manage strings.
"""

from hmac import compare_digest
from hashlib import blake2b  # pylint: disable=no-name-in-module
import re
import string
from typing import Any, Optional

from colorlog.escape_codes import escape_codes, parse_colors

from ifr_lib import ifr_template


def str_format(value: str, **kwargs) -> str:
    """Safe string formatting (avoids raise if a token is missing)

    Args:
        value (str): the string to format
        **kwargs: replacement tokens

    Returns:
        The formatted string
    """
    class SafeDict(dict):
        """Returns the original key if value is missing"""
        def __missing__(self, key):
            return '{' + key + '}'

    return string.Formatter().vformat(value, (), SafeDict(**kwargs))


def ljust(value: Any, ljust_width: Optional[int] = None) -> str:
    """Adjust string length with spaces

     Args:
        value (str): the string to format
        ljust_width (int): string minimum length

    Returns:
        The formatted value
    """
    value = str(value)
    if ljust_width is None:
        return value

    return value.ljust(ljust_width + (len(value) - ansilen(value)))


def ansilen(content: str) -> int:
    """Returns the length of a string as it would be without common ANSI control codes.

    The check of string type not needed for pure string operations, but remembering we are using this to
    monkey-patch len(), needed because textwrap code can and does use len() for non-string measures.

    Args:
        content (str): The input string

    Returns:
        The content's ansi length

    """
    return len(strip_color(content))


def strip_color(content: str) -> str:
    """Removes ANSI color/style sequences from a string.

    Args:
        content (str): The input string

    Returns:
        The color free string
    """
    return re.compile('\x1b\\[(K|.*?m)').sub('', content)


def colorize(content: str, color_options: str, message_length: Optional[int] = None) -> str:
    """Colorized string with color options.

     Args:
        content (str): the string to format
        color_options (str): color options (ex: `blue`, `bold_blue,bg_white`, ...)
        message_length (int): message length (ex: 120)

    Returns:
        The formatted value
    """
    if color_options:
        content = f"{parse_colors(color_options)}{content}{escape_codes['reset']}"

    if message_length:
        content = ljust(content, message_length)

    return content


def interpolate(content: Any,
                replacements: Optional[dict] = None,
                j2_env_params: Optional[dict] = None,
                raise_if_undefined: Optional[bool] = False) -> Any:

    """Interpolates content using Jinja2 Template

    Args:
        content (Any): the string to format
        replacements (dict): tokens to use for replacement
        j2_env_params (dict): see ifr_template.TOKEN_STYLE_BRACKET or ifr_template.TOKEN_STYLE_DOLLAR
        raise_if_undefined (bool): if True, raise an ItemNotFoundException

    Returns:
        The formatted input content

    Raises:
        ItemNotFoundException: if a token is missing and raise_if_undefined is True
    """
    return ifr_template.BaseTemplateRenderer(
        hide_undefined_tokens=False,
        raise_if_undefined=raise_if_undefined,
        j2_env_params=j2_env_params if j2_env_params is not None else ifr_template.TOKEN_STYLE_BRACKET
    ).interpolate(content=content, replacements=replacements)


def hash_value(value: str, digest_size: Optional[int] = 16, salt: Optional[str] = None) -> str:
    """Get the hash code of a given value

    Args:
        value (str): The input value
        digest_size (int, optional): The digest size
        salt (str): The salt key

    Returns:
        The given value hash code
    """

    # skip if value is empty
    if value is None or len(value) == 0:
        return ''

    # transform string to bytes
    if isinstance(value, str):
        value = value.encode('utf-8')

    # prepare hash method
    if salt is not None:
        if isinstance(salt, str):
            salt = salt.encode('utf-8')
        h = blake2b(digest_size=digest_size, key=salt)
    else:
        h = blake2b(digest_size=digest_size)
    h.update(value)

    # return hash code
    return h.hexdigest()


def check_hash(value: str, compare_hash: str, digest_size: Optional[int] = 16, salt: Optional[str] = None) -> bool:
    """Checks a given hash and compare_hash

    Args:
        value (str): The hash to be checked
        compare_hash (str): The compare hash
        digest_size (int, optional): The hash length
        salt (str): The salt key

    Returns:
        A boolean indicating whether the hash and the compare hash are equal
    """
    return compare_digest(hash_value(value, digest_size=digest_size, salt=salt), compare_hash)
