#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides some utilities to manage XML content.
"""

from json import dumps
from os.path import isfile
from typing import List, Optional

import xmltodict
from lxml import etree

from ifr_lib.ifr_exception import (ConversionException, FileNotFoundException,
                                   MissingParameterException, SyntaxException)


def build_xml_node(node_name: str, value: Optional[str] = None, attributes: Optional[dict] = None) -> etree._Element:
    """Builds a node from passed arguments.

    Args:
        node_name (str): The node name
        value (str): The node value.
        attributes (dict): A dictionary representing the attributes

    Returns:
        The built node

    Raises:
        MissingParameterException: if node name is unset
    """
    if not node_name:
        raise MissingParameterException("Node name is not set.")

    node = etree.Element(node_name)
    if value:
        node.text = value
    if attributes:
        for key, value in attributes.items():
            node.attrib[key] = value
    return node


def add_sub_nodes(node: etree._Element, child_nodes: List[etree._Element]) -> etree._Element:
    """Adds children to a parent node.

    Args:
        node (Element): The parent to which the children will be attached
        child_nodes (List[etree._Element]): The list of the siblings to be attached to the parent

    Returns:
        The updated parent node

    Raises:
        MissingParameterException: if node name is unset
    """
    if not child_nodes:
        raise MissingParameterException("Child nodes is not set.")

    for child_node in child_nodes:
        if child_node is not None:
            node.append(child_node)

    return node


def read_xml_file(xml_file: str, remove_comments: Optional[bool] = True) -> etree._Element:
    """Converts an XML file to an ElementTree.

    Args:
        xml_file (str): The xml _source file
        remove_comments (bool): Remove comments option value. Defaults to `True`

    Returns:
        The element tree

    Raises:
        FileNotFoundException: if XML file does not exist
        SyntaxException: if XML document is invalid
    """
    if not isfile(xml_file):
        raise FileNotFoundException(f"XML file {xml_file} does not exist")

    parser = etree.XMLParser(remove_comments=remove_comments)
    try:
        tree = etree.parse(xml_file, parser)
    except etree.XMLSyntaxError as err:
        raise SyntaxException(f"Error while reading file {xml_file} : {str(err)}")
    return tree.getroot()


def write_xml_file(
        xml_file: str,
        xml_content: etree._Element,
        encoding: Optional[str] = 'UTF-8',
        xml_declaration: Optional[bool] = True,
        pretty_print: Optional[bool] = True,
        doc_type: Optional[str] = None):
    """Writes an xml content to a file.

    Args:
        xml_file (str): The output XML file path
        xml_content (lxml.etree.ElementTree): The xml content
        encoding (str): The encoding option value. Defaults to `UTF-8`
        xml_declaration (bool): The xml_declaration option value. Defaults to `True`
        pretty_print (bool): The pretty print option value. Defaults to `True`
        doc_type (str): The _doc type option value.

    """
    # write content to file
    with open(xml_file, 'wb') as xml_file:
        xml_file.write(etree.tostring(xml_content,
                                      encoding=encoding,
                                      xml_declaration=xml_declaration,
                                      pretty_print=pretty_print,
                                      doctype=doc_type))


def xml_to_json(xml_content: etree._Element, indent: Optional[int] = 4) -> str:
    """Exports an XML content to JSON.

    Args:
        xml_content (lxml.etree.ElementTree): The xml content to parse
        indent (int): Indentation spaces. Defaults to `4`

    Returns:
        The JSON stream

    Raises:
        ConversionException: if conversion is impossible

    """
    try:
        return dumps(xmltodict.parse(etree.tostring(xml_content)), indent=indent, separators=(',', ': '))
    except Exception as err:
        raise ConversionException(f"An error occurred while converting xml to json object : {str(err)}")


def xml_file_to_json(xml_file: str, indent: Optional[int] = 4) -> str:
    """Exports XML file content to JSON.

    Args:
        xml_file (str): Path to the XML file
        indent (int): Indentation spaces. Defaults to `4`

    Returns:
        The JSON stream

    Raises:
        FileNotFoundException: if XML file does not exist
        ConversionException: if conversion is impossible
    """
    if not isfile(xml_file):
        raise FileNotFoundException(f"XML file {xml_file} does not exist")
    return xml_to_json(read_xml_file(xml_file, True), indent)


def xml_values_list(xml_element: etree._Element, tag_name: str) -> List[str]:
    """Builds a list of values.

    Args:
        xml_element (lxml.etree.Element): the xml element
        tag_name (str): the tag name

    Returns:
        the string list of the values

    """
    values_list = []
    if tag_name:
        for item in xml_element.findall(tag_name):
            values_list.append(item.text)
    return values_list
