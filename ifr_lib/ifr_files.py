#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides some utilities to manage files and directories
"""

import gzip
import hashlib
import logging
import shutil
import os
import re
import sys
import time
from collections import OrderedDict
from datetime import datetime
from typing import Callable, List, Optional

from ifr_lib import ifr_os
from ifr_lib.ifr_exception import (ConversionException, FileNotFoundException,
                                   ItemNotFoundException)


def script_info(script_file: str) -> OrderedDict:
    """Retrieves executed script information

    Args:
        script_file (str): The script file path

    Returns:
        script information
    """
    return OrderedDict([
        ('full_path', os.path.abspath(script_file)),
        ('dir_name', parent_basename(script_file)),
        ('name', strip_extension(os.path.basename(script_file))),
        ('mtime', mtime(script_file))
    ])


def parent_dir(file_path: str) -> str:
    """Retrieves the absolute path to parent directory of a file.

    Args:
        file_path (str): path to file.

    Returns:
        The file's directory

    Examples:
        >>> print(dirname(iifr_files.py.py))
        ifr_lb
    """
    return os.path.dirname(os.path.abspath(file_path))


def parent_basename(file_path: str) -> str:
    """Retrieves the parent directory of a file. Resolves

    Args:
        file_path (str): path to file.

    Returns:
        The file's directory

    Examples:
        >>> print(dirname(iifr_files.py.py))
        ifr_lb
    """
    return os.path.basename(parent_dir(file_path))


def strip_extension(file_path: str) -> str:
    """Removes the extension from a file path

    Args:
        file_path (str): path to file.

    Returns:
        The file path without its extension

    Examples:
        >>> print(basename_splitext("ifr_lb/file.py"))
        file
    """
    return os.path.splitext(file_path)[0]


def basename_splitext_files(files: List[str]) -> List[str]:
    """return a list of filename without extension

    Args:
        files (list): a list o files

    Returns:
        list: normalized file list

    Examples:
        >>> print(basename_splitext_files(["ifr_lb/file.py", "ifr_lb/file2.py"]))
        [file, file2]
    """
    return [strip_extension(os.path.basename(f)) for f in files]


def mtime(file_path: str) -> time.struct_time:
    """Retrieves the last modification datetime of a file

    Args:
        file_path (str): path to file. Defaults to sys.argv[0] (current called python script)

    Returns:
        The last modification datetime of the file

    Examples:
        >>> print(mtime())
        time.struct_time(
            tm_year=2018, tm_mon=11, tm_mday=8, tm_hour=7,
            tm_min=49, tm_sec=48, tm_wday=3, tm_yday=312, tm_isdst=0
        )
    """
    return time.gmtime(os.stat(file_path).st_mtime)


def mtime_str(file_path: str, pattern: Optional[str] = "%Y-%m-%d %H:%M:%S%z") -> str:
    """Retrieves the last modification datetime of a file.

    Args:
        file_path (str): path to file.
        pattern (str): The format of the date to convert. Defaults to `%Y-%m-%dT%H:%M:%S%z`

    Returns:
        The last modification datetime of the file, formatted according to pattern

    Examples:
        >>> print(mtime())
        2018-11-08 07:49:48+0100
    """
    return time.strftime(pattern, mtime(file_path))


def is_mtime_older_than(file_path: str, delta_value: Optional[int] = 10, delta_unit: Optional[str] = 'minutes') -> bool:
    """Checks if file mtime is older than a delta time.

    Args:
        file_path (str): path to file.
        delta_value (int):
        delta_unit (str): unit amongst [seconds, minutes, hours, days]

    Returns:
        True if file mtime is older than a delta time, otherwise False

    Raises:
        ConversionException : invalid delta unit

    Examples:
        >>> print(is_mtime_older_than())
        True
    """
    conversion_dict = {'seconds': 1, 'minutes': 60, 'hours': 3600, 'days': 86400}
    try:
        factor = conversion_dict[delta_unit]
        return ((time.time() - os.path.getmtime(file_path)) / factor) >= delta_value
    except KeyError:
        raise ConversionException('"{}" is not a valid time unit. Time units list : {}'
                                  .format(delta_unit, ', '.join(conversion_dict.keys())))


def is_file_empty(file_path: str) -> bool:
    """Checks if file is empty (`size == 0`).

    Args:
        file_path (str): path to file.

    Returns:
        True if file is empty, otherwise False

    Examples:
        >>> print(is_file_empty())
        True
    """
    return os.stat(file_path).st_size == 0


def gzip_file(source_file_path: str,
              destination_file_path: Optional[str] = None,
              delete_source: Optional[bool] = False
              ):
    """Zips a file with or without deleting the _source.

    Args:
        source_file_path (str): The _source file path.
        destination_file_path (str): The destination file path. Same as the _source path + the passed extension by default.
        delete_source (bool): Indicates whether the _source file should be deleted, False by default.
    """

    zip_file_extension = 'gz'

    # define and create destination path
    if destination_file_path is None:
        destination_file_path = source_file_path
    else:
        parent_dir_path = parent_dir(destination_file_path)
        if not os.path.isdir(parent_dir_path):
            ifr_os.mkdirs(parent_dir_path)

    destination_file_path = destination_file_path + '.' + zip_file_extension
    # zip file
    with open(source_file_path, 'rb') as src, gzip.open(destination_file_path, 'wb') as zipped_file:
        zipped_file.writelines(src)

    # delete _source
    if delete_source:
        os.remove(source_file_path)

    return destination_file_path


def gunzip(
        source_file_path: str,
        destination_file_path: Optional[str] = None,
        delete_source: Optional[bool] = False
):
    """Dezips a file with or without deleting the _source.

       Args:
            source_file_path (str): The _source file path.
            destination_file_path (str): The destination file path. Same as the _source path + the passed extension by default.
            delete_source (bool): Indicates whether the _source file should be deleted, False by default.
       """
    # define and create destination path
    if destination_file_path is None:
        destination_file_path = os.path.splitext(source_file_path)[0]
    else:
        destination_dir_path = parent_dir(destination_file_path)
        if not os.path.exists(destination_dir_path):
            os.makedirs(destination_dir_path)

    with gzip.open(source_file_path, 'rb') as f_in:
        with open(destination_file_path, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)

    # delete _source
    if delete_source:
        os.remove(source_file_path)


def conversion_ratio(initial_file, converted_file):
    """
            Usage: Log conversion ratio
            Args:
                initial_file: filepath
                converted_file: filepath

    """

    initial_file_size = os.stat(initial_file).st_size
    converted_file_size = os.stat(converted_file).st_size
    ratio_conversion = round(initial_file_size/converted_file_size)

    return {
        "initial_file_size": initial_file_size,
        "converted_file_size": converted_file_size,
        "ratio_conversion": ratio_conversion,
        "initial_file": initial_file,
        "converted_file": converted_file
    }


def __find_in_syspath(pathname: str, match_fct: Optional[Callable] = os.path.isfile) -> str:
    """Finds an item in syspath

    Args:
        pathname (str): path of the item
        match_fct (fct): Match function. Defaults to `os.path.isfile`

    Returns:
        The absolute path of the item

    Raises:
        ItemNotFoundException: if the item is not found
    """
    for dirname in sys.path:
        candidate = os.path.join(dirname, pathname)
        if match_fct(candidate):
            return candidate
    raise ItemNotFoundException("Can't find pathname %s" % pathname)


def find_file_in_syspath(pathname: str) -> str:
    """Finds a file in syspath

    Args:
        pathname (str): path of the file

    Returns:
        The absolute path of the file

    Raises:
        ItemNotFoundException: if the file is not found
    """
    return __find_in_syspath(pathname)


def find_dir_in_syspath(pathname: str) -> str:
    """Finds a directory in syspath

    Args:
        pathname (str): path of the directory

    Returns:
        The absolute path of the directory

    Raises:
        ItemNotFoundException: if the directory is not found
    """
    return __find_in_syspath(pathname, os.path.isdir)


def get_hash(file_path: str) -> str:
    """Get the file hash

    Args:
        file_path (str): path to file.

    Returns:
        The file hash, a string of hexadecimal digits

    """
    if not os.path.isfile(file_path):
        raise FileNotFoundException("File does not exist : {}".format(file_path))
    sha256_hash = hashlib.sha256()
    with open(file_path, "rb") as file:
        # Read and update hash string value in blocks of 4K
        for byte_block in iter(lambda: file.read(4096), b""):
            sha256_hash.update(byte_block)

    return sha256_hash.hexdigest()


def to_unix_path(path: str) -> str:
    """Converts path to unix path (slash instead of backslash)

    Args:
        path: the path to convert

    Returns:
        The converted path

    """
    # escape multiple backslashes
    escape_many = re.compile(r"(?P<slash>\\[\\]+)")
    for elt in escape_many.findall(path):
        path = path.replace(elt, "?" * len(elt))

    # replace single backslash
    path = path.replace('\\', "/")

    # unescaped backslash
    return path.replace('?', "\\")


def touch(path: str, new_mtime: Optional[datetime] = None):
    """Create or modify update time of a file

    Args:
        path (str): path to file
        new_mtime (datetime): new modification time
    """
    assert new_mtime is None or isinstance(new_mtime, datetime)

    times = None
    if new_mtime is not None:
        utime = time.mktime(new_mtime.timetuple())

        if os.path.exists(path):
            st = os.stat(path)
            times = (st.st_atime, utime)
        else:
            times = (utime, utime)

    if os.path.isdir(path):
        os.utime(path, times=times)
    elif os.path.isfile(path):
        with open(path, 'a'):
            os.utime(path, times=times)
