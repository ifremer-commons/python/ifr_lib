#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides a Timer class, to manage execution duration.

    Provides a Timers class, to manage a dictionary of Timer.
"""

import datetime
import logging
import types
from collections import OrderedDict
from typing import Callable

from ifr_lib.ifr_datetimes import elapsed_time, now
from ifr_lib.ifr_exception import (ItemNotFoundException,
                                   MissingParameterException)


class Timer(object):
    """Represents a Timer object.

    A timer as a start and an end datetime.

    To start timer, use function `Timer.start()`.

    To start timer, use function `Timer.stop()`.

    To retrieve time duration, use function `Timer.duration`.

    Example:
        >>> import time
        >>> timer = Timer()
        >>> timer.start()
        >>> time.sleep(5)
        >>> timer.stop()
        >>> print(timer.duration)
        0:00:05.000731

    Attributes:
        start_datetime (datetime): the start date time
        end_datetime (datetime): the end date time
        duration (timedelta): the elapsed time between start and end datetime

    """

    def __init__(self):
        """Initializes attributes to None."""
        self.start_datetime = None
        self.end_datetime = None
        self.duration = None

    def start(self) -> bool:
        """Starts the timer (set start_datetime to now).

        Returns:
            True if timer can be started (i.e. start_datetime is None), otherwise False
        """
        if self.start_datetime:
            return False

        self.start_datetime = now()
        return True

    def stop(self) -> bool:
        """Stops the timer (set end_datetime to now).

        Returns:
            True if timer can be stopped (i.e. start_datetime is None), otherwise False
        """
        if self.end_datetime:
            return False
        self.end_datetime = now()
        self.duration = elapsed_time(self.start_datetime, self.end_datetime)
        return True

    def reset(self):
        """Resets attributes to None."""
        self.start_datetime = None
        self.end_datetime = None
        self.duration = None

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, *args):
        self.stop()


class Timers(object):

    def __init__(self):
        """Initializes timers map with an empty OrderedDict.

        Examples:
            >>> import time
            >>> timers = Timers()
            >>> timers.start('first')
            >>> time.sleep(5)
            >>> timers.start('second')
            >>> time.sleep(5)
            >>> timers.stop('second')
            >>> time.sleep(5)
            >>> timers.stop('first')
            >>> print(timers.durations())
            OrderedDict([
                ('first', datetime.timedelta(seconds=15, microseconds=1816)),
                ('second', datetime.timedelta(seconds=5, microseconds=541))
            ])
            >>> from ifr_lib import dict_to_str
            >>> print(dict_to_str(timers.durations(), title='TIMERS'))
            ----------------------------------------------------------------------
            TIMERS
            ----------------------------------------------------------------------
            - first: 0:00:15.001801
            - second: 0:00:05.000536
        """
        self.timers = OrderedDict()

    def contains(self, timer: str) -> bool:
        """Checks if timer exists.

        Args:
            timer (str): The timer name

        Returns:
            True if timer exists, otherwise False

        Raises:
            MissingParameterException: if timer is not set
        """
        if not timer:
            raise MissingParameterException('Timer is not set')

        return timer in self.timers.keys()

    def add(self, timer: str) -> bool:
        """Adds a new timer

        Args:
            timer (str): The timer name

        Returns:
            False if timer already exists, otherwise True

        Raises:
            MissingParameterException: if timer is not set
        """
        if not self.contains(timer):
            self.timers[timer] = Timer()
            return True
        return False

    def remove(self, timer: str) -> bool:
        """Removes an existing timer

        Args:
            timer (str): The timer name

        Returns:
            True if timer is removed, otherwise False

        Raises:
            MissingParameterException: if timer is not set
        """
        if self.contains(timer):
            del self.timers[timer]
            return True
        return False

    def start(self, timer: str) -> bool:
        """Starts a timer.

        Create the timer if not existing.

        Args:
            timer (str): The timer name

        Returns:
            True if timer is started, otherwise False
        """
        if not self.contains(timer):
            self.add(timer)
        return self.timers[timer].start()

    def stop(self, timer: str) -> bool:
        """Stops a timer.

        Args:
            timer (str): The timer name

        Returns:
            True if timer is stopped, otherwise False
        """
        if self.contains(timer):
            return self.timers[timer].stop()
        else:
            return False

    def duration(self, timer: str) -> datetime.timedelta:
        """Retrieves the duration of a timer.

        Args:
            timer (str): The timer name

        Returns:
            The time delta between start and end datetime.
        """
        if self.contains(timer):
            return self.timers[timer].duration

    def durations(self) -> OrderedDict:
        """Retrieve the duration of all timers.

        Returns:
            The duration of all timers
        """
        durations = OrderedDict()
        for name, timer in self.timers.items():
            durations[name] = timer.duration
        return durations

    def __getitem__(self, timer):
        if not self.contains(timer):
            raise ItemNotFoundException(f"Timer {timer} not found")
        return self.timers[timer]


def timefunc(fn: Callable, *args, **kwargs) -> Callable:
    """Function that times execution of a passed in function, returns a new function
    encapsulating the behavior of the original function
    """
    logger = logging.getLogger()

    def fncomposite(*args, **kwargs):
        timer = Timer()
        timer.start()
        rt = fn(*args, **kwargs)
        timer.stop()
        logger.debug(f"Executing {fn.__name__} took {timer.duration} seconds.")
        return rt

    # return the composite function
    return fncomposite


class Timed(type):
    """The `Timed` metaclass that replaces methods of its classes
    with new methods `timed` by the behavior of the composite function transformer
    """

    def __new__(cls, name, bases, attr):
        """Replaces each function with a new function that is timed
        run the computation with the provided args and return the computation result
        """
        for key, value in attr.items():
            if isinstance(value, [types.FunctionType, types.MethodType]):
                attr[key] = timefunc(value)

        return super(Timed, cls).__new__(cls, key, bases, attr)
