#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides utilities to manage SOAP client.
"""

from requests.exceptions import ConnectionError, HTTPError
from zeep import Client
from zeep.exceptions import XMLSyntaxError as ZeepXMLSyntaxError
from zeep.proxy import ServiceProxy

from ifr_lib.ifr_exception import GenericException


class SoapException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


def create_client(service_url: str) -> ServiceProxy:
    """Creates a SOAP client.

    Args:
        service_url (str): The service url

    Returns:
        The service proxy to access the methods (`zeep.proxy.OperationProxy`)

    Raises:
        SoapException: in case of client creation failure
    """
    try:
        client = Client(service_url)
        return client.service
    except (ConnectionError, HTTPError, ZeepXMLSyntaxError) as error:
        raise SoapException(f"An error occurred during SOAP request : {error}")
