#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides some utilities to manage Http client.
"""

import json
import re
import requests
from typing import Optional, Union, Dict
from urllib import parse


from ifr_lib.ifr_exception import GenericException


class CasException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class CasClient:
    """CAS SSO client

    Allow to connect to CAS and send GET or POST http requests.
    A session is used to store headers (cookies, ...)

    Example:
        >>> client = CasClient('cas.ifremer.fr', 'login_intranet', 'pwd_intranet', 'http//protected_url')
        >>> response = client.get('http//protected_url/myfile')
        >>> print(response.content.decode('utf8'))
    """
    # http session
    session: requests.Session = None
    # ticket granting ticket
    __tgt = None
    # service ticket
    __st = None
    # is authenticated to CAS
    __is_authenticated = False

    def __init__(self, cas_host: str, username: str, password: str, service_url: str, rest_endpoint: Optional[str] = None):
        self.cas_host = cas_host
        self.service_url = service_url
        self.rest_endpoint = rest_endpoint if rest_endpoint is not None else "/v1/tickets/"
        self.__credentials = parse.urlencode({'username': username, 'password': password})

    def ticket_granting_ticket(self) -> str:
        """Return ticket granting ticket"""
        return self.__tgt

    def service_ticket(self) -> str:
        """Return service ticket"""
        return self.__st

    def is_authenticated(self) -> bool:
        """Is the client connected to CAS ?"""
        return self.__is_authenticated

    def connect(self):
        """Connect to CAS server if not already authenticated

        Raises:
            CasException: if authentication failed
        """
        if not self.is_authenticated():
            self.__tgt = self.__retrieve_tgt()
            self.__st = self.__retrieve_st()
            self.__is_authenticated = True

    def disconnect(self):
        """Logout from CAS server"""
        if self.is_authenticated():
            self.session.get(f"https://{self.cas_host}/logout")
            self.__is_authenticated = False

    def __retrieve_tgt(self) -> str:
        """Retrieve ticket granting ticket

        Returns:
            str: ticket granting ticket

        Raises:
            CasException: if login failed
        """
        self.session = requests.session()
        response = self.session.post(
            f"https://{self.cas_host}{self.rest_endpoint}",
            data=self.__credentials,
            headers={
                "Content-type": "application/x-www-form-urlencoded",
                "Accept": "text/plain",
                "User-Agent": "python"
            }
        )
        try:
            location = response.headers['location']
        except KeyError:
            raise CasException(f"Authentication failed for https://{self.cas_host}{self.rest_endpoint} !")

        return location[location.rfind('/') + 1:]

    def __retrieve_st(self):
        """Retrieve service ticket

        Returns:
            str: service ticket
        """
        response = self.session.post(
            f"https://{self.cas_host}{self.rest_endpoint}{self.__tgt}",
            data=parse.urlencode({'service': self.service_url}),
            headers={
                "Content-type": "application/x-www-form-urlencoded",
                "Accept": "text/plain",
                "User-Agent": "python"
            })

        st = response.content.decode("utf-8")
        if re.search(r'ST-\d+-\w+-.*', st):
            return st

        raise CasException(f"Bad service ticket")

    def get(self, url: str, with_st: bool = True, **kwargs) -> requests.Response:
        """Send an HTTP GET request.

        Connect first to CAS server if not authenticated.

        Args:
            url: url to service or file
            with_st: add service ticket to url (ticket=<service_ticket>) if True
            **kwargs: request options (@see request.session.get)

        Returns:
            requests.Response: the response
        """
        self.connect()
        return self.session.get(url if with_st is False else add_url_params(url, {'ticket': self.__st}), **kwargs)

    def post(self,
             url: str,
             data: Optional[Dict] = None,
             xml_data: Optional[str] = None,
             json_data: Optional[str] = None,
             with_st: bool = True,
             **kwargs) -> requests.Response:
        """Send an HTTP GET request.

        Connect first to CAS server if not authenticated.

        Args:
            url: url to service or file
            data: request parameters
            xml_data: XML data (will set headers={'Content-Type': 'application/xml'} )
            json_data: JSON data (will set headers={'Content-Type': 'application/json'} )
            with_st: add service ticket to data (ticket=<service_ticket>) if True
            **kwargs: request options (@see request.session.post)

        Returns:
            requests.Response: the response
        """
        self.connect()

        headers = dict()
        if 'headers' in kwargs:
            headers = kwargs.pop('headers')

        if xml_data is not None:
            post_data = xml_data
            headers['Content-Type'] = 'application/xml'
        elif json_data is not None:
            post_data = json_data
            headers['Content-Type'] = 'application/json'
        else:
            if data is None:
                data = dict()
            if with_st:
                data['ticket'] = self.__st
            post_data = data

        return self.session.post(url, data=post_data,  headers=headers, **kwargs)


def add_url_params(url: str, params: dict) -> str:
    """Add GET params to provided URL being aware of existing.

    Args:
        url: string of target URL
        params: dict containing requested params to be added

    Returns:
        str: string with updated URL

    Examples:

        >> url = 'http://stackoverflow.com/test?answers=true'
        >> new_params = {'answers': False, 'data': ['some','values']}
        >> add_url_params(url, new_params)
        'http://stackoverflow.com/test?data=some&data=values&answers=false'
    """
    # Unquoting URL first so we don't loose existing args
    url = parse.unquote(url)
    # Extracting url info
    parsed_url = parse.urlparse(url)
    # Extracting URL arguments from parsed URL
    get_args = parsed_url.query
    # Converting URL arguments to dict
    parsed_get_args = dict(parse.parse_qsl(get_args))
    # Merging URL arguments dict with new params
    parsed_get_args.update(params)
    # Bool and Dict values should be converted to json-friendly values
    # you may throw this part away if you don't like it :)
    parsed_get_args.update(
        {k: json.dumps(v) for k, v in parsed_get_args.items()
         if isinstance(v, (bool, dict))}
    )
    # Converting URL argument to proper query string
    encoded_get_args = parse.urlencode(parsed_get_args, doseq=True)
    # Creating new parsed result object based on provided with new
    # URL arguments. Same thing happens inside of urlparse.
    return parse.ParseResult(
        scheme=parsed_url.scheme,
        netloc=parsed_url.netloc,
        path=parsed_url.path,
        params=parsed_url.params,
        query=encoded_get_args,
        fragment=parsed_url.fragment
    ).geturl()
