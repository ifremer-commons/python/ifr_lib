#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides a class YamlConfig, to manage configuration from YAML syntax.
"""

import functools
import logging
import os
from collections import OrderedDict
from typing import Any, Iterable, Optional, Type, Union

import ruamel.yaml as yaml

from ifr_lib import ifr_collections, ifr_exception, ifr_ini, ifr_template

ALL_PYYAML_LOADERS = (yaml.Loader, yaml.SafeLoader)
ALL_PYYAML_DUMPERS = (yaml.Dumper, yaml.SafeDumper)

YamlType = Union[str, list, dict]


class YamlConfig:
    """Loads a yaml content from a file or a stream.

    Can retrieve all leaf keys.
    Can access key in different ways.

    Can replace tokens (default ${key}) from a key / value pairs of items.
    See args : replacements, token_prefix, token_suffix, warn_if_orphaned_keys

    Can merge with another YamlConfig object.

    Can export content to dict, ini format.

    Attributes:
        content: YAML content as OrderedDict
        leaf_keys: list of all leaf keys
        orphan_keys: dictionary of orphaned keys (key/counter)

    Examples:

        - load YAML content :

            $ yaml_content=\"""\\
                # example
                name:
                  # details
                  family: Smith   # very common
                  given: Alice    # one of the siblings
                \"""
            $ config = YamlConfig(stream=yaml_content)

        - print content :

            $ print(config)
            name:
              family: Smith
              given: Alice

        - export content :

            $ print(dict_to_str(config.to_dict()))
            - name.family: Smith
            - name.given: Alice
            $ print(config.to_ini())
            [name]
            family = Smith
            given = Alice

        - retrieve property via array syntax :

            $ print(config['name'])
            {'family': 'Smith', 'given': 'Alice'}
            $ print(config['name.family'])
            Smith
            $ print(config['name']['family'])
            Smith

        - retrieve property via box :

            $ print(config.box.name)
            {'family': 'Smith', 'given': 'Alice'}
            $ print(config.box.name.family)
            Smith

        - replacements :

            $ yaml_content2=\"""\\
                my_project:
                  id: 3
                  group: ${name.family}
                  other: ${fake.key.one}
                \"""
            $ config2 = YamlConfig(stream=yaml_content2, replacements=config.to_dict())
            $ print(config2)
            my_project:
              group: Smith
              id: 3
              other: ${fake.key.one}
            $ print(config2.orphan_keys)
            Counter({'fake.key.one': 1})

        - merge :

            $ yaml_content3=\"""\\
                my_project:
                  other: test
                \"""
            $ config3 = YamlConfig(stream=yaml_content3, replacements=config.to_dict())
            $ config2.merge(config3)
            $ print(config2)
            my_project:
              group: Smith
              id: 3
              other: test
    """

    def __init__(self,
                 content: Any = None,
                 file: Optional[str] = None,
                 replacements: Optional[dict] = None,
                 j2_renderer: Optional[ifr_template.BaseTemplateRenderer] = None,
                 encoding: Optional[str] = None):
        """Initializes attributes and load yaml content from file or from stream.

        Also :
        - replaces tokens if requested
        - retrieves orphaned keys
        - transforms yaml content to box
        - load all leaf keys

        Args:
            content (str | stream | binary | dict, optional) : YAML content
            file (str) : YAML file
            replacements (dict): key / value pairs of items to replace
            j2_renderer : Jinja2 template renderer

        Raises:
            FileNotFoundException: YAML file is not found
            SyntaxException: YAML content is invalid
        """
        # init logger
        self._log = logging.getLogger(__name__)

        # init attributes
        self.content = None
        self.orphan_keys = None
        self.leaf_keys = None
        self.encoding = 'utf-8' if encoding is None else encoding

        # retrieve options
        self.replacements = replacements

        if j2_renderer is None:
            self._renderer = ifr_template.BaseTemplateRenderer(
                j2_env_params=ifr_template.TOKEN_STYLE_DOLLAR,
                raise_if_undefined=True
            )
        else:
            self._renderer = j2_renderer

        # load yaml content
        if content is not None:
            self.__load_yaml(content, None)
        elif file is not None:
            if not os.path.isfile(file):
                raise ifr_exception.FileNotFoundException(f"Yaml file doesn't exists : {file}")
            self.__load_yaml(None, file)

        # load orphaned keys
        self.__update_orphan_keys()

        # load all keys
        self.__load_leaf_keys()

    @staticmethod
    def dump(data, dumper=yaml.RoundTripDumper):
        """Serialize a Python object into a YAML stream.
        If stream is None, return the produced string instead.
        """
        return yaml.safe_dump(data, Dumper=dumper)

    def __load_yaml(self, content, file_path):
        """Loads a YAML document.

        Replace tokens if requested.

        Raises:
            SyntaxException: YAML content is invalid
        """
        try:

            if file_path is not None:
                with open(file_path, 'rt', encoding=self.encoding) as stream:
                    content = yaml.load(stream, Loader=yaml.RoundTripLoader)
            elif not isinstance(content, dict):
                # Parse yaml content and make it a dict
                content = yaml.load(content, Loader=yaml.RoundTripLoader)

            # interpolate
            if self.replacements and content:
                self.content = self._renderer.interpolate(content, ifr_collections.map_to_dict(self.replacements))
            else:
                self.content = content

            if hasattr(content, "close"):
                content.close()

        except Exception as err:
            if hasattr(content, "close"):
                content.close()

            raise ifr_exception.SyntaxException(f"Yaml syntax problem (ScannerError) {str(err)}")

    def __update_orphan_keys(self):
        """Retrieves orphaned keys."""
        self.orphan_keys = None
        self.orphan_keys = self._renderer.find_undeclared_variables(
            content=self.content,
            replacements=self.replacements
        )

    def __load_leaf_keys(self):
        """Retrieves all leaf keys from Box object."""
        def iterate_dict(dictionary, parents=None):
            if parents is None:
                parents = []
            result = []
            if dictionary is not None and hasattr(dictionary, "items"):
                for key, value in dictionary.items():
                    if isinstance(value, dict):
                        result.extend(iterate_dict(value, parents + [key]))
                    else:
                        if parents:
                            result.append('.'.join(parents) + '.' + key)
                        else:
                            result.append(key)

            return result
        self.leaf_keys = None
        self.leaf_keys = iterate_dict(self.content)

    def merge(self, obj):
        """Merges the content with that of another YamlConfig instance.

        Also : updates leaf keys, orphaned keys.

        Args:
            obj (YamlConfig|dict): a YamlConfig instance

        Raises:
            BadTypeException: yaml_config_instance is not an instance of YamlConfig

        """
        # skip None value
        if obj is None:
            return

        if not isinstance(obj, (YamlConfig, dict)):
            raise ifr_exception.BadTypeException(
                f"Object to merge is not an instance of YamlConfig or dict : {type(obj)}"
            )

        if isinstance(obj, YamlConfig):
            dict_to_merge = obj.content
        else:
            dict_to_merge = obj

        if self.content is None:
            self.content = dict_to_merge
        else:
            self.content = ifr_collections.merge_dict(self.content, dict_to_merge)

        # load orphaned keys
        self.__update_orphan_keys()

        # load all keys
        self.__load_leaf_keys()

    def is_leaf(self, item: str) -> bool:
        """Checks if an item is leaf (no child)

        Args:
            item (str):

        Returns:
            bool: True if the item is a leaf, otherwise False

        """
        return item in self.leaf_keys

    def contains(self, item):
        """Checks if key exists.

        Args:
            item (str): a key

        Returns:
            bool: True if key exists, otherwise False
        """
        # try to retrieve item
        try:
            self[item]
            return True
        except ifr_exception.ItemNotFoundException:
            return False

    def __getitem__(self, item: str) -> Any:
        """Retrieves item by key using array convention.

        Args:
            item (str): a key

        Returns:
            bool: True if key exists, otherwise False

        Raises:
            ItemNotFoundException: key does not exist
        """
        if self.content is None:
            raise ifr_exception.ConfigurationException('Config is empty !')

        try:
            if "." not in item:
                return self.content[item]
            return functools.reduce(
                (lambda level, k: level.get(k)), item.split("."), self.content
            )
        except (KeyError, AttributeError):
            raise ifr_exception.ItemNotFoundException(f"Item {item} does not exist.")

    def __setitem__(self, key, value):
        self.merge(ifr_collections.to_dict(key, value))

    def __contains__(self, item):
        return self.contains(item)

    def __str__(self):
        """Prints YAML document.

        Returns:
            str: YAML content
        """
        return yaml.dump(self.content, Dumper=yaml.RoundTripDumper)

    def keys(self):
        """Retrieves first level keys

        Returns:
            list: first level keys
        """
        return self.content.keys()

    def leafs(self, start_with=None):
        """Exports YAML leaf nodes to dictionary (leaf key / value).

        Returns:
            dict: YAML document as dictionary
        """
        return OrderedDict((k, self[k]) for k in self.leaf_keys if (start_with is None or k.startswith(start_with)))

    def as_dict(self):
        """Retrieves configuration as dictionary."""
        return self.content

    def __get_item(self, item=None, check_is_not_leaf=False):
        """Retrieves all or a fragment of the configuration.

        Args:
            item (str, optional): item to export. Defaults to None (=all)
            check_is_not_leaf (bool): should we raise an exception if item is a leaf. Defaults to False

        Returns:
            any: item value

        Raises:
            BadParameterException: if item is a leaf and check_is_not_leaf is True
        """
        if item:
            if check_is_not_leaf and self.is_leaf(item):
                raise ifr_exception.BadParameterException("Item {} is a leaf item".format(item))
            return self[item]
        return self.content

    def to_yaml(self, item=None):
        """Exports configuration or a fragment to YAML format.

        Args:
            item (str, optional): item to export. Defaults to None (=all)

        Returns:
            str: a configuration to YAML format

        Example:
            $ yaml_content=\"""\\
            name:
              family: Smith
              given: Alice
            test:
              sub:
                a: b
                c: d
            \"""
            $ config = YamlConfig(stream=yaml_content)
            $ print(config.to_yaml("test"))
        """
        return yaml.dump(self.__get_item(item), Dumper=yaml.RoundTripDumper)

    def to_json(self, item: Optional[str] = None) -> str:
        """Exports configuration or a fragment to JSON format.

        Args:
            item (str): item to export. Defaults to None (=all)

        Returns:
            A configuration to JSON format

        Example:
            $ yaml_content=\"""\\
            name:
              family: Smith
              given: Alice
            test:
              sub:
                a: b
                c: d
            \"""
            $ config = YamlConfig(stream=yaml_content)
            $ print(config.to_json("test"))
            {"name": {"family": "Smith", "given": "Alice"}, "test": {"sub": {"a": "b", "c": "d"}}}
        """
        from ifr_lib.ifr_json import json_dumps
        return json_dumps(self.__get_item(item))

    def to_ini(self, item: Optional[str] = None) -> str:
        """Exports configuration or a fragment to INI format.

        Args:
            item (str, optional): item to export. Defaults to None (=all)

        Returns:
            A configuration to INI format

        Example:
            $ yaml_content=\"""\\
            name:
              family: Smith
              given: Alice
            test:
              sub:
                a: b
                c: d
            \"""
            $ config = YamlConfig(stream=yaml_content)
            $ print(config.to_ini("test"))
            [sub]
            a = b
            c = d
            $ print(config.to_ini("test.sub"))
            ...
            ifr_lib.exception.ItemNotFoundException: Item test.sub is a leaf item
        """
        return ifr_ini.dict_to_ini(self.__get_item(item))


class IfrYamlCodec(object):
    _yaml_prefix = "ifr_yaml"
    _eligible_classes = []

    @classmethod
    def types_to_yaml_tags(cls):
        """return a dict[Class, classname]"""
        return {c: c.__name__ for c in cls._eligible_classes}

    @classmethod
    def yaml_tags_to_types(cls):
        """return a dict[classname, Class]"""
        return {c.__name__: c for c in cls._eligible_classes}

    @classmethod
    def get_codec_yaml_prefix(cls):
        """return codec prefix"""
        return f"!{cls._yaml_prefix}/"

    @classmethod
    def get_known_types(cls) -> Iterable[Type[Any]]:
        """return the list of types that we know how to encode"""
        return cls.types_to_yaml_tags().keys()

    @classmethod
    def is_yaml_tag_supported(cls, yaml_tag_suffix: str) -> bool:
        """return True if the given yaml tag suffix is supported"""
        return yaml_tag_suffix in cls.yaml_tags_to_types().keys()

    @classmethod
    def from_yaml_dict(cls, yaml_tag_suffix: str, dct):
        """Create an object corresponding to the given tag, from the decoded dict"""
        typ = cls.yaml_tags_to_types()[yaml_tag_suffix]
        return typ(**dct)

    @classmethod
    def to_yaml_dict(cls, obj) -> Any:
        """Encode the given object and also return the tag that it should have"""
        return vars(obj)

    @classmethod
    def get_obj_yaml_prefix(cls, obj):
        return cls.types_to_yaml_tags()[type(obj)]

    @classmethod
    def get_yaml_tag(cls, obj):
        yaml_tag_suffix = cls.get_codec_yaml_prefix()
        if len(yaml_tag_suffix) == 0 or yaml_tag_suffix[-1] != '/':
            yaml_tag_suffix = yaml_tag_suffix + '/'

        return yaml_tag_suffix + cls.get_obj_yaml_prefix(obj)

    @classmethod
    def encode(cls, dumper: yaml.Dumper, obj: Any) -> Any:
        """Encode object instances

        Args:
            dumper: Dumper instance
            obj: any object to encode

        Returns:
            The encoded object instances
        """
        # case of List instance
        if isinstance(obj, list):
            return dumper.represent_sequence(cls.get_yaml_tag(obj), obj, flow_style=None)

        # other cases
        return dumper.represent_mapping(cls.get_yaml_tag(obj), cls.to_yaml_dict(obj), flow_style=None)

    @classmethod
    def decode(cls, loader, yaml_tag_suffix, node, **kwargs):
        if not cls.is_yaml_tag_supported(yaml_tag_suffix):
            return None

        # case of List object
        if issubclass(cls.yaml_tags_to_types()[yaml_tag_suffix], list):
            typ = cls.yaml_tags_to_types()[yaml_tag_suffix]
            return typ(*loader.construct_sequence(node, deep=True))

        # other cases
        loader.flatten_mapping(node)
        pairs = loader.construct_pairs(node, deep=True)  # 'deep' allows the construction to be complete (inner seq...)
        constructor_args = OrderedDict(pairs)
        return cls.from_yaml_dict(yaml_tag_suffix, constructor_args, **kwargs)

    @classmethod
    def register(cls, loaders=ALL_PYYAML_LOADERS, dumpers=ALL_PYYAML_DUMPERS):
        """Registers this codec with PyYaml, on the provided loaders and dumpers
        (default: all PyYaml loaders and dumpers).
         - The encoding part is registered for the object types listed in cls.get_known_types(), in order
         - The decoding part is registered for the yaml prefix in cls.get_yaml_prefix()

        Args:
            loaders: the PyYaml loaders to register this codec with. By default all pyyaml loaders are considered
                     (Loader, SafeLoader...)
            dumpers: the PyYaml dumpers to register this codec with. By default all pyyaml loaders are considered
                     (Dumper, SafeDumper...)

        """
        for loader in loaders:
            loader.add_multi_constructor(cls.get_codec_yaml_prefix(), cls.decode)

        for dumper in dumpers:
            for t in cls.get_known_types():
                dumper.add_multi_representer(t, cls.encode)


class YamlSerializable(object):
    """Represents an abstract yaml serializable object."""
    def to_yaml(self, **options):
        return yaml.safe_dump(self, **options)

    @classmethod
    def from_yaml(cls, value):
        return yaml.load(value, Loader=yaml.Loader)


def merge(content1: Optional[YamlType], content2: Optional[YamlType]) -> YamlConfig:
    config = YamlConfig(content1)
    if content2 is not None:
        config.merge(YamlConfig(content2))
    return config
