#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides utilities to manage Jinja2 templates (interpolation)
"""


import os
from datetime import datetime
from email.mime.text import MIMEText
from typing import Any, Mapping, Optional

import jinja2
from jinja2 import meta, sandbox

from ifr_lib import ifr_datetimes
from ifr_lib.ifr_exception import ItemNotFoundException


# --------------------------------------------------------------
# Jinja2 Environment configuration
# --------------------------------------------------------------
@jinja2.runtime.implements_to_string
class DollarStyleUndefined(jinja2.DebugUndefined):
    """An undefined that returns the debug info when printed.

    >>> foo = DollarStyleUndefined(name='foo')
    >>> str(foo)
    '${ foo }'
    >>> not foo
    True
    >>> foo + 42
    Traceback (most recent call last):
      ...
    jinja2.exceptions.UndefinedError: `foo` is undefined
    """

    __slots__ = ()

    def __str__(self):
        if self._undefined_hint is None:
            if self._undefined_obj is jinja2.environment.missing:
                return u"${ %s }" % self._undefined_name
            return "${ no such element: %s[%r] }" % (
                jinja2.runtime.object_type_repr(self._undefined_obj),
                self._undefined_name,
            )
        return u"${ undefined value printed: %s }" % self._undefined_hint


TOKEN_STYLE_BRACKET = {
    'block_start_string': jinja2.environment.BLOCK_START_STRING,
    'block_end_string': jinja2.environment.BLOCK_END_STRING,
    'variable_start_string': jinja2.environment.VARIABLE_START_STRING,
    'variable_end_string': jinja2.environment.VARIABLE_END_STRING,
    'comment_start_string': jinja2.environment.COMMENT_START_STRING,
    'comment_end_string': jinja2.environment.COMMENT_END_STRING
}

TOKEN_STYLE_DOLLAR = {
    'block_start_string': '%{',
    'block_end_string': '}',
    'variable_start_string': '${',
    'variable_end_string': '}',
    'comment_start_string': '#{',
    'comment_end_string': "}",
    'undefined': DollarStyleUndefined
}


# --------------------------------------------------------------
# Jinja2 Templates
# --------------------------------------------------------------
class BaseTemplateRenderer(object):
    ENABLED_EXTENSIONS = (
        'jinja2.ext.i18n',
        'jinja2.ext.do',
        'jinja2.ext.loopcontrols',
    )

    def __init__(self,
                 raise_if_undefined: bool = False,
                 hide_undefined_tokens: bool = False,
                 j2_env_params: dict = None,
                 loader: jinja2.BaseLoader = None,
                 ):

        self.raise_if_undefined = raise_if_undefined

        # Custom env params
        if j2_env_params is None:
            j2_env_params = dict()

        if hide_undefined_tokens:
            j2_env_params['undefined'] = jinja2.Undefined

        if loader is not None:
            j2_env_params['loader'] = loader

        j2_env_params.setdefault('keep_trailing_newline', True)
        j2_env_params.setdefault('undefined', jinja2.DebugUndefined)
        j2_env_params.setdefault('extensions', self.ENABLED_EXTENSIONS)

        # Environment
        self._env = sandbox.SandboxedEnvironment(**j2_env_params)

        # Register functions and filters
        def _env(varname: str, default: str = None):
            if default is not None:
                # With the default, there's never an error
                return os.getenv(varname, default)
            else:
                # Raise KeyError when not provided
                return os.environ[varname]

        def _datetime(date: datetime, fmt: str = None):
            if fmt:
                return date.strftime(fmt)
            else:
                return date.strftime('%Y%m%d')

        def _parse_date(date: str, fmt: str = None):
            return ifr_datetimes.parse_date(date, fmt)

        self.register_functions(env=_env, parse_date=_parse_date)
        self.register_filters(datetime=_datetime, parse_date=_parse_date)

    def register_filters(self, **filters):
        self._env.filters.update(filters)

    def register_functions(self, **functions):
        self._env.globals.update(**functions)

    def interpolate(self, content: Any, replacements: Mapping = None) -> Any:
        if content is None:
            return None

        if replacements is None:
            replacements = dict()

        if isinstance(content, jinja2.Template):
            try:
                return self.render(content, replacements)
            except jinja2.exceptions.TemplateError:
                return content

        if isinstance(content, str):
            try:
                return self.render(self._env.from_string(content), replacements)
            except jinja2.exceptions.TemplateError:
                return content

        if isinstance(content, list):
            res = list()
            for elt in content:
                try:
                    res.append(self.interpolate(elt, replacements))
                except (KeyError, TypeError):
                    res.append(elt)
            return res

        if isinstance(content, dict):
            res = {}
            for k, v in content.items():
                try:
                    res[k] = self.interpolate(v, replacements)
                except (KeyError, TypeError, jinja2.exceptions.UndefinedError):
                    res[k] = v
            return res

        return content

    def from_string(self, content: str) -> jinja2.Template:
        return self._env.from_string(content)

    def get_template(self, path: str):
        return self._env.get_template(path)

    def render(self, template: jinja2.Template, replacements: Optional[dict] = None) -> str:
        """Renders a template using the passed replacements

        Args:
            template (jinja2.Template): The jinja2 template
            replacements (dict): the replacements dictionary

        Returns:
            The rendered string
        """
        if replacements is None:
            replacements = dict()

        report = template.render(**replacements)

        if self.raise_if_undefined is True:
            undefined_variables = self.find_undeclared_variables(report, replacements)
            if undefined_variables:
                raise ItemNotFoundException(f"Some tokens are undefined : {undefined_variables!r}")

        return report

    def parse(self, content: Any) -> jinja2.Template:
        return self._env.parse(content)

    def find_undeclared_variables(self, content: Any, replacements: Mapping = None) -> list:
        """Searches the passed content for undeclared variables, using the replacements

        Args:
            content (Any): the inout content
            replacements (Mapping): the replacements mapping

        Returns:
            The sorted list of undefined variables found in content
        """
        if replacements is None:
            replacements = dict()

        leaf_keys = replacements.keys()
        return sorted(meta.find_undeclared_variables(self.parse(content)) - leaf_keys)


class MimeTextTemplateRenderer(BaseTemplateRenderer):

    def to_mimetext(self,
                    template: jinja2.Template,
                    replacements: dict,
                    output: Optional[str] = 'plain',
                    charset: Optional[str] = 'utf-8') -> MIMEText:
        """Generates a MIME text object.

        Warns in case of unexpected and/or missing variables.

        Args:
            template: jinja2 template
            replacements (dict[str, obj]): The replacements dictionary
            output (str): The output type (ex : plain, html...). Defaults to `plain`
            charset (str): The character set to use. Defaults to `utf-8`

        Returns:
            A MIME text
        """
        return MIMEText(self.interpolate(content=template, replacements=replacements), output, _charset=charset)

    def to_plain_mimetext(self, template: jinja2.Template, replacements: dict, charset: Optional[str] = 'utf-8')\
            -> MIMEText:
        """Generates a plain MIME text.

        Args:
            template: jinja2 template
            replacements (dict[str, obj]): The replacements dictionary
            charset (str): The character set to use. Defaults to `utf-8`

        Returns:
            A plain MIME text
        """
        return self.to_mimetext(template, replacements, 'plain', charset)

    def to_html_mimetext(self, template: jinja2.Template, replacements: dict, charset: Optional[str] = 'utf-8')\
            -> MIMEText:
        """Generates an HTML MIME text.

        Args:
            template: jinja2 template
            replacements (dict[str, obj]): The replacements dictionary
            charset (str): The character set to use. Defaults to `utf-8`

        Returns:
            An HTML MIME text
        """
        return self.to_mimetext(template, replacements, 'html', charset)
