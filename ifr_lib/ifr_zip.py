#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    ifr_lib.ifr_zip
    ~~~~~~~~~~~~~~~~

    Provides some utilities to manage zip files.
"""

import os
import shutil
import tempfile
import zipfile


def remove_from_zip(zip_filename: str, *filenames):
    temp_dir = tempfile.mkdtemp()
    try:
        temp_name = os.path.join(temp_dir, 'new.zip')
        with zipfile.ZipFile(zip_filename, 'r') as zip_read:
            with zipfile.ZipFile(temp_name, 'w') as zip_write:
                for item in zip_read.infolist():
                    if item.filename not in filenames:
                        data = zip_read.read(item.filename)
                        zip_write.writestr(item, data)
        shutil.move(temp_name, zip_filename)
    finally:
        shutil.rmtree(temp_dir)


def add_to_zip(zip_filename: str, *filenames):
    with zipfile.ZipFile(zip_filename, 'a') as zip_write:
        for file_to_add in filenames:
            zip_write.write(file_to_add, os.path.basename(file_to_add))


def add_or_update_to_zip(zip_filename: str, *filenames):
    if os.path.isfile(zip_filename):
        remove_from_zip(zip_filename, *filenames)
    add_to_zip(zip_filename, *filenames)
