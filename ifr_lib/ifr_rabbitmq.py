#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides some utilities to manage rabbitmq.
"""

import logging
import os
import shutil
from ssl import create_default_context
from typing import Any, Optional

import pika
import xmltodict

from ifr_lib import ifr_json, ifr_path
from ifr_lib.ifr_exception import GenericException
from ifr_lib.ifr_exec import retry_block, RetryConfig


class FileReader(object):
    def __init__(self, folder_path):
        self.folder_path = folder_path

    @property
    def content_type(self):
        return None

    @property
    def pattern(self):
        return "*"

    @property
    def files(self) -> list:
        return ifr_path.PathFinder(
            folder=self.folder_path,
            filter=ifr_path.AndFilter(
                ifr_path.BasenameFnmatchFilter(self.pattern),
                ifr_path.TimeFilter(ifr_path.TimeFilter.FILE_MTIME, -1, ifr_path.TimeFilter.DELTA_UNIT_MINUTES)
            )
        ).get_paths()

    def read(self, file: str) -> str:
        try:
            with open(file, 'r') as file:
                return file.read()
        except Exception:
            raise GenericException(f"Error reading (file: {file})")

    def __iter__(self):
        return self.files.__iter__()


class XmlFileReader(FileReader):
    @property
    def content_type(self):
        return "application/xml"

    @property
    def pattern(self):
        return "*.xml"

    def read(self, file: str) -> str:
        try:
            with open(file) as f:
                data = f.read()
            return xmltodict.parse(data)
        except Exception:
            raise GenericException(f"Error during load/parse in xml (file: {file})")


class JsonFileReader(FileReader):
    @property
    def content_type(self):
        return "application/json"

    @property
    def pattern(self):
        return "*.json"


class RabbitMQConfig(object):

    def __init__(self,
                 host="localhost",
                 port: Optional[int] = pika.connection.Parameters.DEFAULT_PORT,
                 ssl: Optional[bool] = pika.connection.Parameters.DEFAULT_SSL,
                 user: Optional[str] = None,
                 password: Optional[str] = None,
                 virtual_host: Optional[str] = pika.connection.Parameters.DEFAULT_VIRTUAL_HOST,
                 queue_name: Optional[str] = "cs-emm",
                 routing_key: Optional[str] = None,
                 ):

        self.ssl_context = create_default_context() if ssl is True else None

        self.connectionParams = {
            "host": host,
            "credentials":
                pika.credentials.PlainCredentials(user, password)
                if password is not None
                else pika.connection.Parameters.DEFAULT_CREDENTIALS,
            "ssl_options":
                pika.SSLOptions(self.ssl_context)
                if self.ssl_context is not None
                else None,
            "port": port,
            "virtual_host": virtual_host,
            "blocked_connection_timeout": 60,
            "heartbeat": 600,
        }
        self.routing_key = routing_key
        self.queue_name = queue_name


class RabbitMQService(object):

    def __init__(self, config: RabbitMQConfig):

        import logging
        self.__log = logging.getLogger(__name__)
        self.__connectionParams = config.connectionParams
        self.__connection = None
        self.__channel = None
        self.__routing_key = config.routing_key
        self.__queue_name = config.queue_name

    def connect_rabbitmq(self):
        if self.__connection is not None and self.__connection.is_open:
            self.__log.info("Connection already established")
        else:
            try:
                self.__connection = pika.BlockingConnection(pika.ConnectionParameters(**self.__connectionParams))
                self.__channel = self.__connection.channel()
            except Exception:
                self.__log.exception("Error during the connection")
                raise

    def disconnect_rabbitmq(self):
        if self.__connection is not None and self.__connection.is_open and not self.__connection.is_closed:
            self.__connection.close()

    def publish_message(self, message: Any, content_type: Optional[str] = "application/json"):
        try:
            self.__channel.basic_publish(
                exchange="",
                routing_key=self.__routing_key if self.__routing_key is not None else self.__queue_name,
                properties=pika.spec.BasicProperties(content_type=content_type),
                body=ifr_json.json_dumps(message),
            )
        except Exception as exception:
            raise GenericException(f"Error during message publication : {exception}")


def read_and_publish(spool_path: Optional[str] = ".",
                     error_path: Optional[str] = "./err",
                     file_name_pattern: Optional[str] = "*",
                     config: Optional[RabbitMQConfig] = None,
                     retry_config: Optional[RetryConfig] = None,
                     logger: Optional[logging.Logger] = None):

    def log(level: int, msg: Any, exc_info: Optional[bool] = False):
        if logger is not None:
            logger.log(level, msg, exc_info=exc_info)

    rmq = RabbitMQService(config)

    if file_name_pattern == "*.json":
        file_reader = JsonFileReader(spool_path)
    elif file_name_pattern == "*.xml":
        file_reader = XmlFileReader(spool_path)
    else:
        file_reader = FileReader(spool_path)

    while True:
        try:
            retry_block(
                rmq.connect_rabbitmq,
                exceptions=pika.exceptions.AMQPConnectionError,
                retry_config=retry_config,
                logger=logger
            )
            log(logging.DEBUG, "Connection to RabbitMQ => Ok")

            retry_config = RetryConfig(nb_tries=3)

            for f in file_reader:
                try:
                    retry_block(
                        rmq.publish_message,
                        error_function=rmq.connect_rabbitmq,
                        exceptions=pika.exceptions.AMQPConnectionError,
                        retry_config=retry_config,  # retry_config
                        logger=logger,
                        message=file_reader.read(f),
                        content_type=file_reader.content_type,
                    )
                    log(logging.INFO, f"file published: {f}")
                except Exception:
                    log(logging.WARNING, f"file could not be published: {f}", exc_info=True)
                    shutil.move(f, os.path.join(error_path, os.path.basename(f)))
                    log(logging.INFO, f"file moved to: {error_path}")
                    continue

                try:
                    os.remove(f)
                    log(logging.DEBUG, f"file deleted: {f}")
                except OSError as error:
                    raise GenericException(f"Error while removing file {f} : {error}")

            rmq.disconnect_rabbitmq()
        except KeyboardInterrupt:
            rmq.disconnect_rabbitmq()
            break
        # Do not recover if connection was closed by broker
        except pika.exceptions.ConnectionClosedByBroker:
            break
        # Do not recover on channel errors
        except pika.exceptions.AMQPChannelError:
            break
        # Recover on all other connection errors
        except pika.exceptions.AMQPConnectionError:
            continue
