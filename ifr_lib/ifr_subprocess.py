#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides some utilities to manage subprocesses.
"""


import shlex
from subprocess import PIPE, Popen
from typing import Tuple


def run(cmd: str) -> Tuple[int, str, str]:
    """Runs the given command locally and returns the output, err and exit_code.

    Returns:
        exit code, stdout message, stderr message

    Examples:
        >>> run("ls -l | grep example")
    """

    if "|" in cmd:
        cmd_parts = cmd.split('|')
    else:
        cmd_parts = list()
        cmd_parts.append(cmd)

    i = 0
    processes = dict()
    for cmd_part in cmd_parts:
        cmd_part = cmd_part.strip()
        if i == 0:
            processes[i] = Popen(shlex.split(cmd_part), stdin=None, stdout=PIPE, stderr=PIPE)
        else:
            processes[i] = Popen(shlex.split(cmd_part), stdin=processes[i - 1].stdout, stdout=PIPE, stderr=PIPE)
        i = i + 1
    output, err = processes[i - 1].communicate()
    exit_code = processes[0].wait()

    return exit_code, output.decode('utf-8'), err.decode('utf-8')
