#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides utilities to manage logging.
"""

import logging
import logging.config as logging_conf
import os
import re
import sys
import time
from logging.handlers import TimedRotatingFileHandler
from typing import Any, Optional, Union

from colorlog import LevelFormatter
from colorlog.escape_codes import escape_codes, parse_colors

from ifr_lib import ifr_collections, ifr_numbers, ifr_str
from ifr_lib.ifr_datetimes import str_to_datetime
from ifr_lib.ifr_exception import ConfigurationException
from ifr_lib.ifr_files import gzip_file
from ifr_lib.ifr_yaml import YamlConfig


def level_code(level: Union[str, int]) -> int:
    """Retrieves the log level from either str or numerical value

    Args:
        level: The level name or code

    Returns:
        The logging level code
    """

    if isinstance(level, str):
        name_to_level = {
            'CRITICAL': logging.CRITICAL,
            'FATAL': logging.FATAL,
            'ERROR': logging.ERROR,
            'WARN': logging.WARNING,
            'WARNING': logging.WARNING,
            'INFO': logging.INFO,
            'DEBUG': logging.DEBUG,
            'NOTSET': logging.NOTSET
        }

        if level not in name_to_level:
            return logging.NOTSET
        return name_to_level[level]

    if isinstance(level, int):
        level_int = [0, 10, 20, 30, 40, 50]
        if level not in level_int:
            return logging.NOTSET
        return level

    return logging.NOTSET


def level_name(level: Union[str, int]) -> str:
    """Retrieves log level name from str or numerical value.

    This function exists because `logging.getLevelName()` returns a numerical value in some cases.

    Args:
        level (str or int): log level

    Returns:
        log level name

    Examples:
        >>> print(level_name("WARN"))
        WARNING
        >>> print(level_name("WARNING"))
        WARNING
        >>> print(level_name(logging.WARNING))
        WARNING
        >>> print(level_name(30))
        WARNING
    """

    if isinstance(level, str):
        level = level.upper()

    name = logging.getLevelName(level)
    if ifr_numbers.is_number(name):
        return logging.getLevelName(name)
    return name


class IfrFileHandler(TimedRotatingFileHandler):
    """Handler for logging to a file, rotating the log file at certain timed
    intervals and compress the logs if requested.
    When parameter fixed to MIDNIGHT by default and backupCount to 10.
    Inherits from TimedRotatingFileHandler. One additional argument : compress, True by default.

    Example of configuration :
      info_file_handler:
        '()': 'ifr_lib.ifr_logging.IfrFileHandler'
        level: DEBUG
        formatter: colored
        filename: ${LOG_DIR}/info.log
        encoding: utf8
        when: D
        backupCount: 20
        compress: True
        atTime: '15:30:45'
    """
    COMPRESSED_FILE_EXTENSION = 'gz'

    def __init__(self,
                 filename: str,
                 when: Optional[str] = 'MIDNIGHT',
                 interval: Optional[int] = 1,
                 backupCount: Optional[int] = 10,
                 encoding: Optional[str] = None,
                 delay: Optional[bool] = False,
                 utc: Optional[bool] = False,
                 atTime: Optional[str] = None,
                 compress: Optional[bool] = True):

        super(IfrFileHandler, self).__init__(filename=filename, when=when, interval=interval, backupCount=backupCount,
                                             encoding=encoding, delay=delay, utc=utc)

        self.compress = compress

        # in python < 3.4, atTime does not exist in TimedRotatingFileHandler
        if atTime:
            atTime = str_to_datetime(atTime, '%H:%M:%S').time()
        self.atTime = atTime

        # retrieve compression option and recompile extMatch with zip extension if needed (used by getFilesToDelete)
        # if compress is True, modify extension pattern (used to delete old files)
        # see TimedRotatingFileHandler.__init__()
        if self.compress:
            pattern = self.extMatch.pattern
            self.extMatch = re.compile(pattern[:-1] + r"\." + self.COMPRESSED_FILE_EXTENSION + pattern[-1:])

    def doRollover(self):
        if self.stream:
            self.stream.close()
            self.stream = None
        # get the time that this sequence started at and make it a TimeTuple
        current_time = int(time.time())
        dst_now = time.localtime(current_time)[-1]
        t = self.rolloverAt - self.interval
        if self.utc:
            time_tuple = time.gmtime(t)
        else:
            time_tuple = time.localtime(t)
            dst_then = time_tuple[-1]
            if dst_now != dst_then:
                if dst_now:
                    addend = 3600
                else:
                    addend = -3600
                time_tuple = time.localtime(t + addend)
        dfn = self.baseFilename + "." + time.strftime(self.suffix, time_tuple)
        if os.path.exists(dfn):
            os.remove(dfn)
        # Issue 18940: A file may not have been created if delay is True.
        if os.path.exists(self.baseFilename):
            os.rename(self.baseFilename, dfn)
            if self.compress:
                gzip_file(dfn, delete_source=True)
        if self.backupCount > 0:
            for s in self.getFilesToDelete():
                os.remove(s)
        if not self.delay:
            self.stream = self._open()
        new_rollover_at = self.computeRollover(current_time)
        while new_rollover_at <= current_time:
            new_rollover_at = new_rollover_at + self.interval
        # If DST changes and midnight or weekly rollover, adjust for this.
        if (self.when == 'MIDNIGHT' or self.when.startswith('W')) and not self.utc:
            dst_at_rollover = time.localtime(new_rollover_at)[-1]
            if dst_now != dst_at_rollover:
                if not dst_now:  # DST kicks in before next rollover, so we need to deduct an hour
                    addend = -3600
                else:  # DST bows out before next rollover, so we need to add an hour
                    addend = 3600
                new_rollover_at += addend
        self.rolloverAt = new_rollover_at


class IfrColorFormatter(LevelFormatter):

    __default_log_colors = {
        'DEBUG': 'cyan',
        'INFO': 'green',
        'WARNING': 'yellow',
        'ERROR': 'red',
        'CRITICAL': 'bold_red'
    }
    _default_secondary_colors = {
        'message': {
            'WARNING': 'yellow',
            'ERROR': 'red',
            'CRITICAL': 'bold_red'
        }
    }

    _default_msg_format = ("%(purple)s%(asctime)s%(reset)s | "
                           "%(log_color)s%(levelname).3s%(reset)s | "
                           "%(message_log_color)s%(message)s%(reset)s")

    _default_date_format = '%d/%m/%y %H:%M:%S'
    _default_multiline_color = 'bold_white'

    def __init__(self,
                 fmt: Optional[str] = None,
                 datefmt: Optional[str] = None,
                 style: Optional[str] = '%',
                 log_colors: Optional[dict] = None,
                 reset: Optional[bool] = True,
                 secondary_log_colors: Optional[dict] = None,
                 multilines_color: Optional[str] = None):
        if fmt is None:
            fmt = self._default_msg_format

        if datefmt is None:
            datefmt = self._default_date_format

        if log_colors is None:
            _log_colors = self.__default_log_colors
        else:
            _log_colors = ifr_collections.merge_dict(self.__default_log_colors, log_colors)

        if secondary_log_colors is None:
            _secondary_colors = self._default_secondary_colors
        else:
            _secondary_colors = ifr_collections.merge_dict(self._default_secondary_colors, secondary_log_colors)

        super(IfrColorFormatter, self).__init__(fmt, datefmt, style, _log_colors, reset, _secondary_colors)

        if multilines_color is None:
            self.multilines_color = self._default_multiline_color
        else:
            self.multilines_color = multilines_color

    def format(self, record: logging.LogRecord) -> str:
        message = super(IfrColorFormatter, self).format(record)

        _msg_fragments = message.split('\n')
        if len(_msg_fragments) <= 1:
            return message

        multiline_msg = list()
        multiline_msg.append(_msg_fragments.pop(0))
        multiline_msg_color = parse_colors(self.multilines_color) + "\n".join(_msg_fragments)
        if self.reset and not multiline_msg_color.endswith(escape_codes['reset']):
            multiline_msg_color += escape_codes['reset']
        multiline_msg.append(multiline_msg_color)
        return "\n".join(multiline_msg)


class IfrNoColorFormatter(logging.Formatter):

    _default_msg_format = '%(asctime)s | %(levelname).3s | %(message)s'
    _default_date_format = '%d/%m/%y %H:%M:%S'

    def __init__(self, fmt: Optional[str] = None, datefmt: Optional[str] = None, style: Optional[str] = '%'):
        if fmt is None:
            fmt = self._default_msg_format

        if datefmt is None:
            datefmt = self._default_date_format

        if sys.version_info > (3, 2):
            super(IfrNoColorFormatter, self).__init__(fmt, datefmt, style)
        elif sys.version_info > (2, 7):
            super(IfrNoColorFormatter, self).__init__(fmt, datefmt)
        else:
            logging.Formatter.__init__(self, fmt, datefmt)

    def format(self, record: logging.LogRecord) -> str:
        message = super(IfrNoColorFormatter, self).format(record)
        return ifr_str.strip_color(message)


class ColorLogger(logging.Logger):
    """Adds features to standard logger
    - add 'color' option to log functions, to format_message message
    - add 'repeat' option to log functions, to format_message repeat a term to a defined message length
    - print dictionary

    Examples:
        # initialize Logger
        >>> logging.setLoggerClass(ColorLogger)
        >>> log = logging.getLogger(__name__)
        >>> log.message_length = 150

        # standard message
        >>> log.info('my message')

        # standard message with color
        >>> log.info('my message', color='thin_blue')

        # repeat message with log.message_length
        >>> log.info('=', color='thin_blue', repeat=True)

        # log each items of dictionary
        >>> log.log_dict(
        >>>  logging.INFO,
        >>>  {'key1':{'key2': 'value'}}, title='Configuration :', header='-', footer='-', color='thin_white'
        >>> )
    """

    def __init__(self, name: str, level: Optional[int] = logging.NOTSET):
        self.message_length = None
        super(ColorLogger, self).__init__(name, level)

    def __repeat_msg(self, msg: str, repeat: Optional[bool] = False) -> Union[str, None]:
        """Repeats a term to message length if repeat is True

        Args:
            msg (str): message to log
            repeat (bool): repeat the message if True. Defaults to False

        Returns:
            str: formatted message
        """
        if repeat and self.message_length is not None and msg is not None:
            return msg * self.message_length
        return msg

    def format_message(self, msg: str, color: Optional[str] = None, repeat: Optional[bool] = False) -> str:
        """Formats a message

        Args:
            msg (str): message to log
            color (str): color name
            repeat (bool): repeat the message if True. Defaults to False

        Returns:
            formatted message
        """
        return ifr_str.colorize(self.__repeat_msg(msg, repeat), color, self.message_length)

    def _log(self,
             level: int,
             msg: str,
             args: Any,
             exc_info: Optional[Any] = None,
             extra: Optional[Any] = None,
             stack_info: Optional[bool] = False,
             color: Optional[str] = None,
             repeat: Optional[bool] = False):
        """Provides a formatted message to logging.Logger._log()

        Args:
            msg (str): message to log
            color (str): color name
            repeat (bool): repeat the message if True. Defaults to False

        Returns:
            str: formatted message
        """
        if sys.version_info < (3, 2):
            super(ColorLogger, self)._log(
                level,
                self.format_message(msg, color, repeat),
                args,
                exc_info=exc_info,
                extra=extra
            )
        else:
            super(ColorLogger, self)._log(
                level,
                self.format_message(msg, color, repeat),
                args,
                exc_info=exc_info,
                extra=extra,
                stack_info=stack_info
            )

    def log_dict(self,
                 level: int,
                 dictionary: dict,
                 exc_info: Optional[Any] = None,
                 extra: Optional[Any] = None,
                 stack_info: Optional[bool] = False,
                 title: Optional[str] = None,
                 header: Optional[str] = None,
                 footer: Optional[str] = None,
                 title_footer: Optional[str] = None,
                 item_sorted: Optional[bool] = False,
                 color: Optional[str] = None,
                 *args):

        """Logs each items of a dictionary with possible decorators.

        Args:
            level (int): logging level
            dictionary (dict): the dictionary to log
            exc_info (Any): the execution info
            extra (Any): extra info
            stack_info (bool): stack info
            title (str): a title to display before the list of elements
            header (str): a separator before the title.
            footer (str): a separator before the title.
            title_footer (str): a separator after the title.
            item_sorted (bool): if true, sort the items by the key, otherwise keep the default sort
            color (str): color options (ex: `blue`, `bold_blue,bg_white`, ...)
        """
        if not self.isEnabledFor(level):
            return

        if dictionary is None or (isinstance(dictionary, dict) and len(dictionary) == 0):
            return

        # retrieve each lines formatted
        lines = ifr_collections.prettify_dict(
            dictionary=dictionary,
            title=title,
            header=self.__repeat_msg(header, True),
            footer=self.__repeat_msg(footer, True),
            title_footer=self.__repeat_msg(title_footer, True),
            item_sorted=item_sorted,
            color_options=color
        )

        # log each lines
        for line in lines:
            # adjust message length if we use ansi colors
            if color is not None and self.message_length is not None and len(line) <= self.message_length:
                line = line.ljust(self.message_length + (len(line) - ifr_str.ansilen(line)))

            # log line
            self._log(level, line, args=args, exc_info=exc_info, extra=extra, stack_info=stack_info)


def setup_logging(config: Optional[Union[dict, str]] = None, root_level: Optional[Union[str, int]] = logging.INFO):
    """Configures python logging framework.

    Use default logging configuration and override it if config is set.

    Root logging level is set with root_level

    Args:
        config (dict | str | stream | binary, optional): logging configuration dictionary or YAML content
        root_level (str | int): logging level. Defaults to INFO

    Raises:
        FileNotFoundException: logging configuration file is not found
        SyntaxException: logging configuration is not a valid yaml file
        ConfigurationException: logging configuration content is not a valid logging configuration

    Examples:
        >>> print(setup_logging())
        # will configure logging with the default configuration
        # root level will be set to INFO
        >>> print(setup_logging(config={'root': {'handlers': ['stdout', 'stderr']}}, root_level='DEBUG')
        # will override default configuration with content of config
        # root level will be set to DEBUG
    """

    default_logging_config = """\
        version: 1
        disable_existing_loggers: False
        root:
          handlers: [console]
        handlers:
          console:
            class: logging.StreamHandler
            level: DEBUG
            formatter: console_fmt
            stream: ext://sys.stdout
        formatters:
          console_fmt:
            '()': "ifr_lib.ifr_logging.IfrColorFormatter"
            datefmt: '%d/%m/%Y %H:%M:%S'
        """

    logging_config = YamlConfig(content=default_logging_config)

    # merge with config
    if config is not None:
        logging_config.merge(YamlConfig(content=config))

    # configure logging
    try:
        logging_conf.dictConfig(logging_config.content)
    except ValueError as err:
        raise ConfigurationException(f"Error in logging configuration : {str(err)}")

    # define root logger level
    if root_level is not None:
        logging.getLogger().setLevel(level_name(root_level))


def basic_config(root_level=logging.INFO):
    setup_logging(
        config="""
        version: 1
        disable_existing_loggers: False
        root:
          handlers: [console]
        handlers:
          console:
            class: logging.StreamHandler
            level: DEBUG
            formatter: console_fmt
            stream: ext://sys.stdout
        formatters:
          console_fmt:
            '()': "ifr_lib.ifr_logging.IfrColorFormatter"
            datefmt: '%d/%m/%Y %H:%M:%S'
        """,
        root_level=root_level
    )
