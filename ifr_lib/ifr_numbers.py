#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides some utilities to manage numbers.
"""


from typing import Any, Union

from ifr_lib.ifr_exception import ConversionException


def is_number(value: Any) -> bool:
    """Checks if the input value is a number.

    Args:
        value (Any): a value

    Returns:
        True if value is a number

    Examples:
        >>> print(is_number('026'))
        True
        >>> print(is_number('abc'))
        False
        >>> print(is_number(26))
        True
        >>> print(is_number('26.05'))
        True
    """
    try:
        float(value)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(value)
        return True
    except (TypeError, ValueError):
        pass

    return False


def to_int(value: Union[str, int, float]) -> int:
    """Converts a value to int.

    Args:
        value (Union[str, int, float]): a number as string, int or float

    Returns:
        A number as int

    Raises:
        ConversionException: if value is not a number

    Examples:
        >>> print(to_int("026"))
        26
        >>> print(to_int("026.05"))
        26
    """
    try:
        return int(to_float(value))
    except ValueError as err:
        raise ConversionException(f"Cannot convert {value} to int. The following error occurred : {err}")


def to_float(value: Union[str, int, float]) -> float:
    """Converts a value to float.

    Args:
        value (Union[str, int, float]): a number as string, int or float

    Returns:
        A number as float

    Raises:
        ConversionException: if value is not a number

    Examples:
        >>> print(to_float("026"))
        26.0
        >>> print(to_float("026.05"))
        26.05
    """
    try:
        return float(value)
    except ValueError as err:
        raise ConversionException(f"Cannot convert {value} to float. The following error occurred : {err}")
