#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Rules management """
import logging
import os
import re
import sys
from typing import Any

from ifr_lib.ifr_yaml import YamlConfig

log = logging.getLogger(__name__)


# pylint: disable=too-few-public-methods
class RulesEngine:
    """ An object which process rules extraction with a given rule set (.yaml) """

    def __init__(self, configuration):
        # open the rule set file under yaml format
        config = YamlConfig(file=os.path.abspath(configuration))
        rule_set = config['rules']

        # validating ruleset : default rule
        if 'default' not in rule_set:
            raise KeyError('no default rules exists in the given rule set')
        self.rule_set = rule_set

    def extraction(self, engine):
        """ Process data extraction with from an Engine """
        # We process all rules iteratively till one rule match

        for key, rule in self.rule_set.items():
            log.debug(f"Try rule {key}")
            if key != 'default' and engine.validate(rule['rule']):
                # process data extraction
                log.debug(f"Rule match : {key}")
                return engine.extract(rule)
        # no rules matching ? process default rule
        return engine.extract(self.rule_set['default'])


class Engine:
    """ Processor for the RulesEngine """

    def __init__(self, **kwargs):
        # all dict elements are stored as tokens
        self.tokens = dict(kwargs)
        # tokenize all path variables by filtering the values starting by '/' or 'http(s)://'
        # pylint: disable=unused-variable
        for key, value in self.tokens.copy().items():
            if value is not None:
                self.extract_parts(key, value)

    def extract_parts(self, key, value):
        groups = re.match(r'^(?:/|https?://)(.+)$', value)
        if groups:
            # each path part are elements between '/'
            parts = groups.group(1).split('/')
            for index, token in enumerate(parts):
                # token added : KEY_index
                self.tokens[key + '_' + str(index)] = token
            # add last path reference
            self.tokens[key + '_last'] = parts[len(parts) - 1]

    def add_tokens(self, key: str, value: Any):
        """Adds a token to the tokens dictionary

        Args:
            key (str): The token key
            value (Any): The token value
        """
        self.tokens[key] = value
        self.extract_parts(key, value)

    def validate(self, rule: Any) -> bool:
        """Validation process by matching the given input (without $) and the given regex

        Args:
            rule (Any): The rule to be checked

        Returns:
            A boolean indicating whether the rule is validated
        """
        rules = rule if isinstance(rule, list) else [rule]
        agg_bool = True
        for _rule in rules:
            _input = _rule['input'][1:]
            if _input in self.tokens and re.match(_rule['regexp'], self.tokens[_input] or '') is not None:
                agg_bool = agg_bool & True
            else:
                return False
        return agg_bool

    def extract(self, rule):
        # recover additional fields considered as data to be fetched
        fields = filter(lambda x: x != 'comment' and x != 'rule', rule)
        result = dict()

        for field in fields:
            value = rule[field]

            # look for potential regex extraction
            if isinstance(value, dict) and 'input' in value and 'regexp' in value:
                log.debug(f"process field : {field}")
                regexp = value['regexp']
                log.debug(f"regexp : {regexp}")

                input_value = self.replace_token(value['input'])
                log.debug(f"input : {input_value}")

                if 'replace' in value:
                    replace_expression = value['replace']
                    replace_result = re.sub(re.compile(regexp), replace_expression, input_value)
                    if replace_result:
                        match = re.search(re.compile('({}[^&]+)'.format(replace_expression.replace('\\1', ''))), replace_result)
                        result[field] = match.group(1) if match is not None else ''
                    else:
                        result[field] = ''
                else:
                    match = re.search(re.compile(regexp), input_value)

                    # regex not found ? field is unknown
                    result[field] = match.group(1) if match is not None else ''
            else:
                result[field] = self.replace_token(value)

        return result

    def replace_token(self, phrase: str) -> str:
        """Replaces the $ variable by the stored value

        Args:
            phrase (str): The original phrase

        Returns:
            The phrase with the replaced variable
        """
        if phrase is not None and isinstance(phrase, str):
            for key in sorted(self.tokens, key=str.__len__, reverse=True):
                phrase = phrase.replace('$' + key, self.tokens[key] or '')
        return phrase


def process_rule(rule_file: str, **args):

    log.info('Load rules')
    metadata_engine = RulesEngine(rule_file)

    log.info('Define engine')
    engine = Engine(**args)

    log.info('Process engine')
    metadata = metadata_engine.extraction(engine)

    log.info(f"Result : {metadata}")
    return metadata


def check_rule():
    import argparse

    logging.basicConfig(level=logging.DEBUG)

    parser = argparse.ArgumentParser(description='Test rules')
    parser.add_argument('rule_file', help='Rules file path')
    parser.add_argument('dictionary', help='Field key=value', nargs="+")

    # check and retrieve arguments
    options = parser.parse_args()

    log.info(f"Rules file : {options.rule_file}")

    # build engine options
    engine = dict()
    for option in options.dictionary:
        matcher = re.match(r'^([^=]*)=(.*)$', option)
        if matcher is None:
            # invalid option
            log.warning(f"Invalid option '{option}'")
            continue
        engine[matcher.group(1)] = matcher.group(2)
    log.info(f"Engine : {engine}")

    # instantiate job
    process_rule(options.rule_file, **engine)
