#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides some utilities to manage json
"""

import datetime
import json
import re
from collections import OrderedDict
from pathlib import Path
from typing import Any

from ifr_lib import ifr_datetimes
from ifr_lib.ifr_exception import ConversionException


class JsonSerializable(object):
    """Represents an abstract json serializable object."""

    def to_json(self, **options) -> str:
        """Exports attributes to JSON object

        Returns:
            The object's json formatted representation
        """
        return json_dumps(self.__dict__, **options)

    @classmethod
    def from_json(cls, value):
        return cls(**json_loads(value))


def json_dumps(dictionary, **options) -> Any:
    only_public_members = options.pop("only_public_members", True)

    def json_encoder(o):
        if isinstance(o, datetime.datetime):
            return o.isoformat()
        if isinstance(o, datetime.timedelta):
            return o.seconds
        if isinstance(o, Path):
            return str(o)

        if only_public_members is True:
            return {k: v for k, v in o.__dict__.items() if not k.startswith('_')}

        return o.__dict__

    return json.dumps(
        dictionary,
        default=json_encoder,
        ensure_ascii=False,
        **options
    )


def json_loads(value, **options) -> dict:
    def json_decoder(pairs):
        d = OrderedDict()
        for k, v in pairs:
            d[k] = v
            if isinstance(v, str):
                try:
                    if re.match(r'\d{4}-\d{2}-\d{2}', v):
                        d[k] = ifr_datetimes.parse_date(v)
                except ConversionException:
                    continue
        return d

    return json.loads(value, object_pairs_hook=json_decoder, **options)


def json_load(fp, **options):
    return json_loads(fp.read(), **options)
