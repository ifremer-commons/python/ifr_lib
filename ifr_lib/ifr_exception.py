#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides generic exception classes.

    Examples :
        >>> raise GenericException('my message')
"""


class GenericException(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)


class FileNotFoundException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class ItemNotFoundException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class BadTypeException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class BadValueException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class MissingParameterException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class BadParameterException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class SyntaxException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class PatternException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class ConversionException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)


class ConfigurationException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)
