#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides utilities for to manage ini files
"""

from collections import OrderedDict
from configparser import ConfigParser
from typing import Optional, Union


def read_ini_config(config_file: str, section=None, interpolation=None) -> dict:
    """Parses an ini config file into a dictionary

    Args:
        config_file (str): the ini config file path
        section (str): to specify a section
        interpolation (Any): to specify an interpolation

    Returns:
        The parsing result dictionary
    """
    config = ConfigParser(strict=False, interpolation=interpolation)
    config.read(config_file)
    return dict(config.items(section=section))


def _dict_to_sections(dictionary: dict, current_section: Optional[str] = None, sections: Optional[dict] = None) \
        -> OrderedDict:
    if sections is None:
        sections = OrderedDict()

    for key, value in dictionary.items():
        if isinstance(value, dict):
            new_section = (current_section + "." if current_section is not None else '') + key
            sections[new_section] = OrderedDict()
            _dict_to_sections(value, new_section, sections)
        else:
            if value is None:
                value = ''
            if current_section is not None:
                sections[current_section][key] = value
            else:
                if '' not in sections:
                    sections[''] = OrderedDict()
                sections[''][key] = value

    return sections


def _sections_to_ini(sections: dict) -> str:
    result = list()
    for section in sections.keys():
        if sections[section] is None or len(sections[section]) == 0:
            continue
        if section != '':
            result.append(f'[{section}]')
        for key, value in sections[section].items():
            result.append(f'{key} = {value}')
        result.append('')
    # remove last ne line
    result.pop()
    # return list as string
    return "\n".join(result)


def dict_to_ini(dictionary: dict) -> Union[str, None]:
    """Transforms a dictionary to INI format

    Args:
        dictionary (dict): dictionary to transform

    Returns:
        The dictionary to INI format

    Example:
        >>> print(dict_to_ini({"key1": {"key3": "value3", "key2": "value2"}}))
        [key1]
        key3 = value3
        key2 = value2
        >>> print(dict_to_ini({"key1": {"key3": "value3", "key2": {"key4": "value"}, "key5": "value"}}))
        [key1]
        key3 = value3
        key5 = value

        [key1.key2]
        key4 = value
    """

    sections = _dict_to_sections(dictionary)
    if sections is None or len(sections) == 0:
        return None
    return _sections_to_ini(sections)


def ini_to_dict(ini_content: str) -> dict:
    """Deserialize an INI content to dictionary
    Only for python versions 3.2 and above.

    Args:
        ini_content (str): INI content

    Returns:
        The dictionary

    Example:
        >>> print(ini_to_dict(dict_to_ini({"key1": {"key3": "value3", "key2": "value2"}})))
        {"key1": {"key3": "value3", "key2": "value2"}}
        >>> print(dict_to_ini({"key1": {"key3": "value3", "key2": {"key4": "value"}, "key5": "value"}}))
        {"key1": {"key3": "value3", "key5": "value", "key2": {"key4": "value"}}}
    """
    from configparser import ConfigParser

    from ifr_lib.ifr_yaml import YamlConfig
    parser = ConfigParser(interpolation=None)

    parser.read_string(ini_content)

    result = YamlConfig()
    for section in parser.sections():
        result[section] = OrderedDict()
        for key, val in parser.items(section):
            result[section][key] = val
    return result.as_dict()
