#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Provides some utilities to retrieves os information
"""
import os
import sys
from collections import OrderedDict
from getpass import getuser
from multiprocessing import cpu_count
from platform import system
from socket import gethostname
from typing import Optional, Union

from ifr_lib.ifr_exception import ItemNotFoundException
from ifr_lib.ifr_thread import call_timeout

try:
    from pwd import getpwnam
except ImportError:
    getpwnam = None

try:
    from grp import getgrgid, getgrnam
except ImportError:
    getgrnam = None
    getgrgid = None


def execution_info() -> OrderedDict:
    """Retrieves execution information

    Returns:
        OrderedDict: execution information
    """
    info = OrderedDict([
        ('command', " ".join(sys.argv)),
        ('pid', os.getpid()),
        ('hostname', gethostname()),
        ('system', system()),
        ('nb_proc', cpu_count()),
        ('user', getuser())
    ])
    info.update(user_info())
    return info


def user_info(user_name: Optional[str] = None) -> Union[OrderedDict, dict]:
    """Retrieves user information

    Args:
        user_name (str): user name. Retrieves current user if empty

    Returns:
        The user information
    """
    if not user_name:
        user_name = getuser()

    if getpwnam is None:
        return {"user": user_name}

    try:
        pwd = getpwnam(user_name)

        return OrderedDict([
            ('user', user_name),
            ('user_uid', pwd.pw_uid),
            ('user_gid', pwd.pw_gid),
            ('user_gname', group_name(pwd.pw_gid))
        ])
    except KeyError:
        raise ItemNotFoundException(f"User not found : {user_name}")


def group_id(group_name: str) -> Union[int, None]:
    """Retrieves unix group id

    Args:
        group_name (str): unix group name.

    Returns:
        The unix group id
    """
    if getgrnam is None or group_name is None or not isinstance(group_name, str):
        return None

    try:
        result = getgrnam(group_name)
    except KeyError:
        result = None

    if result is not None:
        return result[2]
    return None


def group_name(group_id: int) -> Union[str, None]:
    """Retrieves unix group name

    Args:
        group_id (int): unix group id.

    Returns:
        The unix group name
    """
    if getgrgid is None or group_id is None or not isinstance(group_id, int):
        return None

    try:
        result = getgrgid(group_id)
    except KeyError:
        result = None

    if result is not None:
        return result[0]
    return None


def user_id(user_name: str) -> Union[int, None]:
    """Retrieves unix user id

    Args:
        user_name (str): unix user name.

    Returns:
        The unix user id
    """
    if getpwnam is None or user_name is None:
        return None

    try:
        result = getpwnam(user_name)
    except KeyError:
        result = None

    if result is not None:
        return result[2]
    return None


def chown(path: str, owner: Optional[Union[str, int]] = None, group: Optional[Union[str, int]] = None):
    """Changes file/directory owner/group

    Args:
         path (str): path to directory
         owner (str|int, optional): owner id or name. Defaults to None
         group (str|int, optional): group id or name. Defaults to None

    Examples:
        >>> print(chown("/tmp/mydir/mysubdir/my_file.ext", group="toto"))
    """

    if os.name == 'nt' or (not owner and not group):
        return

    _owner = owner
    _group = group

    # -1 means don't change it
    if owner is None:
        _owner = -1
    # user can either be an int (the uid) or a string (the system username)
    elif isinstance(owner, str):
        _owner = user_id(owner)
        if _owner is None:
            raise ItemNotFoundException(f"No such user: {owner}")

    # -1 means don't change it
    if group is None:
        _group = -1
    elif not isinstance(group, int):
        _group = group_id(group)
        if _group is None:
            raise ItemNotFoundException(f"No such group: {group}")

    os.chown(path, _owner, _group)


def mkdirs(dir_path: str, mode: Optional[oct] = 0o777, exist_ok: Optional[bool] = True, group: Optional[str] = None,
           owner: Optional[str] = None) -> bool:
    """Creates tree directories

    If group or owner is set, changes owner/group.

    Args:
         dir_path (str): path to directory
         mode (octal): directories permissions
         exist_ok (bool) : If the target directory already exists, raise an OSError if exist_ok is False.
                           Otherwise no exception is raised
         group (str): group name. Defaults to None
         owner (str): owner name. Defaults to None

    Returns:
        True if file is empty, otherwise False

    Raises:
        OSError: if exist_ok is False

    Examples:
        >>> print(mkdirs("/tmp/mydir/mysubdir"))
        True
    """

    def create_dir(directory_path: str, exist_ok_option: bool, own: Union[str, int], grp: Union[str, int]) -> bool:
        # create current dir
        try:
            os.mkdir(directory_path)
        except OSError:
            # Cannot rely on checking for EEXIST, since the operating system
            # could give priority to other errors like EACCES or EROFS
            if not exist_ok_option or not os.path.isdir(directory_path):
                raise
            return False
        # change permissions
        chown(directory_path, owner=own, group=grp)
        return True

    # retrieve head and tail from path
    head, tail = os.path.split(dir_path)

    # if dir path ends with "/", split return an empty tail
    # we retry with head if tail is empty
    if not tail:
        head, tail = os.path.split(head)

    # if parent does not exists and is different from "/"
    if head and tail and not os.path.exists(head):
        try:
            mkdirs(head, mode, group, owner, exist_ok)
        except OSError:
            # Defeats race condition when another thread created the path
            pass

        cur_dir = os.path.curdir
        if isinstance(tail, bytes):
            if sys.version_info <= (2, 7):
                cur_dir = bytes(os.path.curdir, 'ASCII')
            else:
                cur_dir = bytes(os.path.curdir)
        if tail == cur_dir:    # xxx/newdir/. exists if xxx/newdir exists
            return False

    return create_dir(dir_path, exist_ok, owner, group)


def rmdirs(dir_path: str, stop_dirname: Optional[str] = None, stop_dirpath: Optional[str] = None):
    """Same as os.removedirs, with a safety addition concerning root_dir removal

    Super-rmdir; remove a leaf directory and all empty intermediate
    ones.  Works like rmdir except that, if the leaf directory is
    successfully removed, directories corresponding to rightmost path
    segments will be pruned away until either the whole path is
    consumed or an error occurs.  Errors during this latter phase are
    ignored -- they generally mean that a directory was not empty.

    Args:
        dir_path (str): path to directory.
        stop_dirname (str): name of directory where to stop
        stop_dirpath (str): directory path where to stop

    Examples:
        >>> rmdirs('/export/home/mypath/subdir1/subdir2', stop_dirname='mypath')
        >>> rmdirs(
        >>>   '/export/home/mypath/subdir1/subdir2',
        >>>   stop_dirpath='/export/home/mypath'
        >>> )
        # will remove subdir2 and subdir1 if they are empty
    """

    # remove current dir
    os.rmdir(dir_path)

    # retrieve head and tail from path
    head, tail = os.path.split(dir_path)

    # if dir path ends with "/", split return an empty tail
    # we retry with head if tail is empty
    if not tail:
        head, tail = os.path.split(head)

    # if tail is empty, we are in "/" directory
    while head and tail:
        # conditions to stop browsing in parent tree
        if stop_dirname and tail == stop_dirname:
            break
        if stop_dirpath and head == stop_dirpath:
            break

        # try to remove dir
        try:
            os.rmdir(head)
        except OSError:
            pass

        # goes up the tree
        head, tail = os.path.split(head)


def rm_empty_dirs(dir_path: str, remove_root_dir: Optional[bool] = False) -> int:
    """Remove empty sub directories

    Args:
        dir_path (str): root path
        remove_root_dir (bool): remove also root_path

    Returns:
        The number of directories deleted
    """
    assert isinstance(dir_path, str)
    assert isinstance(remove_root_dir, bool)

    nb_dir_removed = 0

    for root, dirnames, filenames in os.walk(dir_path, topdown=False):
        # non empty folder
        if filenames:
            continue

        # root folder and remove_root_dir==False
        if root == dir_path and remove_root_dir is False:
            continue

        # remove directory
        # raise OSError if directory is not empty
        try:
            os.rmdir(root)
            nb_dir_removed += 1
        except OSError:
            pass

    return nb_dir_removed


def is_io_locked(dir_path: str, timeout: Optional[float] = 0.5) -> bool:
    """Check if the path is accessible

    Args:
        dir_path (str): path to directory.
        timeout (float): name of directory where to stop. Defaults to 0.5s

    Returns:
        False if path is accessible, otherwise True

    Examples:
        >>> is_io_locked('/export/home')
        True
    """
    return not call_timeout(lambda: os.stat(dir_path), timeout)


def make_executable(file_path: str):
    """Chmod +x on file

    Args:
        file_path (str): path to file
    """
    mode = os.stat(file_path).st_mode
    mode |= (mode & 0o444) >> 2
    os.chmod(file_path, mode)
