# qmonitor

The binary `qmonitor` allows to follow the execution of a jobId list.

```bash
usage: qmonitor [-h] [--interval INTERVAL] [--silent] [--pretty]
                job_id [job_id ...]

Supervise the end of a jobId list

positional arguments:
  job_id               A PBS job ID (ex: 8391934.datarmor0)

optional arguments:
  -h, --help           show this help message and exit
  --interval INTERVAL  Time in second between two analysis (default: 60s)
  --silent             Print only final message
  --pretty             Prettify final message
```

You can inquire job ID via stdin :

```bash
qsub <script> | qmonitor
```

Behavior :
- check the validity and retrieve creation date for each jobs (`qstat <jobID>`)
- while one job is still running
    - update the jobs status
        - if the job is still running, call the binary `IsFinished <jobId>`
        - if the job is just finished, retrieve information from PBS report (`qstat -xf <jobId>`)
    - display report if the option `silent` is unset
    - wait `interval` seconds
- print final report
- exit with computed exit code from all jobs

## Examples

### Invalid job

```bash
> qmonitor 8393705.datarmor8
{"exit_status": 1, "jobs": [{"job_id": "8393705.datarmor8", "state": "Invalid", "exit_status": 1, "start_date": null, "end_date": null, "duration": null}]}
> echo $?
1
```

### Valid and finished job
 
```bash
> qmonitor 8393911.datarmor0 --pretty
{
  "exit_status": 0,
  "jobs": [
    {
      "job_id": "8393911.datarmor0",
      "state": "Finished",
      "exit_status": 0,
      "start_date": "2020-04-15T11:56:28",
      "end_date": "2020-04-15T11:57:34",
      "duration": 66
    }
  ]
}
> echo $?
0

> qmonitor 8394092.datarmor0 8394116.datarmor0 --pretty
{
  "exit_status": 1,
  "jobs": [
    {
      "job_id": "8394092.datarmor0",
      "state": "Finished",
      "exit_status": 0,
      "start_date": "2020-04-15T12:19:49",
      "end_date": "2020-04-15T12:21:29",
      "duration": 100
    },
    {
      "job_id": "8394116.datarmor0",
      "state": "Finished",
      "exit_status": 124,
      "start_date": "2020-04-15T12:25:56",
      "end_date": "2020-04-15T12:27:53",
      "duration": 117
    }
  ]
}
> echo $?
1
```

### stdin succes usage

```bash
> cat << EOF > "${HOME}/test.pbs"
#!/usr/bin/env bash

echo "start"
sleep 45
echo "end"
EOF
> qsub -l walltime=00:01:00 -m a "${HOME}/test.pbs" | qmonitor --interval 30

| Job ID            | State   | Exit_status  | Creation date       |
--------------------------------------------------------------------
| 8394092.datarmor0 | Running | None         | 2020-04-15T12:25:56 |

{"exit_status": 0, "jobs": [{"job_id": "8394092.datarmor0", "state": "Finished", "exit_status": 0, "start_date": "2020-04-15T12:19:49", "end_date": "2020-04-15T12:21:29", "duration": 100}]}
``` 

```bash
> cat << EOF > "${HOME}/test.pbs"
#!/usr/bin/env bash

echo "start"
sleep 10
exit 124
EOF

> qsub -l walltime=00:01:00 -m a "${HOME}/test.pbs" | qmonitor --interval 30

Waiting all jobs to be finished. Check every 30 seconds
Time : 2020-04-15 13:31:56.091865

| Job ID            | State   | Exit_status  | Creation date       |
--------------------------------------------------------------------
| 8394116.datarmor0 | Running | None         | 2020-04-15T12:25:56 |

{"exit_status": 124, "jobs": [{"job_id": "8394116.datarmor0", "state": "Finished", "exit_status": 124, "start_date": "2020-04-15T12:25:56", "end_date": "2020-04-15T12:27:53", "duration": 117}]}
``` 
